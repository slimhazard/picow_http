#!/usr/bin/env python3

# Copyright (c) 2024 Geoff Simmons <geoff@simmons.de>
#
# SPDX-License-Identifier: BSD-2-Clause
# See LICENSE

import sys
import os
import re
import argparse
from yaml import safe_dump

cfg = [
    {'server': []}
]

parser = argparse.ArgumentParser(
    description='Generate a skeleton www.yaml for picow-http',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument('wwwdir', nargs='?', default='.',
                    help='Directory containing static WWW files')
parser.add_argument('-o', '--output', help='output file', default='stdout')
parser.add_argument('-x', '--exclude_regex',
                    help='regex for files/dirs to exclude',
                    default=r'(^crt$)|(~$)')
args = parser.parse_args()

wwwdir = args.wwwdir
out = sys.stdout
if args.output != 'stdout':
    try:
        out = open(args.output, 'w')
    except Exception as e:
        print(f'Cannot open {args.output} for write: {e}', file=sys.stderr)
        sys.exit(-1)
exclude = re.compile(args.exclude_regex)

def walk(srv, path):
    with os.scandir(path) as it:
        for entry in it:
            if exclude.search(entry.name):
                continue
            if entry.is_dir():
                walk(srv, entry.path)
            else:
                val = os.path.relpath(entry.path, wwwdir)
                srv.append({'static': {'file': val }})

walk(cfg[0]['server'], wwwdir)
safe_dump(cfg, stream=out)
