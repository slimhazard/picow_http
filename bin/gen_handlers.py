#!/usr/bin/env python3

# Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
#
# SPDX-License-Identifier: BSD-2-Clause
# See LICENSE

import sys
import os
import shutil
import re
import zlib
import gzip
import brotli
import base64
import subprocess
from yaml import safe_load
from jinja2 import Environment, FileSystemLoader
from cryptography import x509
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend

nsrvrs = 0
servers = []
path_hndlrs = {}

server_items = {
    'static', 'custom', 'certificate', 'name', 'custom_error'
}
server_names = set()

cfgfile = None
gperf_template = None
cert_template = None
gendir = None
gperf_genfile = None
cert_genfile = None
wwwdir = None
www_genpath = None
minify = None

methods = {
    'GET', 'HEAD', 'POST', 'PUT', 'DELETE', 'CONNECT', 'OPTIONS', 'TRACE'
}

types = {
    '.html': 'text/html',
    '.htm':  'text/html',
    '.js':   'text/javascript',
    '.json': 'application/json',
    '.xml':  'application/xml',
    '.gif':  'image/gif',
    '.jpg':  'image/jpeg',
    '.jpeg': 'image/jpeg',
    '.png':  'image/png',
    '.ico':  'image/vnd.microsoft.icon',
    '.svg':  'image/svg+xml',
    '.css':  'text/css',
    '.csv':  'text/csv',
    '.txt':  'text/plain',
}

minifiable = {
    'text/html',
    'text/css',
    'text/javascript',
    'text/xml',
    'application/json',
    'image/svg+xml',
}

DEFAULT_CACHE_CTRL = 'public, max-age=604800'

# Matches 'type/subtype' in a Content-Type configuration that may be
# something like 'type/subtype; charset=UTF-8'.
mime_matcher = re.compile(r'\b([^/]+?/[^;]+?)(?:;|$)')

def mksymbol(s):
    return re.sub(r"[^A-Za-z0-9]", "_", s)

def text(type):
    c = {
        'application/javascript',
        'application/x-javascript',
        'application/json',
        'application/xml',
        'application/html',
        'application/xhtml',
        'application/rss',
        'image/svg+xml',
        'image/svg',
    }
    return type.startswith('text/') or type in c

def compressible(type):
    c = {
        'image/x-icon',
        'image/vnd.microsoft.icon',
        'font/woff',
    }
    return text(type) or type in c

##
## Static handler config
##
def static_cfg(srv, scfg):
    global wwwdir
    global www_genpath

    if 'file' not in scfg:
        print(f'Required field file not found in static config: {scfg}',
              file=sys.stderr)
        sys.exit(-1)

    ##
    ## Create subdirectory 'www' in the generated path. Static files
    ## to be embedded will be located there (and need to be found on
    ## the C/ASM include path for the .incbin command).
    ##
    if www_genpath == None:
        if wwwdir == None:
            print('WWW directory is required for static handler configuration',
                  file=sys.stderr)
            sys.exit(-1)
        try:
            www_genpath = os.path.join(gendir, 'www')
            os.mkdir(www_genpath)
        except FileExistsError:
            pass
        except Exception as e:
            print(f'Cannot create directory {www_genpath}: {e}',
                  file=sys.stderr)
            sys.exit(-1)
            # Delete all in www_genpath

    src = os.path.join(wwwdir, scfg['file'])
    try:
        with open(src, mode='rb') as f:
            crc = zlib.crc32(f.read())
        b64 = base64.b64encode(crc.to_bytes(4, sys.byteorder))
        etag = b64.decode('ascii').strip("=")
    except Exception as e:
        print(f'Failed to read {src}: {e}', file=sys.stderr)
        sys.exit(-1)

    # If the src file is in a subirectory, create the corresponding
    # subdirectory in the generated/www subdirectory created above.
    path_base = os.path.split(src)
    relpath = os.path.relpath(path_base[0], wwwdir)
    filename = srv['symbol'] + "_" + path_base[1]
    www_genpath_dir = os.path.join(www_genpath, relpath)
    www_genpath_dst = os.path.join(www_genpath_dir, filename)
    include_path = os.path.join(relpath, filename)
    try:
        os.mkdir(www_genpath_dir)
    except FileExistsError:
        pass
    except Exception as e:
        print(f'Cannot create directory {www_genpath_dir}: {e}',
              file=sys.stderr)
        sys.exit(-1)

    # Copy to the destination directory below generated/www.
    try:
        shutil.copy2(src, www_genpath_dst)
    except Exception as e:
        print(f'Cannot copy {src} to {www_genpath_dst}: {e}', file=sys.stderr)
        sys.exit(-1)

    # Populate data for template rendering, first for the static config.
    s = {}
    s['etag'] = etag
    s['symbol'] = srv['symbol'] + "_" + mksymbol(scfg['file'])
    if 'type' in scfg:
        s['type'] = scfg['type']
    else:
        # Infer Content-Type from extension
        split = os.path.splitext(scfg['file'])
        if split[1] not in types:
            print(f"Unable to infer type: {scfg['file']}", file=sys.stderr)
            sys.exit(-1)
        s['type'] = types[split[1]]
    if 'cache_control' in scfg:
        s['cache_ctrl'] = scfg['cache_control']
    else:
        s['cache_ctrl'] = DEFAULT_CACHE_CTRL

    # file_path = os.path.join(www_genpath_dst, filename)

    # minify if configured, or if the type is minifiable.
    mime_match = mime_matcher.match(s['type'])
    if mime_match == None:
        print(f"Cannot match media type in type configuration: {s['type']}",
              file=sys.stderr)
        sys.exit(-1)
    mime = mime_match.group(1)
    if minify != None:
        if 'minify' in scfg:
            do_minify = scfg['minify']
        else:
            do_minify = mime in minifiable
        if do_minify:
            try:
                file_orig = www_genpath_dst + '.orig'
                os.rename(www_genpath_dst, file_orig)
                subprocess.run([minify, '--mime', mime, '-o', www_genpath_dst,
                                file_orig], check=True, capture_output=True)
            except subprocess.CalledProcessError as e:
                print(f'{minify} {www_genpath_dst} failed with status '+
		      f'{e.returncode}', file=sys.stderr)
                print(f'{e.stderr}', file=sys.stderr)
                sys.exit(-1)
            except Exception as e:
                print(f'Error invoking {minify} {www_genpath_dst}: {e}',
                      file=sys.stderr)
                sys.exit(-1)

    # Compress if configured, or if the type is compressible, default gzip.
    if 'encodings' not in scfg:
        if compressible(s['type']):
            scfg['encodings'] = [ 'gzip' ]
        else:
            scfg['encodings'] = [ 'identity' ]
    s['encodings'] = {}
    for enc in scfg['encodings']:
        if enc == 'identity':
            s['encodings']['id'] = {}
            s['encodings']['id']['file'] = include_path
            s['encodings']['id']['suffix'] = 'id'
        elif enc == 'gzip':
            try:
                with open(www_genpath_dst, mode='rb') as f_in:
                    with gzip.open(www_genpath_dst + ".gz", mode='wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
            except Exception as e:
                print(f'Error in gzip compression of {www_genpath_dst}: {e}',
                      file=sys.stderr)
                sys.exit(-1)
            s['encodings']['gzip'] = {}
            s['encodings']['gzip']['file'] = include_path + ".gz"
            s['encodings']['gzip']['suffix'] = 'gz'
        elif enc == 'br':
            try:
                with open(www_genpath_dst, mode='rb') as f_in:
                    with open(www_genpath_dst + ".br", mode='wb') as f_out:
                        if text(mime):
                            mode = brotli.MODE_TEXT
                        elif mime == 'font/woff':
                            mode = brotli.MODE_WOFF
                        else:
                            mode = brotli.MODE_GENERIC
                        f_out.write(brotli.compress(f_in.read(), mode=mode))
            except Exception as e:
                printf(f'Error in brotli compression of {www_genpath_dst}: {e}',
                       file=sys.stderr)
                sys.exit(-1)
            s['encodings']['br'] = {}
            s['encodings']['br']['file'] = include_path + ".br"
            s['encodings']['br']['suffix'] = 'br'
        else:
            print(f'encoding must be "gzip", "br" or "identity": {enc}',
                  file=sys.stderr)
            sys.exit(-1)
    srv['statics'].append(s)

    # Populate data for gperf hash config.
    paths = []
    if 'path' in scfg:
        paths.append(scfg['path'])
    else:
        paths.append('/' + scfg['file'])
        if scfg['file'] == 'index.html':
            paths.append('/')
            paths.append('/index.htm')
        if scfg['file'] == 'index.htm':
            paths.append('/')
            paths.append('/index.html')
    for path in paths:
        if path not in path_hndlrs:
            psymbol = mksymbol(path)
            path_hndlrs[path] = { 'symbol': psymbol, 'srv': [None] * nsrvrs }
        path_hndlrs[path]['srv'][srv['idx']] = s['symbol']

##
## Custom handler config
##
def custom_cfg(srv, ccfg):
    # global nsrvrs

    if type(ccfg) is not dict or len(ccfg) != 2 or 'path' not in ccfg or \
       'methods' not in ccfg:
        print(f"Custom config requires the fields 'path' and 'methods': {ccfg}",
              file=sys.stderr)
        sys.exit(-1)
    for m in ccfg['methods']:
        if m not in methods:
            print(f'Invalid method: {m}', file=sys.stderr)
            sys.exit(-1)
    symbol = mksymbol(srv['symbol'] + "_" + ccfg['path'])
    srv['customs'].append({
        'symbol' : symbol,
        'methods': ccfg['methods'],
        'allow': ', '.join(ccfg['methods'])
    })
    if ccfg['path'] not in path_hndlrs:
        path_hndlrs[ccfg['path']] = {
            'symbol': mksymbol(ccfg['path']),
            'srv': [None] * nsrvrs
        }
    path_hndlrs[ccfg['path']]['srv'][srv['idx']] = symbol

##
## Custom error status config
##
def set_status_bit(bits, status):
    if status < 400 or status > 599:
        print("custom_error config: status must be >= 400 and <= 599: "+
              f"{status}", file=sys.stderr)
        sys.exit(-1)
    bit = status - 400
    bits[bit >> 3] |= 1 << (bit & 7)

def err_status_cfg(srv, scfg):
    if type(scfg) is not list:
        print(f"custom_error config: must be an array: {scfg}",
              file=sys.stderr)
        sys.exit(-1)
    if 'errbits' not in srv:
        srv['errbits'] = [0] * 25
    for s in scfg:
        if len(s) != 1 or ('status' not in s and 'range' not in s):
            print("custom_error config: fields must be 'status' or 'range': "+
                  f"{s}", file=sys.stderr)
            sys.exit(-1)
        if 'status' in s:
            try:
                code = int(s['status'])
            except Exception as e:
                print("custom_error config: cannot convert 'status' to "+
                      f"integer: {s} ({e})", file=sys.stderr)
                sys.exit(-1)
            set_status_bit(srv['errbits'], code)
        else:
            r = s['range']
            if len(r) != 2 or 'min' not in r or 'max' not in r:
                print("custom_error config: 'min' and 'max' (only) required "+
                      f"in 'range': {r}", file=sys.stderr)
                sys.exit(-1)
            try:
                rmin = int(r['min'])
                rmax = int(r['max'])
            except Exception as e:
                print("custom_error config: 'min' and 'max' must be integers"+
                      f": {r} ({e})", file=sys.stderr)
                sys.exit(-1)
            if rmin > rmax:
                print("custom_error config: 'min' must be less than 'max': "+
                      f"{r}", file=sys.stderr)
                sys.exit(-1)
            for code in range(rmin, rmax + 1):
                set_status_bit(srv['errbits'], code)

##
## TLS certificate config
##
def cert_cfg(srv, ccfg):
    global wwwdir

    if type(ccfg) is not dict or len(ccfg) < 2 or len(ccfg) > 3 or \
       'crt' not in ccfg or 'key' not in ccfg or \
       (len(ccfg) == 3 and 'pass' not in ccfg):
        print(f"certificate config: fields 'crt' & 'key' are required, "+
              "'pass' is optional: {ccfg}", file=sys.stderr)
        sys.exit(-1)
    srv['cert'] = {}
    # XXX DRY with www_genpath above
    if wwwdir == None:
        print('WWW directory is required for certificate configuration',
              file=sys.stderr)
        sys.exit(-1)
    try:
        www_crtpath = os.path.join(gendir, 'crt')
        os.mkdir(www_crtpath)
    except FileExistsError:
        pass
    except Exception as e:
        print(f'Cannot create directory {www_crtpath}: {e}', file=sys.stderr)
        sys.exit(-1)
    # Delete all in www_crtpath
    try:
        files = os.listdir(www_crtpath)
        for file in files:
            path = os.path.join(www_crtpath, file)
            if os.path.isfile(path):
                os.remove(path)
    except Exception as e:
            print(f'Cannot delete files in {www_crtpath}: {e}', file=sys.stderr)
            sys.exit(-1)

    keypass = None
    if 'pass' in ccfg:
        path_base = os.path.split(ccfg['pass'])
        filename = srv['symbol'] + "_" + path_base[1]
        www_passpath_dst = os.path.join(www_crtpath, filename)
        pass_src_path = os.path.join(wwwdir, ccfg['pass'])
        try:
            with open(pass_src_path, "rb") as pass_in:
                keypass = pass_in.read()
            with open(www_passpath_dst, "wb") as pass_out:
                pass_out.write(keypass)
        except Exception as e:
            print(f"Cannot copy {pass_src_path} to generated path: {e}",
                  file=sys.stderr)
            sys.exit(-1)
        srv['cert']['pass_file'] = filename

    try:
        crt_src_path = os.path.join(wwwdir, ccfg['crt'])
        with open(crt_src_path, "rb") as crt_in:
            crt_in_bytes = crt_in.read()
            try:
                crt = x509.load_pem_x509_certificate(crt_in_bytes)
                crt_der = crt.public_bytes(serialization.Encoding.DER)
            except ValueError:
                crt_der = crt_in_bytes
            except Exception as e:
                print(f"Cannot read/decode certificate {crt_src_path}: {e}",
                      file=sys.stderr)
                sys.exit(-1)
        path_base = os.path.split(ccfg['crt'])
        filename = srv['symbol'] + "_" + path_base[1] + ".der"
        www_crtpath_dst = os.path.join(www_crtpath, filename)
        with open(www_crtpath_dst, "wb") as crt_out:
            crt_out.write(crt_der)
    except Exception as e:
        print(f"Cannot copy {crt_src_path} as DER to generated path: {e}",
                  file=sys.stderr)
        sys.exit(-1)
    srv['cert']['crt_file'] = filename

    try:
        key_src_path = os.path.join(wwwdir, ccfg['key'])
        key_enc = serialization.NoEncryption()
        if keypass != None:
            key_enc = serialization.BestAvailableEncryption(keypass)
        with open(key_src_path, "rb") as key_in:
            key_in_bytes = key_in.read()
            try:
                key = serialization.load_pem_private_key(key_in_bytes, keypass,
                                                         default_backend())
                key_der = key.private_bytes(
                    serialization.Encoding.DER,
                    serialization.PrivateFormat.TraditionalOpenSSL,
                    serialization.NoEncryption()
                )
            except ValueError:
                key_der = key_in_bytes
            except Exception as e:
                print(f"Cannot read/decode private key {key_src_path}: {e}",
                      file=sys.stderr)
                sys.exit(-1)
        path_base = os.path.split(ccfg['key'])
        filename = srv['symbol'] + "_" + path_base[1] + ".der"
        www_keypath_dst = os.path.join(www_crtpath, filename)
        with open(www_keypath_dst, "wb") as key_out:
            key_out.write(key_der)
    except Exception as e:
        print(f"Cannot copy {key_src_path} as DER to generated path: {e}",
                  file=sys.stderr)
        sys.exit(-1)
    srv['cert']['key_file'] = filename

##
## Main
##

if len(sys.argv) != 7 and len(sys.argv) != 8:
    print(f"Usage: {sys.argv[0]} config gperf_templ crt_templ gen_dir\n"+
          "\tgperf_gen_file cert_gen_file www_dir",
          file=sys.stderr)
    sys.exit(-1)

cfgfile = sys.argv[1]
gperf_template = sys.argv[2]
cert_template = sys.argv[3]
gendir = sys.argv[4]
gperf_genfile = sys.argv[5]
cert_genfile = sys.argv[6]
if len(sys.argv) == 8:
    wwwdir = sys.argv[7]

minify = shutil.which('minify')

##
## Load the YAML config
##
try:
    f = open(cfgfile, 'r')
except Exception as e:
    print(f'Cannot open {cfgfile}: {e}', file=sys.stderr)
    sys.exit(-1)

try:
    cfg = safe_load(f)
except Exception as e:
    print(f'Cannot load {cfgfile} as YAML: {e}', file=sys.stderr)
    sys.exit(-1)
f.close()

if type(cfg) is not list:
    print(f'Top-level config in {config} must be an array', file=sys.stderr)
    sys.exit(-1)

nsrvrs = len(cfg)
for s in cfg:
    if type(s) is not dict or len(s) != 1 or 'server' not in s:
        print(f"Each top-level item in {cfgfile} must be 'server'",
              file=sys.stderr)
        sys.exit(-1)

    server = {}
    server['idx'] = len(servers)
    server['symbol'] = 'srv' + str(server['idx'])
    server['statics'] = []
    server['customs'] = []

    try:
        srv_genpath = os.path.join(gendir, server['symbol'])
        os.mkdir(srv_genpath)
    except FileExistsError:
        pass
    except Exception as e:
        print(f'Cannot create directory {srv_genpath}: {e}',
              file=sys.stderr)
        sys.exit(-1)

    for c in s['server']:
        if type(c) is not dict or len(c) != 1 or \
           not set(c.keys()) <= server_items:
            print(f"{cfgfile}: Each item of a server config must be one of: "+
                  f"{server_items}", file=sys.stderr)
            sys.exit(-1)

        if 'static' in c:
            static_cfg(server, c['static'])
        elif 'custom' in c:
            custom_cfg(server, c['custom'])
        elif 'name' in c:
            if c['name'] in server_names:
                print(f"{cfgfile}: server names must be unique ({c['name']})",
                      file=sys.stderr)
                sys.exit(-1)
            server['name'] = c['name']
            server_names.add(c['name'])
        elif 'custom_error' in c:
            err_status_cfg(server, c['custom_error'])
        else:
            if 'cert' in server:
                print(f"{cfgfile}: 'certificate' may only appear once in a "+
                      "server configuration", file=sys.stderr)
                sys.exit(-1)
            cert_cfg(server, c['certificate'])

    if 'name' not in server:
        if 'default' in server_names:
            print(f"{cfgfile}: more than one default server", file=sys.stderr)
            sys.exit(-1)
        server['name'] = 'default'
        server_names.add('default')
    servers.append(server)

    if 'errbits' in server:
        server['err_array'] = \
            f"{{{','.join([hex(b) for b in server['errbits']])}}}"

##
## Generation from templates
##

# Load the templates.
gperf_tmpl_path = os.path.dirname(gperf_template)
gperf_tmpl_base = os.path.basename(gperf_template)
gperf_env = Environment(loader=FileSystemLoader(gperf_tmpl_path))
cert_tmpl_path = os.path.dirname(cert_template)
cert_tmpl_base = os.path.basename(cert_template)
cert_env = Environment(loader=FileSystemLoader(cert_tmpl_path))
try:
    gperf_tmpl = gperf_env.get_template(gperf_tmpl_base)
    cert_tmpl = cert_env.get_template(cert_tmpl_base)
except Exception as e:
    print(f'Cannot load template {gperf_template}: {e}', file=sys.stderr)
    sys.exit(-1)

# Render the templates.
gperf_genpath = os.path.join(gendir, gperf_genfile)
try:
    data = {'servers': servers, 'path_hndlrs': path_hndlrs }
    content = gperf_tmpl.render(data)
    with open(gperf_genpath, 'w') as output:
        output.write(content)
except Exception as e:
    print(f'Cannot generate template {gperf_genpath}: {e}', file=sys.stderr)
    sys.exit(-1)

cert_genpath = os.path.join(gendir, cert_genfile)
try:
    data = { 'servers': servers }
    content = cert_tmpl.render(data)
    with open(cert_genpath, 'w') as output:
        output.write(content)
except Exception as e:
    print(f'Cannot generate template {cert_genpath}: {e}', file=sys.stderr)
    sys.exit(-1)
