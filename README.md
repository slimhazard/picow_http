<img align="right" src="/doc/img/picow-http.png">

[[_TOC_]]

# Overview

picow-http is an HTTP server for the [Raspberry Pi Pico W
microcontroller](https://www.raspberrypi.com/documentation/microcontrollers/raspberry-pi-pico.html#raspberry-pi-pico-w-and-pico-wh),
which can be integrated as a library into an application based on the
[Pico C
SDK](https://www.raspberrypi.com/documentation/microcontrollers/c_sdk.html).
Some of the library's features are:

  * Integration of static resources (such as HTML, CSS, Javascript and
    images):
      * Static resources are embedded in flash memory, and response
	    handlers for them are automatically configured.
	  * [Minification](https://developer.mozilla.org/en-US/docs/Glossary/minification)
	    and compression, where appropriate, are performed
	    automatically. This helps to minimize both network bandwidth
	    and flash memory footprint.
      * Static responses facilitate client-side caching and
		[validation](https://developer.mozilla.org/en-US/docs/Web/HTTP/Conditional_requests),
		using the
		[Cache-Control](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control)
		and
		[ETag](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag)
		response headers.
	  * With configuration in [CMakeLists.txt](https://cmake.org/),
	    static resource integration is performed transparently in the
	    build process, implemented as dependencies for the `make`
	    command.
  * A [public API](https://slimhazard.gitlab.io/picow_http/) for
	server management (configuring, starting and stopping a server)
	and implementation of custom response handlers to generate dynamic
	content.
  * The underlying TCP/IP stack is
	[lwIP](https://savannah.nongnu.org/projects/lwip/), integrated via
	the [Pico SDK's networking
	libraries](https://www.raspberrypi.com/documentation/pico-sdk/networking.html)
	(although this is almost completely transparent to application
	developers using picow-http). picow-http uses lwIP's ["raw" TCP
	API](https://www.nongnu.org/lwip/2_1_x/group__callbackstyle__api.html).
  * Optional support for
	[TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security)
	(https) for secure communication. If TLS is chosen, the server
	certificate and private key are embedded in flash as part of the
	build process, by the same mechanisms used to embed static
	resources.  The TLS implementation is based on
	[mbedtls](https://www.trustedfirmware.org/projects/mbed-tls/), linked
	via	the SDK's
	[pico_lwip_mbedtls](https://www.raspberrypi.com/documentation/pico-sdk/networking.html#pico_lwip)
	library. If TLS is not required, a smaller version of picow-http can be
	installed.

For more detailed documentation, see:

  * the [project Wiki](https://gitlab.com/slimhazard/picow_http/-/wikis/Home)
  * the [API documentation](https://slimhazard.gitlab.io/picow_http/)

Developers of applications using picow-http should be familiar with:

  * the C SDK
    ([online](https://www.raspberrypi.com/documentation/pico-sdk/),
     [PDF](https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-c-sdk.pdf))
  * the means by which the PicoW connects to WLAN
	([PDF](https://datasheets.raspberrypi.com/picow/connecting-to-the-internet-with-pico-w.pdf))
  * the [HTTP protocol](https://www.rfc-editor.org/rfc/rfc9110.html)
  * for TLS, the means to create a [server
	certificate](https://en.wikipedia.org/wiki/Public_key_certificate#TLS/SSL_server_certificate)
	and private key
  * the basics of HTML and related technologies

See the [picow-http-example](https://gitlab.com/slimhazard/picow-http-example)
repository for a full example of a project that uses picow-http.

# Installation

## Required software

The [C
SDK](https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf)
since version 1.5.1 and its toolchain are required for development of
applications that use picow-http. These tools are also required:

  * [gperf](https://www.gnu.org/software/gperf/)
  * [minify](https://github.com/tdewolff/minify) as a command-line tool
	(not strictly required,	but strongly recommended)
  * python3, but that is already in the SDK toolchain
  * Python libraries [brotli](https://github.com/google/brotli),
	[yaml](https://pyyaml.org/wiki/PyYAMLDocumentation),
	[jinja2](https://pypi.org/project/Jinja2/), and
	[pyca/cryptography](https://cryptography.io/en/latest/).
	* Python `pyca/cryptography` in turn requires an SSL library such as
	  [OpenSSL](https://www.openssl.org/).

See the [project
Wiki](https://gitlab.com/slimhazard/picow_http/-/wikis/required-software)
for details.

## Linking picow_http or picow_https

This repository provides the libraries `picow_http` and `picow_https`
(CMake INTERFACEs). An application project integrates one or the other
of them, depending on whether TLS is required, as specified in the
CMake configuration.

`picow_https` transparently bases network communication on TLS;
`picow_http` is a smaller version of the library, with lower RAM
footprint and faster connection setup (since TLS handshake is not
executed). For `picow_https`, server key material must be provided;
otherwise the two libraries are identical for the application
developer.

You can add picow-http to your project as a [git
submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules), and
add the submodule with `add_subdirectory()` in `CMakeLists.txt`:

```shell
# lib/ is a project subdirectory.
$ git submodule add https://gitlab.com/slimhazard/picow_http.git lib/picow-http

$ git commit -m "Add picow-http as a submodule"
```

```cmake
# In CMakeLists.txt for the application using picow-http.
# Assuming that CMakeLists.txt is in the project root.
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/lib/picow-http)
```

Alternatively you can clone the repository anywhere you like on your
system, and configure its location in `CMakeLists.txt` for your app
with the two-argument version of the `add_subdirectory()` directive:

```cmake
# In CMakeLists.txt
add_subdirectory(/path/to/picow-http ${CMAKE_BINARY_DIR})
```

`${CMAKE_BINARY_DIR}` in the second argument specifies your project's
build directory as the build output for picow-http.

Using a submodule has the advantage that its location can be
configured relative to the project root (not at a hard-wired path),
and that it explicitly specifies the version of picow-http with which
the project is to be built.

`picow_http` or `picow_https` can then be integrated into your project
with the CMake directive `target_link_libraries()`, together with any
other libraries to be used by the application:

```cmake
# In CMakeLists.txt
add_executable(my-project
	# ... source files here
)

# This configuration uses the "threadsafe background" network architecture.
target_link_libraries(my-project
	picow_http
	pico_cyw43_arch_lwip_threadsafe_background
	# ... additional libs here
)
```

```cmake
# For TLS support
add_executable(my-tls-project
	# ... source files here
)

# This configuration uses the "poll" network architecture.
target_link_libraries(my-tls-project
	picow_https
	pico_cyw43_arch_lwip_poll
	# ... additional libs here
)
```

In `target_link_libraries`, you must choose:

  * either `picow_http` or `picow_https`
  * a [network
	architecture](https://www.raspberrypi.com/documentation/pico-sdk/networking.html#detailed-description38)
	for your project

picow-http can be used with any of the three network architectures
provided by the SDK:

  * threadsafe background mode (`pico_cyw43_arch_lwip_threadsafe_background`)
  * poll mode (`pico_cyw43_arch_lwip_poll`)
  * [FreeRTOS](https://www.freertos.org/) (`pico_cyw43_arch_lwip_sys_freertos`)
	* But see [Limitations](#limitations) below.

See the [SDK
docs](https://www.raspberrypi.com/documentation/pico-sdk/networking.html#detailed-description38)
for details about network architectures. Both of `picow_http` and `picow_https`
may be used with any supported mode.

picow-http requires use of the `picow_http_gen_handlers` directive in
the cmake configuration, to generate handlers for static resources, to
configure custom resource handlers, and if necessary to embed a server
certificate and private key. See the [project
Wiki](https://gitlab.com/slimhazard/picow_http/-/wikis/picow_http_gen_handlers)
for details.

## Linking FreeRTOS

There are additional considerations for an application that uses picow-http
with [FreeRTOS](https://www.freertos.org):

  * `target_link_libraries` must include `pico_cyw43_arch_lwip_sys_freertos`
  * the [FreeRTOS kernel](https://github.com/FreeRTOS/FreeRTOS-Kernel) must
    be linked. A FreeRTOS kernel version since V11.0.0 is required.
  * one of FreeRTOS' [memory management
    options](https://www.freertos.org/a00111.html) must be specified
  * the lwIP compile option `NO_SYS=0` must be set

Since SDK 2.0.0, picow-http also supports FreeRTOS in "nosys" mode:

  * `target_link_libraries` includes
	`pico_cyw43_arch_lwip_threadsafe_background`
  * the FreeRTOS kernel is linked and a memory management option is
	specified, as above
  * the lwIP compile option `NO_SYS=1` is set

A convenient way to specify the kernel for your build is to include
[`FreeRTOS_Kernel_import.cmake`](https://github.com/FreeRTOS/FreeRTOS-Kernel/blob/main/portable/ThirdParty/GCC/RP2040/FreeRTOS_Kernel_import.cmake) from the
[FreeRTOS RP2040 port](https://github.com/FreeRTOS/FreeRTOS-Kernel/tree/main/portable/ThirdParty/GCC/RP2040)
in your `CMakeLists.txt`. It must be included before the `project()` directive.
Then if the path of the kernel library is set in `FREERTOS_KERNEL_PATH`,
either as a cmake variable or environment variable, the code in
`FreeRTOS_Kernel_import.cmake` will add the build configuration for FreeRTOS.

As with the path for the picow-http library, the path for the FreeRTOS
kernel can be included in your project as a git submodule, or anywhere
on your system. In this example, both `FreeRTOS_Kernel_import.cmake`
and the submodule are included in the project, and
`FREERTOS_KERNEL_PATH` is set directly in `CMakeLists.txt`:

```bash
# In a project root directory
$ tree
.
├── CMakeLists.txt
├── FreeRTOS_Kernel_import.cmake
├── lib
│   ├── FreeRTOS-Kernel
│   └── picow-http
├── ... other files ...

# In CMake Lists.txt, before project()
set(FREERTOS_KERNEL_PATH ${CMAKE_CURRENT_LIST_DIR}/lib/FreeRTOS-Kernel)
include(${CMAKE_CURRENT_LIST_DIR}/FreeRTOS_Kernel_import.cmake)
```

FreeRTOS is named as a library in `target_link_libraries` with a designation
for one of its five [memory management
options](https://www.freertos.org/a00111.html): one of `FreeRTOS-Kernel-Heap1`
through `FreeRTOS-Kernel-Heap5`. A discussion of the heap allocation modes is
beyond the scope of this document; see the FreeRTOS docs for details. A common
choice is `FreeRTOS-Kernel-Heap4`.

So `target_link_libraries` for a FreeRTOS app using picow-http could be:

```cmake
target_link_libraries(my-project
	picow_http
	pico_cyw43_arch_lwip_sys_freertos
	FreeRTOS-Kernel-Heap4
	# ... additional libs here
)
```

The value for `NO_SYS` must be defined as a compile option, for
example in `lwipopts.h` or in `CMakeLists.txt` under
`target_compile_definitions`.

# Configuration

The configuration of picow-http for an application encompasses:

  * the [build-time
	configuration](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration)
	(the "`www.yaml`" file) that specifies details of static resources
	and server key material to be embedded, and declares endpoints for
	custom response handlers
  * integration in `CMakeLists.txt`, to link the library as described
	above, and to include the required directive
	[`picow_http_gen_handlers`](https://gitlab.com/slimhazard/picow_http/-/wikis/picow_http_gen_handlers)
  * an implementation of
	[`lwipopts.h`](https://gitlab.com/slimhazard/picow_http/-/wikis/Configuring-lwipopts.h)
	for the TCP stack
  * an implementation of
	[`mbedtls_config.h`](https://github.com/Mbed-TLS/mbedtls/blob/v2.28.1/include/mbedtls/config.h)
	for TLS support
  * an implementation of
    [`FreeRTOSConfig.h`](https://www.freertos.org/a00110.html) for FreeRTOS
	support
  * run time configuration of the [http
	server](https://slimhazard.gitlab.io/picow_http/structserver__cfg.html)
	and [NTP time
	synchronization](https://slimhazard.gitlab.io/picow_http/structntp__cfg.html)
  * some constants in the [API](https://slimhazard.gitlab.io/picow_http/)
	that may be optionally changed from default values

The [`etc/`](etc/) subdirectory of this repository has sample
implementations of [`lwipopts.h`](etc/lwipopts.h),
[`mbedtls_config.h`](etc/mbedtls_config.h) and
[`FreeRTOSConfig.h`](etc/FreeRTOSConfig.h), which can be used as starting points
for a project.

# Running the server

After a [WiFi connection has been
established](https://datasheets.raspberrypi.com/picow/connecting-to-the-internet-with-pico-w.pdf),
the server is started by calling
[`http_srv_init()`](https://slimhazard.gitlab.io/picow_http/group__server.html#gae2b8bdf44100f13cd2c5e18969208ff5):

```c
/* On the include path when picow-http is linked via CMake. */
#include "picow_http/http.h"

int
main(void)
{
	struct server *srv;
	struct server_cfg cfg;

	/*
	 * Connection to WiFi, and any other initialization for your app
	 * here ...
	 */

	/* Get a default configuration for the http server */
	cfg = http_default_cfg();

	/* Start the server */
	if (http_srv_init(&srv, &cfg) != ERR_OK) {
		// Error handling
	}
}
```

For a FreeRTOS implementation, run the WiFi connection and
`http_srv_init()` in a [task](https://www.freertos.org/taskandcr.html):

```c
#include "picow_http/http.h"
#include "FreeRTOS.h"
#include "task.h"

/* Your choice of a priority for the network/http init task here */
#define HTTP_TASK_PRIO (tskIDLE_PRIORITY + 1UL)

/* FreeRTOS stack sizes are expressed in words, not bytes. */
#define PICO_STACK_WORDS (PICO_STACK_SIZE / sizeof(configSTACK_DEPTH_TYPE))

/*
 * Initialization for picow_https, which initializes TLS, requires a larger
 * stack.
 */
#define HTTP_STACK_SIZE (PICO_STACK_WORDS)
#define HTTPS_STACK_SIZE (2 * PICO_STACK_WORDS)

static struct server *srv;

void
initiate_http(void *params)
{
	struct server_cfg cfg;
	(void)params;

	/* Connection to WiFi, etc ... */

	cfg = http_default_cfg();
	if (http_srv_init(&srv, &cfg) != ERR_OK) {
		// Error handling
	}

	vTaskDelete(NULL);
}

int
main(void)
{
	if (xTaskCreate(initiate_http, "http", HTTP_STACK_SIZE, NULL,
			       HTTP_TASK_PRIO, NULL) != pdPASS) {
		// Error handling ...
	}

	/*
	 * ... any other initialization and/or task creation for your
	 * application here ...
	 */

	/* The FreeRTOS task scheduler never exits. */
	vTaskStartScheduler();
}
```

FreeRTOS is then responsible for the lwIP calls that implement the
HTTP server.

To note about starting the server with FreeRTOS:

  * The initialization task that calls `http_srv_init()` runs once and
	returns, and hence is required to call
	[`vTaskDelete(NULL)`](https://www.freertos.org/a00126.html) (to
	delete itself) before it completes.
  * A stack size must be specified at [task
    creation](https://www.freertos.org/a00019.html), which is expressed in
    words rather than bytes. The `PICO_STACK_WORDS` macro shown above
	converts the SDK's default stack size into words.
  * For `picow_https`, a larger stack size is required, due to the
    initialization performed by mbedtls. The stack size shown above as
	`HTTPS_STACK_SIZE` has been tested successfully with `picow_https`.
  * A task priority must be chosen at task creation. Since server
    initialization runs just once, the choice may not be critical.
	The value in `HTTP_TASK_PRIO` above (1 greater than the priority of
	the idle task) is probably sufficient.
  * The `struct server` variable `srv` used as the first parameter of
	`http_srv_init()` is declared in the example above as static, outside
	the scope of the task. Its value may be needed again, if your code will call
	[`http_srv_fini()`](https://slimhazard.gitlab.io/picow_http/group__server.html#ga7e08d490cc241305c09d8646791ab4dd)
	at a later time to stop the server.

Custom response handlers are implemented as functions in your code that
satisfy a
[typedef](https://slimhazard.gitlab.io/picow_http/group__resp.html#ga23afab92dd579b34f1190006b6fa1132),
and are registered at runtime by calling
[`register_hndlr()`](https://slimhazard.gitlab.io/picow_http/group__resp.html#gac4ee42ee6a8559778bb486dcb6253cfe). See the
[Wiki](https://gitlab.com/slimhazard/picow_http/-/wikis/Custom-Handlers)
for details.

# Examples

  * The [walkthrough](https://gitlab.com/slimhazard/picow_http/-/wikis/Example-walkthrough)
	in the [project Wiki](https://gitlab.com/slimhazard/picow_http/-/wikis/home)
	for setting up static content is a good starting point for getting
	to know picow-http
  * See also the Wiki on [implementing custom response
	handlers](https://gitlab.com/slimhazard/picow_http/-/wikis/Custom-Handlers)
  * [picow_http_example](https://gitlab.com/slimhazard/picow-http-example)
	is a complete example project that uses picow-http

# Limitations

  * picow-http currently only supports HTTP version 1.1.
  * With SDK version 1.5.1, support for FreeRTOS is limited to lwIP OS
    mode or "sys" mode (`NO_SYS=0`). "no-sys" mode -- the
    `pico_cyw43_arch_lwip_threadsafe_background` library with lwIP
    `NO_SYS=1`, linked with FreeRTOS -- is supported since SDK 2.0.0.
