/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdint.h>
#include <stddef.h>

#define STATIC_EMBED(file, sym) asm (		\
    ".section .rodata." #sym "           \n"	\
    ".balign 4                           \n"	\
    ".global " #sym "_data               \n"	\
    #sym "_data:                         \n"	\
    ".incbin \"" file "\"                \n"	\
    ".global " #sym "_sz                 \n"	\
    ".set " #sym "_sz, . - " #sym "_data \n"	\
    ".balign 4                           \n"	\
    ".section \".text\"                  \n"	\
)

#define STATIC_DECLARE(sym) extern const uint8_t sym ## _data[], sym ## _sz[]
