/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdint.h>

#include "pico/cyw43_arch.h"

#include "picow_http/http.h"
#include "picow_http/assertion.h"

int
hdr_cmp(struct pbuf *p, const uint8_t *val, uint16_t off, uint16_t len)
{
	AN(p);
	AN(val);

	cyw43_arch_lwip_check();
	for (const uint8_t *b = (const uint8_t *)p->payload + off, *v = val;
	     v < val + len; b++, v++) {
		uint8_t diff = *b ^ *v;
		if (diff == 0)
			continue;
		if (diff == 0x20 && (*v | 0x20) >= 'a' && (*v | 0x20) <= 'z')
			continue;
		return -1;
	}
	return 0;
}

const char *
http_req_hdr(struct req *req, const char *name, size_t name_len,
	     size_t *val_len)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	if (name == NULL || val_len == NULL)
		return NULL;
	*val_len = 0;

	cyw43_arch_lwip_check();
	for (int i = 0; i < req->nhdrs; i++) {
		struct hdr *hdr = &req->hdr[i];

		if (hdr->name.len != name_len)
			continue;
		if (hdr_cmp(req->p, name, hdr->name.off, hdr->name.len) != 0)
			continue;
		*val_len = hdr->val.len;
		return req->p->payload + hdr->val.off;
	}
	return NULL;
}

bool
http_req_hdr_eq(struct req *req, const char *fld, size_t fld_len,
		const char *val, size_t val_len)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	if (fld == NULL || val == NULL)
		return false;

	cyw43_arch_lwip_check();
	for (int i = 0; i < req->nhdrs; i++) {
		struct hdr *hdr = &req->hdr[i];

		if (hdr->name.len != fld_len || hdr->val.len != val_len)
			continue;
		if (hdr_cmp(req->p, fld, hdr->name.off, hdr->name.len) != 0)
			continue;
		if (memcmp(req->p->payload + hdr->val.off, val, val_len) == 0)
			return true;
	}
	return false;
}

bool
http_req_hdr_contains(struct req *req, const char *fld, size_t fld_len,
		      const char *val, size_t val_len)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	if (fld == NULL || val == NULL)
		return false;

	cyw43_arch_lwip_check();
	for (int i = 0; i < req->nhdrs; i++) {
		struct hdr *hdr = &req->hdr[i];
		uint8_t *p, *end;

		if (hdr->name.len != fld_len || hdr->val.len < val_len)
			continue;
		if (hdr_cmp(req->p, fld, hdr->name.off, hdr->name.len) != 0)
			continue;
		p = req->p->payload + hdr->val.off;
		end = p + hdr->val.len;
		do {
			p = memchr(p, *val, end - p);
			if (p == NULL)
				break;
			if (memcmp(p + 1, val + 1, val_len - 1) == 0)
				return true;
			p++;
		} while (p < end);
	}
	return false;
}

err_t
http_req_hdr_iter(struct req *req, name_val_iter_f iter_cb, void *priv)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	AN(iter_cb);

	for (unsigned i = 0; i < req->nhdrs; i++) {
		err_t err;
		struct hdr *hdr = &req->hdr[i];
		const char *p = req->p->payload;

		if ((err = iter_cb(p + hdr->name.off, (size_t)(hdr->name.len),
				   p + hdr->val.off, (size_t)(hdr->val.len),
				   priv)) != ERR_OK)
			return err;
	}
	return ERR_OK;
}

const uint8_t *
http_req_query_val(const uint8_t *query, size_t query_len, const uint8_t *name,
		   size_t name_len, size_t *val_len)
{
	const uint8_t * const end = query + query_len;

	if (query == NULL || name == NULL || val_len == NULL)
		return NULL;
	*val_len = 0;
	for (const uint8_t *p = query; p < end; p++) {
		const uint8_t *name_end;
		const uint8_t *val;

		p = memchr(p, *name, end - p);
		if (p == NULL)
			break;
		if (p != query && *(p - 1) != '&')
			/* Not at the first char in a name. */
			continue;
		name_end = p + name_len;
		if (name_end + 1 >= end)
			/* "name=" cannot be at the end */
			return NULL;
		if (*name_end != '=')
			continue;
		if (memcmp(p + 1, name + 1, name_len - 1) != 0)
			continue;

		/* Found it */
		val = name_end + 1;
		p = memchr(val, '&', end - val);
		if (p != NULL)
			*val_len = p - val;
		else
			*val_len = end - val;
		return val;
	}
	return NULL;
}

err_t
http_req_query_iter(const uint8_t *query, size_t len, name_val_iter_f iter_cb,
		    void *priv)
{
	AN(iter_cb);

	if (query == NULL || len == 0)
		return ERR_ARG;
	for (const uint8_t *q = query, *end = q + len; q < end;) {
		err_t err;
		const uint8_t *name_end, *val_start, *val_end;

		val_end = memchr(q, '&', end - q);
		if (val_end == NULL)
			val_end = end;
		name_end = memchr(q, '=', val_end - q);
		if (name_end != NULL)
			val_start = name_end + 1;
		else
			name_end = val_start = val_end;
		if ((err = iter_cb(q, name_end - q, val_start,
				   val_end - val_start, priv)) != ERR_OK)
			return err;
		q = val_end + 1;
	}
	return ERR_OK;
}

static inline bool
hdr_is_cookie(struct req *req, struct hdr *hdr)
{
	if (hdr->name.len != STRLEN_LTRL("Cookie"))
		return false;
	return (hdr_cmp(req->p, "Cookie", hdr->name.off, hdr->name.len) == 0);
}

static inline const uint8_t *
cookie_delims(const uint8_t *p, const uint8_t *end, const uint8_t **name_end,
	      const uint8_t **val_end)
{
	while (http_ows(*p))
		p++;
	*val_end = memchr(p, ';', end - p);
	if (*val_end == NULL)
		*val_end = end;
	*name_end = memchr(p, '=', *val_end - p);
	if (*name_end == NULL)
		*name_end = *val_end;
	return p;
}

const uint8_t *
http_req_cookie(struct req *req, const char *name, size_t name_len,
		size_t *val_len)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	if (val_len == NULL)
		return NULL;
	*val_len = 0;
	if (name == NULL)
		return NULL;

	cyw43_arch_lwip_check();
	for (unsigned i = 0; i < req->nhdrs; i++) {
		struct hdr *hdr = &req->hdr[i];

		if (!hdr_is_cookie(req, hdr))
			continue;
		if (hdr->val.len < name_len)
			continue;

		for (const uint8_t *p = req->p->payload + hdr->val.off,
			     *end = p + hdr->val.len; p < end;) {
			const uint8_t *name_end, *val_end;

			p = cookie_delims(p, end, &name_end, &val_end);
			if (name_end - p != name_len ||
			    memcmp(p, name, name_len) != 0) {
				p = val_end + 1;
				continue;
			}

			if (name_end == val_end)
				return "";
			*val_len = val_end - (name_end + 1);
			return name_end + 1;
		}
	}

	return NULL;
}

err_t
http_req_cookie_iter(struct req *req, name_val_iter_f iter_cb, void *priv)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);

	cyw43_arch_lwip_check();
	for (unsigned i = 0; i < req->nhdrs; i++) {
		struct hdr *hdr = &req->hdr[i];

		if (!hdr_is_cookie(req, hdr))
			continue;
		for (const uint8_t *p = req->p->payload + hdr->val.off,
			     *end = p + hdr->val.len; p < end;) {
			const uint8_t *name_end, *val_end, *val;
			size_t val_len;
			err_t err;

			p = cookie_delims(p, end, &name_end, &val_end);
			if (name_end == val_end) {
				val = "";
				val_len = 0;
			}
			else {
				val = name_end + 1;
				val_len = val_end - (name_end + 1);
			}
			if ((err = iter_cb(p, name_end - p, val, val_len, priv))
			    != ERR_OK)
				return err;
			p = val_end + 1;
		}
	}
	return ERR_OK;
}

static inline ssize_t
body_rcvd(struct req *req)
{
	return req->p->tot_len - body_off(req);
}

err_t
http_req_body(struct http *http, uint8_t *buf, size_t *len, size_t off)
{
	struct req *req;
	uint16_t n;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->req, REQ_MAGIC);
	req = &http->req;
	AN(req->p);

	if (http->state < HTTP_STATE_HDR_PARSED)
		return ERR_INPROGRESS;
	if (buf == NULL || len == NULL)
		return ERR_ARG;
	if (req->clen == 0) {
		*len = 0;
		return ERR_OK;
	}
	if (off > req->clen)
		return ERR_VAL;

	if (body_rcvd(req) < off + *len) {
		if (body_rcvd(req) - off < 0)
			return ERR_INPROGRESS;
		*len = body_rcvd(req) - off;
	}
	if (*len == 0)
		return ERR_OK;
	n = pbuf_copy_partial(req->p, buf, *len, body_off(req) + off);
	if (n == 0)
		return ERR_BUF;
	return ERR_OK;
}

err_t
http_req_body_ptr(struct http *http, const uint8_t **buf, size_t *len,
		  size_t off)
{
	struct req *req;
	struct pbuf *q;
	uint16_t out_off, body_offf;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->req, REQ_MAGIC);
	req = &http->req;
	AN(req->p);

	if (buf == NULL || len == NULL)
		return ERR_ARG;
	if (http->state < HTTP_STATE_HDR_PARSED)
		return ERR_INPROGRESS;
	if (req->clen == 0) {
		*len = 0;
		return ERR_OK;
	}
	if (off > req->clen)
		return ERR_VAL;
	if (body_rcvd(req) < off)
		return ERR_INPROGRESS;

	body_offf = body_off(req) + off;
	q = pbuf_skip(req->p, body_offf, &out_off);
	if (q == NULL)
		return ERR_INPROGRESS;
	PICOW_HTTP_ASSERT(q->len >= out_off);
	*buf = (const uint8_t *)(q->payload + out_off);
	*len = q->len - out_off;
	return ERR_OK;
}

err_t
http_req_chunk(struct http *http, uint8_t *buf, size_t *len)
{
	struct req *req;
	uint64_t n;
	uint16_t sz_end, sz_sz;
	err_t err;
	char szbuf[4];

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->req, REQ_MAGIC);
	req = &http->req;
	AN(req->p);

	if (http->state < HTTP_STATE_HDR_PARSED)
		return ERR_INPROGRESS;
	if (buf == NULL || len == NULL)
		return ERR_ARG;

	sz_end = pbuf_memfind(req->p, "\r\n", 2, req->chunk_off);
	if (sz_end == PBUF_NOT_FOUND)
		return ERR_INPROGRESS;
	sz_sz = sz_end - req->chunk_off;
	if (sz_sz > 4)
		return ERR_MEM;
	if (pbuf_copy_partial(req->p, szbuf, sz_sz, req->chunk_off) != sz_sz)
		return ERR_BUF;
	if ((err = hex_decode(szbuf, sz_sz, &n)) != ERR_OK) {
		PICOW_HTTP_ASSERT(err == ERR_VAL);
		return ERR_VAL;
	}
	PICOW_HTTP_ASSERT(n <= UINT16_MAX);
	if (n > *len)
		return ERR_MEM;
	if (req->p->tot_len < sz_end + n + 3)
		return ERR_INPROGRESS;
	if (pbuf_get_at(req->p, sz_end + n + 2) != '\r' ||
	    pbuf_get_at(req->p, sz_end + n + 3) != '\n')
		return ERR_VAL;
	*len = n;
	if (n == 0)
		return ERR_OK;
	if (pbuf_copy_partial(req->p, buf, n, sz_end + 2) != n)
		return ERR_BUF;
	req->chunk_off += sz_sz + n + 4;
	return ERR_OK;
}
