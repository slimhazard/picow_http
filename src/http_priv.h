/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#ifndef _PICOW_HTTP_PRIV_H
#define _PICOW_HTTP_PRIV_H

#include <stdint.h>
#include <stddef.h>

#include "pico/time.h"

#include "lwip/pbuf.h"
#include "lwip/altcp.h"
#include "lwip/memp.h"

#ifdef MAX_CONCURRENT_CX_HINT
#define HTTP_N	(MAX_CONCURRENT_CX_HINT)
#elif defined(MEMP_NUM_TCP_PCB)
#define HTTP_N	(MEMP_NUM_TCP_PCB)
#else
#define HTTP_N	(6)
#endif

#ifdef NUM_SERVER_HINT
#define SRV_N	(NUM_SERVER_HINT)
#elif defined(MEMP_NUM_TCP_PCB_LISTEN)
#define SRV_N	(MEMP_NUM_TCP_PCB_LISTEN)
#else
#define SRV_N	(1)
#endif

#define PBUF_NOT_FOUND (0xffff)

enum http_state_t {
	HTTP_STATE_IDLE,
	HTTP_STATE_HDR_RX,
	HTTP_STATE_HDR_PARSED,
	HTTP_STATE_HDR_PARSE_FAIL,
	HTTP_STATE_CLOSING,
	HTTP_STATE_TX,
};

struct txt {
	uint16_t off;
	uint16_t len;
};

struct hdr {
	struct txt name;
	struct txt val;
};

struct msg {
	uint8_t			*buf;
	const struct memp_desc	*pool;
	size_t			len;
	size_t			queued;
};

struct req {
	unsigned		magic;
#define REQ_MAGIC 0xb3eab4ba
	struct pbuf		*p;
	struct hdr		hdr[HTTP_REQ_MAX_HDRS];
	datetime_t		recv_t;
	struct txt		path;
	struct txt		query;
	size_t			recvd;
	uint16_t		req_line_end;
	uint16_t		hdr_end;
	uint16_t		clen;
	uint16_t		chunk_off;
	enum http_method_t	method;
	uint8_t			nhdrs;
	bool			close:1;
	bool			host:1;
};

struct resp {
	unsigned	magic;
#define RESP_MAGIC 0xf5fe485d
	struct msg	hdr;
	struct msg	body;
	size_t		ack_pending;
	uint8_t		*hdr_cur;
	absolute_time_t	send_t;
	uint16_t	status;
	bool		alloc:1;
	bool		hdr_only:1;
};

struct static_data {
	const uint8_t	*ptr;
	const size_t    sz;
};

struct cert_data {
	const unsigned			magic;
#define CERT_DATA_MAGIC 0x5bb471f1
	const struct static_data	cert;
	const struct static_data	key;
	const struct static_data	pass;
};

struct priv_data {
	void		*priv;
	priv_fini_f	*fini;
};

struct srv_data {
	const unsigned		magic;
#define SRV_DATA_MAGIC 0xd8151c07
	const char		*name;
	const struct cert_data	*cert_data;
	const uint8_t		*err_hndlr_status;
	const int		idx;
	struct priv_data	default_hndlr_priv;
	struct priv_data	err_hndlr_priv;
	void			*srv_priv;
	hndlr_f			default_hndlr;
	hndlr_f			err_hndlr;
};

struct http {
	unsigned		magic;
#define HTTP_MAGIC 0x900aca28
	struct req		req;
	struct resp		resp;
	struct priv_data	cx_priv;
	struct altcp_pcb	*pcb;
	struct srv_data		*srv_data;
	absolute_time_t		idle_t;
	int64_t			idle_tmo_us;
	int64_t			send_tmo_us;
	enum http_state_t	state;
};

struct hndlrs {
	void		*priv;
	const char	*allow;
	const size_t	allow_sz;
	const uint8_t	method_bitmap;
	hndlr_f		method_hndlr[];
};

struct static_hndlr_cfg {
	const unsigned			magic;
#define STATIC_CFG_MAGIC 0x2e743d8a
	const struct static_data	id;
	const struct static_data	gzip;
	const struct static_data	br;
	const struct static_data	ct;
	const struct static_data	etag;
	const struct static_data	cctrl;
};

LWIP_MEMPOOL_PROTOTYPE(resp_hdr_small_pool);
LWIP_MEMPOOL_PROTOTYPE(resp_hdr_large_pool);

/*
 * HTTP OWS -- optional whitespace
 * RFC 9110 section 5.6.3
 */
static inline bool
http_ows(char c)
{
	return c == ' ' || c == '\t';
}

static inline ip_addr_t *
http_remote_ip(struct altcp_pcb *pcb)
{
	return altcp_get_ip(pcb, false);
}

static inline uint16_t
http_remote_port(struct altcp_pcb *pcb)
{
	return altcp_get_port(pcb, false);
}

static inline ip_addr_t *
http_local_ip(struct altcp_pcb *pcb)
{
	return altcp_get_ip(pcb, true);
}

static inline uint16_t
http_local_port(struct altcp_pcb *pcb)
{
	return altcp_get_port(pcb, true);
}

static inline uint16_t
body_off(struct req *req)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	return req->hdr_end + sizeof("\r\n\r\n") - 1;
}

struct hndlr_map * has_hndlr (register const char *str, register size_t len);
struct srv_data * http_srv_data(const char *srv);

int hdr_cmp(struct pbuf *p, const uint8_t *val, uint16_t off, uint16_t len);

err_t http_resp_fini(struct http *http);
void resp_free_buf(struct msg *msg);
void resp_format_status(struct resp *resp, uint8_t *dst);

hndlr_f get_hndlr(struct http *http, void **priv);
err_t http_hndlr_static(struct http *http, void *priv);
err_t http_hndlr_err(struct http *http, void *priv);

#endif /* _PICOW_HTTP_PRIV_H */
