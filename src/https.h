/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "lwip/altcp.h"
#include "picow_http/http.h"

int get_allocator(const struct cert_data *data, altcp_allocator_t *alloc,
		  altcp_allocator_t **allocp, bool tls);

void free_tls_conf(void *p);
