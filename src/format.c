/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdint.h>

#include "hardware/divider.h"

#include "picow_http/http.h"
#include "picow_http/assertion.h"

#include "lwip/err.h"

#define MAX_NUM_LEN (STRLEN_LTRL("-2147483648"))
#define NIBBLE_MASK ((uint32_t)0xf0000000)
#define ILL ((int8_t)127)

static const char hex_alphabet[][16] = {
        "0123456789abcdef",
        "0123456789ABCDEF"
};

/* URL decoder states */
enum decode_state_e {
	NORMAL,
	PERCENT,  /* after reading '%' */
	FIRSTNIB, /* after reading the first nibble after '%' */
};

/*
 * For url_decode() and hex_decode()
 * Shift the ASCII table over so that it begins at '0', and replace the
 * hex digits with their binary values.
 */
static const uint8_t hex_nibble[] = {
	0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, 10,  11,  12,
	13,  14,  15,  ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL,
	ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL, ILL, 10,
	11,  12,  13,  14,  15
};

err_t
format_decimal(char *s, size_t *len, int32_t n)
{
	divmod_result_t div;
	char *p = s;
	uint32_t nn = (uint32_t)n;
	static const uint32_t decade[] = {
		1000 * 1000 * 1000, 100 * 1000 * 1000, 10 * 1000 * 1000,
		1000 * 1000, 100 * 1000, 10 * 1000, 1000, 100, 10,
	};
	static const size_t n_decade = 9;
	int idx;

	if (s == NULL || len == NULL)
		return ERR_ARG;
	if (*len == 0)
		return ERR_OK;

	if (n < 0) {
		if (n == INT32_MIN) {
			if (*len < MAX_NUM_LEN)
				return ERR_BUF;
			memcpy(s, "-2147483648", MAX_NUM_LEN);
			*len = MAX_NUM_LEN;
			return ERR_OK;
		}
		nn = (uint32_t)(-n);
		*p = '-';
		p++;
	}

	idx = n_decade - ilog10(nn | 1);
	if (n_decade - idx + (p - s) >= *len)
		return ERR_BUF;

	div = hw_divider_divmod_u32(nn, decade[idx]);
	while (idx < n_decade) {
		idx++;
		hw_divider_divmod_u32_start(to_remainder_u32(div), decade[idx]);
		*p = to_quotient_u32(div) + '0';
		p++;
		div = hw_divider_result_wait();
	}

	PICOW_HTTP_ASSERT(p < s + *len && to_remainder_u32(div) < 10);
	*p = to_remainder_u32(div) + '0';
	*len = p + 1 - s;
	return ERR_OK;
}

err_t
format_hex(char *s, size_t *len, uint32_t n, bool upper)
{
	char *p = s;
	const char * const alpha = hex_alphabet[bool_to_bit(upper)];
	unsigned nibbles;

	if (s == NULL || len == NULL)
		return ERR_ARG;
	/* Declaration here after NULL check */
	const char * const end = p + *len;

	if (*len == 0)
		return ERR_OK;
	if (n == 0) {
		*p = '0';
		*len = 1;
		return ERR_OK;
	}

	nibbles = (ilog2(n) + 4) >> 2;
	AN(nibbles);
	for (n <<= ((8 - nibbles) << 2); nibbles > 0 && p < end;
	     n <<= 4, nibbles--, p++)
		*p = alpha[(n & NIBBLE_MASK) >> 28];
	if (nibbles != 0)
		return ERR_BUF;
	*len = p - s;
	return ERR_OK;
}

err_t
url_decode(char * restrict const out, size_t *outlen,
	   const char * restrict const in, ssize_t inlen, bool plus)
{
	char *dst = out;
	size_t len = SIZE_MAX;
	enum decode_state_e state = NORMAL;
	uint8_t nib_hi, nib_lo;

	if (in == NULL || out == NULL || outlen == NULL)
		return ERR_ARG;

	const char * const end = out + *outlen;
	if (inlen >= 0)
		len = inlen;

	for (const char *c = in; *c && len; c++, len--) {
		switch (state) {
		case NORMAL:
			if (*c == '%') {
				state = PERCENT;
				continue;
			}
			if (dst == end)
				return ERR_BUF;
			if (plus && *c == '+')
				*dst++ = ' ';
			else
				*dst++ = *c;
			break;
		case PERCENT:
			if (*c < '0' || *c > 'f'
			    || (nib_hi = hex_nibble[*c - '0']) == ILL)
				return ERR_VAL;
			state = FIRSTNIB;
			break;
		case FIRSTNIB:
			if (dst == end)
				return ERR_BUF;
			if (*c < '0' || *c > 'f'
			    || (nib_lo = hex_nibble[*c - '0']) == ILL)
				return ERR_VAL;
			*dst++ = (nib_hi << 4) | nib_lo;
			state = NORMAL;
			break;
		default:
			panic("illegal URL decode state");
		}
	}

	if (state != NORMAL)
		return ERR_VAL;

	PICOW_HTTP_ASSERT(dst <= end);
	*outlen = dst - out;
	return ERR_OK;
}

err_t
hex_decode(const char * const buf, ssize_t len, uint64_t *n)
{
	if (buf == NULL || n == NULL)
		return ERR_ARG;
	*n = 0;
	if (len == 0)
		return ERR_OK;
	if (len < 0)
		len = SIZE_MAX;

	for (unsigned const char *c = buf; *c && len; c++, len--) {
		uint8_t val;

		if (*n >= (1ULL << 60))
			return ERR_ARG;
		if (*c < '0' || *c > 'f' ||
		    (val = hex_nibble[*c - '0']) == ILL)
			return ERR_VAL;
		*n <<= 4;
		*n += val;
	}
	return ERR_OK;
}
