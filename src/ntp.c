/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <time.h>
#include <string.h>
#include <inttypes.h>

#include "hardware/rtc.h"
#include "hardware/clocks.h"

#include "pico/cyw43_arch.h"
#include "pico/async_context.h"

#include "lwip/pbuf.h"
#include "lwip/udp.h"
#include "lwip/dns.h"

#include "picow_http/assertion.h"
#include "picow_http/ntp.h"
#include "picow_http/log.h"

#define NTP_MSG_LEN		(48)
#define NTP_PORT		(123)
#define NTP_DELTA		(2208988800)
#define NTP_XMIT_TS_OFFSET	(40)
#define NTP_REF_ID_OFFSET	(12)
#define NTP_KOD_STRATUM		(0)

#define MIN_REFRESH_INTVL_S	(64)

#define SECS2MS(s)		(s * 1000)

enum ntp_state_t {
	INIT,
	DNS_PENDING,
	DNS_RCVD,
	DNS_RETRY,
	NTP_PENDING,
	NTP_SYNCED,
	NTP_INVALID,
};

struct ntp_hndl_t {
	unsigned		magic;
#define NTP_HNDL_MAGIC (0x64f80f99)
	char			**hosts;
	struct udp_pcb		*pcb;
	async_context_t		*ctx;
	async_at_time_worker_t	*wrk;
	ip_addr_t		addr;
	enum ntp_state_t	state;
	unsigned		response_tmo_s;
	unsigned		retry_intvl_s;
	unsigned		dns_tmo_s;
	unsigned		refresh_intvl_s;
	unsigned		nhosts;
	unsigned		host_idx;
} ntp_hndl;

static bool inited = false;

static void ntp_work(async_context_t *context, async_at_time_worker_t *worker);
static async_at_time_worker_t ntp_worker = {
	.do_work = ntp_work,
	.user_data = &ntp_hndl,
};

static const char *ws = " \t\n\v\f\r";

static inline void
ntp_bump_host_idx(struct ntp_hndl_t *hndl)
{
	CHECK_OBJ_NOTNULL(hndl, NTP_HNDL_MAGIC);
	hndl->host_idx++;
	if (hndl->host_idx >= hndl->nhosts)
		hndl->host_idx = 0;
}

static inline void
ntp_cancel_wrk(struct ntp_hndl_t *hndl)
{
	CHECK_OBJ_NOTNULL(hndl, NTP_HNDL_MAGIC);
	AN(hndl->ctx);
	AN(hndl->wrk);
	(void)async_context_remove_at_time_worker(hndl->ctx, hndl->wrk);
}

static inline void
ntp_abort_udp(struct ntp_hndl_t *hndl)
{
	CHECK_OBJ_NOTNULL(hndl, NTP_HNDL_MAGIC);
	if (hndl->pcb == NULL)
		return;
	cyw43_arch_lwip_begin();
	udp_remove(hndl->pcb);
	cyw43_arch_lwip_end();
	hndl->pcb = NULL;
}

static err_t
ntp_request(struct ntp_hndl_t *hndl)
{
	err_t ret;
	struct pbuf *p;
	uint8_t *req;

	CHECK_OBJ_NOTNULL(hndl, NTP_HNDL_MAGIC);
	AN(hndl->pcb);
	cyw43_arch_lwip_begin();
	if ((p = pbuf_alloc(PBUF_TRANSPORT, NTP_MSG_LEN, PBUF_RAM)) == NULL) {
		cyw43_arch_lwip_end();
		return ERR_MEM;
	}
	req = (uint8_t *) p->payload;
	memset(req, 0, NTP_MSG_LEN);
	req[0] = 0x1b;
	ret = udp_sendto(hndl->pcb, p, &hndl->addr, NTP_PORT);
	pbuf_free(p);
	cyw43_arch_lwip_end();

	return ret;
}

static void
ntp_recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr,
	 u16_t port)
{
	struct ntp_hndl_t *hndl = arg;
	uint8_t mode = pbuf_get_at(p, 0) & 0x7;
	uint8_t stratum = pbuf_get_at(p, 1);

	CHECK_OBJ_NOTNULL(hndl, NTP_HNDL_MAGIC);
	PICOW_HTTP_ASSERT(pcb == hndl->pcb);
	ntp_cancel_wrk(hndl);
	if (stratum == NTP_KOD_STRATUM) {
		char ref_id[5];

		pbuf_copy_partial(p, ref_id, 4, NTP_REF_ID_OFFSET);
		ref_id[4] = '\0';
		HTTP_LOG_WARN("NTP KoD received from %s: %s",
			      hndl->hosts[hndl->host_idx], ref_id);
		hndl->state = NTP_INVALID;
		/* XXX remove the host on DENY or RSTR, slow down on RATE */
	}
	else if (ip_addr_eq(addr, &hndl->addr) && port == NTP_PORT &&
		 p->tot_len == NTP_MSG_LEN && mode == 0x4) {
		uint8_t secs_buf[4] = {0};
		uint32_t ntpt;
		struct tm gt;
		datetime_t t;
		time_t epoch;
		struct timeval tv = { 0 };

		pbuf_copy_partial(p, secs_buf, sizeof(secs_buf),
				  NTP_XMIT_TS_OFFSET);
		ntpt = secs_buf[0] << 24 | secs_buf[1] << 16 |
			secs_buf[2] << 8 | secs_buf[3];
		epoch = ntpt - NTP_DELTA;
		gmtime_r(&epoch, &gt);

		t.sec = gt.tm_sec;
		t.min = gt.tm_min;
		t.hour = gt.tm_hour;
		t.day = gt.tm_mday;
		t.month = gt.tm_mon + 1;
		t.year = gt.tm_year + 1900;
		t.dotw = gt.tm_wday;
		AN(rtc_set_datetime(&t));
		tv.tv_sec = epoch;
		AZ(settimeofday(&tv, NULL));
		HTTP_LOG_DEBUG("NTP synced, epoch=%jd time()=%jd",
			       (intmax_t)epoch, (intmax_t)time(NULL));
		hndl->state = NTP_SYNCED;
	}
	else
		hndl->state = NTP_INVALID;

	udp_remove(pcb);
	hndl->pcb = NULL;
	pbuf_free(p);
	/*
	 * XXX background mode does not reschedule the worker if the
	 * timeouts are set here directly, also not if this timeout is set
	 * to 0. The 1 ms timeout seems to work as a workaround, but it's
	 * not clear why.
	 */
	PICOW_HTTP_ASSERT(async_context_add_at_time_worker_in_ms(
				  hndl->ctx, hndl->wrk, 1));
}

static err_t
ntp_initiate(struct ntp_hndl_t *hndl)
{
	CHECK_OBJ_NOTNULL(hndl, NTP_HNDL_MAGIC);
	if (hndl->pcb == NULL) {
		hndl->pcb = udp_new_ip_type(IPADDR_TYPE_ANY);
		if (hndl->pcb == NULL)
			return ERR_MEM;
	}
	cyw43_arch_lwip_begin();
	udp_recv(hndl->pcb, ntp_recv, hndl);
	cyw43_arch_lwip_end();

	return ntp_request(hndl);
}

static void
dns_found_cb(const char *name, const ip_addr_t *ipaddr, void *arg)
{
	struct ntp_hndl_t *hndl = arg;
	uint32_t tmo_s;

	CHECK_OBJ_NOTNULL(hndl, NTP_HNDL_MAGIC);
	ntp_cancel_wrk(hndl);

	if (ipaddr == NULL) {
		AN(name);
		HTTP_LOG_ERROR("DNS lookup failed for NTP server: %s", name);
		hndl->state = INIT;
		tmo_s = hndl->retry_intvl_s;
		ntp_bump_host_idx(hndl);
	}
	else {
		hndl->addr = *ipaddr;
		hndl->state = DNS_RCVD;
		tmo_s = 0;
	}
	(void)async_context_add_at_time_worker_in_ms(
		hndl->ctx, hndl->wrk, SECS2MS(tmo_s));
}

static err_t
dns_initiate(struct ntp_hndl_t *hndl)
{
	err_t err;

	CHECK_OBJ_NOTNULL(hndl, NTP_HNDL_MAGIC);
	cyw43_arch_lwip_begin();
	err = dns_gethostbyname(hndl->hosts[hndl->host_idx], &hndl->addr,
				dns_found_cb, hndl);
	cyw43_arch_lwip_end();
	return err;
}

static void
ntp_work(async_context_t *ctx, async_at_time_worker_t *wrk)
{
	struct ntp_hndl_t *hndl;
	uint32_t tmo_s;
	err_t err;

	AN(ctx);
	AN(wrk);
	hndl = wrk->user_data;
	CHECK_OBJ_NOTNULL(hndl, NTP_HNDL_MAGIC);
	PICOW_HTTP_ASSERT(hndl->ctx == ctx);
	PICOW_HTTP_ASSERT(hndl->wrk == wrk);

	switch(hndl->state) {
	case INIT:
		err = dns_initiate(hndl);
		switch(err) {
		case ERR_OK:
			tmo_s = 0;
			hndl->state = DNS_RCVD;
			break;
		case ERR_INPROGRESS:
			tmo_s = hndl->dns_tmo_s;
			hndl->state = DNS_PENDING;
			break;
		default:
			tmo_s = hndl->retry_intvl_s;
			HTTP_LOG_ERROR("dns_gethostbyname(\"%s\") error %d, "
				       "retrying after %d seconds",
				       hndl->hosts[hndl->host_idx], err, tmo_s);
			hndl->state = DNS_RETRY;
			break;
		}
		break;
	case DNS_PENDING:
		HTTP_LOG_ERROR("DNS lookup timeout, retrying");
	case DNS_RETRY:
		/* DNS retry interval elapsed. */
		tmo_s = 0;
		hndl->state = INIT;
		ntp_bump_host_idx(hndl);
		break;
	case DNS_RCVD:
		/* DNS resolution was successful */
		err = ntp_initiate(hndl);
		if (err == ERR_OK) {
			tmo_s = hndl->response_tmo_s;
			hndl->state = NTP_PENDING;
		}
		else {
			tmo_s = hndl->retry_intvl_s;
			HTTP_LOG_ERROR("NTP request error %d, "
				       "retrying after %d seconds", err, tmo_s);
			hndl->state = INIT;
			ntp_bump_host_idx(hndl);
		}
		break;
	case NTP_PENDING:
		ntp_abort_udp(hndl);
		HTTP_LOG_ERROR("NTP response timeout, retrying");
		tmo_s = 0;
		hndl->state = INIT;
		ntp_bump_host_idx(hndl);
		break;
	case NTP_INVALID:
		tmo_s = hndl->retry_intvl_s;
		HTTP_LOG_ERROR("NTP response was invalid, retrying after %d "
			       "seconds", tmo_s);
		hndl->state = INIT;
		ntp_bump_host_idx(hndl);
		break;
	case NTP_SYNCED:
		/* NTP sync succeeded */
		tmo_s = hndl->refresh_intvl_s;
		hndl->state = INIT;
		ntp_bump_host_idx(hndl);
		break;
	default:
		HTTP_LOG_ERROR("Illegal NTP sync state: %d", hndl->state);
		tmo_s = hndl->retry_intvl_s;
		hndl->state = INIT;
		break;
	}
	PICOW_HTTP_ASSERT(async_context_add_at_time_worker_in_ms(
				  hndl->ctx, hndl->wrk, SECS2MS(tmo_s)));
}

bool
rtc_synced(void)
{
	static bool synced = false;
	datetime_t t;

	if (!synced) {
		PICOW_HTTP_ASSERT(rtc_get_datetime(&t));
		synced = t.year != 0;
	}
	return synced;
}

void
time_init(struct ntp_cfg *cfg)
{
	static bool first = true;

	if (first) {
		datetime_t init = { 0 };

		/* Run RTC from XOSC */
		clock_configure(clk_rtc, 0,
				CLOCKS_CLK_RTC_CTRL_AUXSRC_VALUE_XOSC_CLKSRC,
				XOSC_MHZ * MHZ,	46875);
		rtc_init();
		init.month = 1;
		init.day = 1;
		PICOW_HTTP_ASSERT(rtc_set_datetime(&init));

		INIT_OBJ(&ntp_hndl, NTP_HNDL_MAGIC);
		first = false;
	}

	if (inited) {
		HTTP_LOG_DEBUG("NTP already initialized");
		return;
	}

	if (ntp_hndl.pcb != NULL) {
		ntp_cancel_wrk(&ntp_hndl);
		ntp_abort_udp(&ntp_hndl);
	}
	ntp_hndl.pcb = NULL;

	ntp_hndl.hosts = NULL;
	ntp_hndl.nhosts = 0;
	ntp_hndl.host_idx = 0;
	for (const char *p = cfg->servers;;) {
		size_t l = strspn(p, ws);
		p += l;
		if (*p == '\0')
			break;
		l = strcspn(p, ws);
		AN(l);
		ntp_hndl.nhosts++;
		ntp_hndl.hosts =
			realloc(ntp_hndl.hosts,
				ntp_hndl.nhosts * sizeof(*ntp_hndl.hosts));
		ntp_hndl.hosts[ntp_hndl.nhosts - 1] = strndup(p, l);
		p += l;
	}
	if (ntp_hndl.nhosts == 0) {
		AZ(ntp_hndl.hosts);
		HTTP_LOG_ERROR("No hosts specified in NTP configuration, "
			       "no time synchonization will be attempted.");
		return;
	}

	ntp_hndl.response_tmo_s = cfg->response_tmo_s;
	ntp_hndl.dns_tmo_s = cfg->dns_tmo_s;
	ntp_hndl.retry_intvl_s = cfg->retry_intvl_s;
	ntp_hndl.refresh_intvl_s = cfg->refresh_intvl_s;
	ntp_hndl.dns_tmo_s = cfg->dns_tmo_s;
	if (ntp_hndl.refresh_intvl_s < MIN_REFRESH_INTVL_S) {
		HTTP_LOG_WARN("NTP refresh interval (%d secs) < minimum (%d), "
			      "setting to minimum", ntp_hndl.refresh_intvl_s,
			      MIN_REFRESH_INTVL_S);
		ntp_hndl.refresh_intvl_s = MIN_REFRESH_INTVL_S;
	}

	ntp_hndl.wrk = &ntp_worker;
	ntp_hndl.ctx = cyw43_arch_async_context();

	ntp_hndl.state = INIT;
	PICOW_HTTP_ASSERT(async_context_add_at_time_worker_in_ms(
				  ntp_hndl.ctx, ntp_hndl.wrk, 0));
	inited = true;
}

void
time_fini(void)
{
	CHECK_OBJ_NOTNULL(&ntp_hndl, NTP_HNDL_MAGIC);
	ntp_cancel_wrk(&ntp_hndl);
	ntp_abort_udp(&ntp_hndl);
	inited = false;
	for (int i = 0; i < ntp_hndl.nhosts; i++)
		free(ntp_hndl.hosts[i]);
	free(ntp_hndl.hosts);
	ntp_hndl.hosts = NULL;
	ntp_hndl.nhosts = 0;
}
