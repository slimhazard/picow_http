/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <inttypes.h>

#include "picow_http/http.h"
#include "picow_http/assertion.h"
#include "picow_http/ntp.h"
#include "hndlr_dispatch.h"

#define HTTP_A_E_LEN (STRLEN_LTRL("Accept-Encoding"))

hndlr_f
get_hndlr(struct http *http, void **priv)
{
	const struct hndlr_map *map;
	struct srv_data *srv_data;
	hndlr_f hndlr;
	struct hndlrs * hndlrs;
	struct txt *path;
	int idx = 0;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(http->srv_data, SRV_DATA_MAGIC);
	srv_data = http->srv_data;
	AN(priv);
	*priv = NULL;

	if (http->resp.status != HTTP_STATUS_OK)
		return http_hndlr_err;
	path = &http->req.path;
	map = has_hndlr((const char *)(http->req.p->payload + path->off),
			(size_t)path->len);
	if (map == NULL) {
		if (srv_data->default_hndlr != NULL) {
			*priv = srv_data->default_hndlr_priv.priv;
			return srv_data->default_hndlr;
		}
		http->resp.status = HTTP_STATUS_NOT_FOUND;
		return http_hndlr_err;
	}
	AN(map->hndlrs);
	hndlrs = map->hndlrs[srv_data->idx];
	if (hndlrs == NULL) {
		/* The map result was for another server. */
		if (srv_data->default_hndlr != NULL) {
			*priv = srv_data->default_hndlr_priv.priv;
			return srv_data->default_hndlr;
		}
		http->resp.status = HTTP_STATUS_NOT_FOUND;
		return http_hndlr_err;
	}
	if ((hndlrs->method_bitmap & (1U << http->req.method)) == 0) {
		err_t err;

		CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);
		err = http_resp_set_hdr(
			&http->resp, "Allow", STRLEN_LTRL("Allow"),
			hndlrs->allow, hndlrs->allow_sz);
		if (err != ERR_OK) {
			HTTP_LOG_ERROR("Error setting Allow: %d");
			http->resp.status = HTTP_STATUS_INTERNAL_SERVER_ERROR;
		}
		else
			http->resp.status = HTTP_STATUS_METHOD_NOT_ALLOWED;
		return http_hndlr_err;
	}
	*priv = hndlrs->priv;
	for (int i = 0; i < http->req.method; i++)
		if ((1U << i) & hndlrs->method_bitmap)
			idx++;
	hndlr = hndlrs->method_hndlr[idx];
	if (hndlr == NULL) {
		/* The handler was configured, but not registered. */
		http->resp.status = HTTP_STATUS_NOT_IMPLEMENTED;
		return http_hndlr_err;
	}
	return hndlr;
}

struct srv_data * const
http_srv_data(const char *name)
{
	for (unsigned i = 0; i < http_n_srv; i++)
		if (strcmp(name, servers[i]->name) == 0)
			return servers[i];
	return NULL;
}

err_t
http_hndlr_static(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	const struct static_hndlr_cfg *cfg;
	const uint8_t *start;
	size_t sz;
	err_t err;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);
	CAST_OBJ_NOTNULL(cfg, priv, STATIC_CFG_MAGIC);
	AN(cfg->etag.ptr);
	AN(cfg->cctrl.ptr);
	AN(cfg->ct.ptr);
	req = http_req(http);
	resp = &http->resp;

	if ((err = http_resp_set_hdr(resp, "ETag", STRLEN_LTRL("ETag"),
				     cfg->etag.ptr, cfg->etag.sz)) != ERR_OK)
		return err;
	if ((err = http_resp_set_hdr(resp, "Cache-Control",
				     STRLEN_LTRL("Cache-Control"),
				     cfg->cctrl.ptr, cfg->cctrl.sz))
	    != ERR_OK)
		return err;
	if (http_req_hdr_eq(req, "If-None-Match", STRLEN_LTRL("If-None-Match"),
			    cfg->etag.ptr, cfg->etag.sz)) {
		resp->status = HTTP_STATUS_NOT_MODIFIED;
		return http_resp_send_hdr(http);
	}

	if (cfg->br.ptr != NULL &&
	    http_req_hdr_contains_ltrl(req, "Accept-Encoding", "br")) {
		start = cfg->br.ptr;
		sz = cfg->br.sz;
		if ((err = http_resp_set_hdr_ltrl(resp, "Content-Encoding",
						  "br"))
		    != ERR_OK)
			return err;
	}
	else if (cfg->gzip.ptr != NULL &&
	    http_req_hdr_contains_ltrl(req, "Accept-Encoding", "gzip")) {
		start = cfg->gzip.ptr;
		sz = cfg->gzip.sz;
		if ((err = http_resp_set_hdr_ltrl(resp, "Content-Encoding",
						  "gzip"))
		    != ERR_OK)
			return err;
	}
	else if (cfg->id.ptr != NULL) {
		start = cfg->id.ptr;
		sz = cfg->id.sz;
	}
	else {
		resp->status = HTTP_STATUS_NOT_ACCEPTABLE;
		return http_hndlr_err(http, NULL);
	}

	if (cfg->gzip.ptr != NULL || cfg->br.ptr != NULL)
		if ((err = http_resp_set_hdr_ltrl(
			     resp, "Vary", "Accept-Encoding")) != ERR_OK)
			return err;

	if ((err = http_resp_set_type(
		     resp, cfg->ct.ptr, cfg->ct.sz)) != ERR_OK)
		return err;

	if ((err = http_resp_set_len(resp, sz)) != ERR_OK)
		return err;

	return http_resp_send_buf(http, start, sz, true);
}

static inline bool
do_err_hndlr(struct http *http)
{
	const uint8_t *bitmap;
	int bit;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);
	CHECK_OBJ_NOTNULL(http->srv_data, SRV_DATA_MAGIC);
	/* This is enforced by register_error_hndlr(). */
	AN(http->srv_data->err_hndlr_status);

	if (http->resp.status >= 600)
		return false;
	bitmap = http->srv_data->err_hndlr_status;
	bit = http->resp.status - 400;
	return (bitmap[bit >> 3] & (1U << (bit & 7))) != 0;
}

#define ERR_BODY	"HTTP error status "
#define ERR_BODY_LEN	STRLEN_LTRL(ERR_BODY)

err_t
http_hndlr_err(struct http *http, void *priv)
{
	struct resp *resp;
	struct srv_data *srv_data;
	char body[ERR_BODY_LEN + 3];
	err_t err;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);
	CHECK_OBJ_NOTNULL(http->srv_data, SRV_DATA_MAGIC);
	PICOW_HTTP_ASSERT(http->resp.status >= HTTP_STATUS_BAD_REQUEST);
	resp = &http->resp;
	srv_data = http->srv_data;
	(void)priv;

	/*
	 * We're going to replace the body; free any previously allocated
	 * body.
	 */
	if (http->resp.alloc && http->resp.body.buf != NULL)
		resp_free_buf(&http->resp.body);

	/* Always set Connection:close, including custom error handlers. */
	if ((err = http_resp_set_hdr_ltrl(resp, "Connection", "close"))
	    != ERR_OK)
		return err;

	if (srv_data->err_hndlr != NULL && do_err_hndlr(http))
		return srv_data->err_hndlr(http,
					   srv_data->err_hndlr_priv.priv);

	if ((err = http_resp_set_type_ltrl(resp, "text/plain")) != ERR_OK)
		return err;
	if ((err = http_resp_set_len(resp, sizeof(body))) != ERR_OK)
		return err;

	memcpy(body, ERR_BODY, ERR_BODY_LEN);
	resp_format_status(resp, body + ERR_BODY_LEN);
	return http_resp_send_buf(http, body, sizeof(body), false);
}

err_t
register_hndlr_methods(struct server_cfg *cfg, const char *path, hndlr_f hndlr,
		       uint8_t methods, void *priv)
{
	struct hndlr_map *map;
	struct hndlrs * hndlrs;
	int idx = 0;

	if (cfg == NULL || path == NULL || hndlr == NULL)
		return ERR_VAL;
	CHECK_OBJ_NOTNULL(cfg->srv_data, SRV_DATA_MAGIC);

	map = has_hndlr(path, strlen(path));
	if (map == NULL)
		return ERR_ARG;
	AN(map->hndlrs);
	hndlrs = map->hndlrs[cfg->srv_data->idx];
	AN(hndlrs);
	if ((hndlrs->method_bitmap & methods) != methods)
		return ERR_ARG;

	hndlrs->priv = priv;
	for (int i = 0; i < __HTTP_METHOD_MAX; i++) {
		uint8_t bit = 1U << i;
		if (bit & hndlrs->method_bitmap) {
			if (bit & methods)
				hndlrs->method_hndlr[idx] = hndlr;
			idx++;
		}
	}
	return ERR_OK;
}

err_t
register_default_hndlr(struct server_cfg *cfg, hndlr_f hndlr, void *priv,
		       priv_fini_f fini)
{
	if (cfg == NULL || hndlr == NULL)
		return ERR_VAL;
	CHECK_OBJ_NOTNULL(cfg->srv_data, SRV_DATA_MAGIC);

	cfg->srv_data->default_hndlr = hndlr;
	cfg->srv_data->default_hndlr_priv.priv = priv;
	cfg->srv_data->default_hndlr_priv.fini = fini;
	return ERR_OK;
}

err_t
register_error_hndlr(struct server_cfg *cfg, hndlr_f hndlr, void *priv,
		     priv_fini_f fini)
{
	if (cfg == NULL || hndlr == NULL)
		return ERR_VAL;
	CHECK_OBJ_NOTNULL(cfg->srv_data, SRV_DATA_MAGIC);

	if (cfg->srv_data->err_hndlr_status == NULL)
		/* Status code for a custom error handler not configured. */
		return ERR_ARG;

	cfg->srv_data->err_hndlr = hndlr;
	cfg->srv_data->err_hndlr_priv.priv = priv;
	cfg->srv_data->err_hndlr_priv.fini = fini;
	return ERR_OK;
}
