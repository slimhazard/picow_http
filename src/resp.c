/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdint.h>
#include <string.h>

#include "pico/cyw43_arch.h"
#include "hardware/rtc.h"
#include "hardware/divider.h"

#include "lwip/altcp.h"

#include "picow_http/http.h"
#include "picow_http/assertion.h"
#include "picow_http/ntp.h"

#define HTTP_STATUS_LEN	(STRLEN_LTRL("HTTP/1.1 200 \r\n"))
#define HTTP_STATUS_OFF	(STRLEN_LTRL("HTTP/1.1 "))
#define HTTP_DATE_LEN	(sizeof("Date: Tue, 15 Nov 1994 12:45:26 GMT\r\n"))

LWIP_MEMPOOL_DECLARE(resp_hdr_small_pool, RESP_HDR_SMALL_POOL_SZ,
		     RESP_HDR_SMALL_BUF_SZ, "small resp hdr");
LWIP_MEMPOOL_DECLARE(resp_hdr_large_pool, RESP_HDR_LARGE_POOL_SZ,
		     RESP_HDR_LARGE_BUF_SZ, "large resp hdr");

static inline err_t
resp_alloc_buf(struct msg *msg)
{
	AN(msg);
	msg->buf = LWIP_MEMPOOL_ALLOC(resp_hdr_small_pool);
	if (msg->buf == NULL)
		return ERR_MEM;
	msg->len = RESP_HDR_SMALL_BUF_SZ;
	msg->pool = &memp_resp_hdr_small_pool;
	return ERR_OK;
}

static inline err_t
resp_expand_buf(struct msg *msg)
{
	char *buf;

	AN(msg);
	if (msg->buf == NULL)
		return resp_alloc_buf(msg);
	PICOW_HTTP_ASSERT(msg->pool == &memp_resp_hdr_small_pool ||
			  msg->pool == &memp_resp_hdr_large_pool);

	if (msg->pool == &memp_resp_hdr_large_pool);
		return ERR_MEM;
	buf = LWIP_MEMPOOL_ALLOC(resp_hdr_large_pool);
	if (buf == NULL)
		return ERR_MEM;
	memcpy(buf, msg->buf, msg->len);
	LWIP_MEMPOOL_FREE(resp_hdr_small_pool, msg->buf);
	msg->buf = buf;
	msg->len = RESP_HDR_LARGE_BUF_SZ;
	msg->pool = &memp_resp_hdr_large_pool;
	return ERR_OK;
}

void
resp_free_buf(struct msg *msg)
{
	AN(msg);

	if (msg->buf == NULL)
		return;
	PICOW_HTTP_ASSERT(msg->pool == &memp_resp_hdr_small_pool ||
			  msg->pool == &memp_resp_hdr_large_pool);

	if (msg->pool == &memp_resp_hdr_small_pool)
		LWIP_MEMPOOL_FREE(resp_hdr_small_pool, msg->buf);
	else
		LWIP_MEMPOOL_FREE(resp_hdr_large_pool, msg->buf);
	msg->buf = NULL;
	msg->pool = NULL;
}

static inline err_t
resp_init_hdr_buf(struct resp *resp)
{
	struct msg *msg = &resp->hdr;
	datetime_t now;
	err_t err;
	int len;

	if (msg->buf != NULL)
		return ERR_OK;
	if ((err = resp_alloc_buf(msg)) != ERR_OK)
		return err;
	PICOW_HTTP_ASSERT(msg->len >= HTTP_STATUS_LEN + HTTP_DATE_LEN);

	memcpy(msg->buf, "HTTP/1.1 200 \r\n", HTTP_STATUS_LEN);
	resp->hdr_cur = msg->buf + HTTP_STATUS_LEN;

	if (rtc_synced()) {
		rtc_get_datetime(&now);
		snprintf(resp->hdr_cur, HTTP_DATE_LEN,
			 "Date: %s, %02d %s %4d %02d:%02d:%02d GMT\r\n",
			 wday_name[now.dotw], now.day,
			 month_name[now.month - 1], now.year, now.hour, now.min,
			 now.sec);
		resp->hdr_cur += HTTP_DATE_LEN - 1;
	}

	return ERR_OK;
}

void
resp_format_status(struct resp *resp, uint8_t *dst)
{
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	AN(dst);

	divmod_result_t div = hw_divider_divmod_u32(resp->status, 100);
	hw_divider_divmod_u32_start(to_remainder_u32(div), 10);
	*dst++ = (uint8_t)to_quotient_u32(div) + '0';
	div = hw_divider_result_wait();
	*dst++ = (uint8_t)to_quotient_u32(div) + '0';
	*dst = (uint8_t)to_remainder_u32(div) + '0';
}

err_t
http_resp_set_hdr(struct resp *resp, const char *name, size_t name_len,
		  const char *val, size_t val_len)
{
	struct msg *msg;
	err_t err;

	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	msg = &resp->hdr;

	if (name == NULL || val == NULL)
		return ERR_VAL;

	if ((err = resp_init_hdr_buf(resp)) != ERR_OK)
		return err;
	while (name_len + val_len + 4 > msg->len - (resp->hdr_cur - msg->buf))
		if ((err = resp_expand_buf(msg)) != ERR_OK)
			return err;

	memcpy(resp->hdr_cur, name, name_len);
	resp->hdr_cur[name_len] = ':';
	resp->hdr_cur[name_len + 1] = ' ';
	resp->hdr_cur += name_len + 2;

	memcpy(resp->hdr_cur, val, val_len);
	resp->hdr_cur[val_len] = '\r';
	resp->hdr_cur[val_len + 1] = '\n';
	resp->hdr_cur += val_len + 2;

	return ERR_OK;
}

#define SZ_LEN (sizeof("4294967295"))

err_t
http_resp_set_len(struct resp *resp, size_t len)
{
	char buf[SZ_LEN];
	size_t l = SZ_LEN;
	err_t err;

	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	if (len > INT32_MAX)
		return ERR_VAL;
	err = format_decimal(buf, &l, len);
	PICOW_HTTP_ASSERT(err == ERR_OK);
	return http_resp_set_hdr(resp, "Content-Length",
				 STRLEN_LTRL("Content-Length"), buf, l);
}

static inline bool
http_hdr_only(struct http *http)
{
	return http->req.method == HTTP_METHOD_HEAD
		|| http->resp.status == HTTP_STATUS_NOT_MODIFIED
		|| http->resp.status == HTTP_STATUS_NO_CONTENT
		|| http->resp.status < 200;
}

err_t
http_resp_send_hdr(struct http *http)
{
	struct resp *resp;
	struct msg *msg;
	err_t err;
	size_t len;
	int flags = TCP_WRITE_FLAG_MORE;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);
	PICOW_HTTP_ASSERT(http->state == HTTP_STATE_HDR_PARSED
			  || http->state == HTTP_STATE_HDR_PARSE_FAIL);
	cyw43_arch_lwip_check();
	resp = &http->resp;
	msg = &resp->hdr;
	if (msg->buf == NULL)
		if ((err = resp_init_hdr_buf(resp)) != ERR_OK)
			return err;

	if (resp->status >= 1000)
		return ERR_VAL;
	if (resp->status != 0 && resp->status < 100)
		return ERR_VAL;
	if (msg->queued > 0)
		return ERR_ALREADY;

	while (msg->len - (resp->hdr_cur - msg->buf) < 2)
		if ((err = resp_expand_buf(msg)) != ERR_OK)
			return err;

	http->state = HTTP_STATE_TX;
	resp->hdr_cur[0] = '\r';
	resp->hdr_cur[1] = '\n';

	/* The len field will now be the length of the header to send. */
	msg->len = len = resp->hdr_cur + 2 - msg->buf;

	if (resp->status != HTTP_STATUS_OK)
		resp_format_status(resp, msg->buf + HTTP_STATUS_OFF);

	if (http_hdr_only(http))
		flags = 0;
	if (len > altcp_sndbuf(http->pcb)) {
		len = altcp_sndbuf(http->pcb);
		flags = TCP_WRITE_FLAG_MORE;
	}
	resp->send_t = get_absolute_time();
	while (len > 0) {
		if ((err = altcp_write(http->pcb, msg->buf, len, flags))
		    != ERR_OK) {
			/* On ERR_MEM attempt smaller lengths */
			if (err == ERR_MEM) {
				len >>= 1;
				flags = TCP_WRITE_FLAG_MORE;
				continue;
			}
			break;
		}
		else {
			msg->queued = len;
			break;
		}
	}
	resp->ack_pending += len;

	/* If ERR_MEM, we will retry in altcp_sent and/or altcp_poll. */
	if (err == ERR_MEM)
		err = ERR_OK;
	return err;
}

static inline err_t
resp_body_dup(struct resp *resp, const uint8_t *buf, bool durable)
{
	if (!durable) {
		resp->body.buf = malloc(resp->body.len);
		if (resp->body.buf == NULL)
			return ERR_MEM;
		memcpy(resp->body.buf, buf, resp->body.len);
		resp->alloc = true;
	}
	else
		resp->body.buf = (uint8_t *)((uintptr_t)buf);

	return ERR_OK;
}

err_t
http_resp_send_buf(struct http *http, const uint8_t *buf, size_t len,
		   bool durable)
{
	struct resp *resp;
	err_t err;
	int flags = 0;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);
	PICOW_HTTP_ASSERT(http->state == HTTP_STATE_HDR_PARSED
			  || http->state == HTTP_STATE_HDR_PARSE_FAIL
			  || http->state == HTTP_STATE_TX);
	cyw43_arch_lwip_check();
	resp = &http->resp;

	if (buf == NULL)
		return ERR_VAL;
	if (resp->body.queued > 0)
		return ERR_ALREADY;

	resp->body.len = len;
	resp->hdr_only = http_hdr_only(http);
	if (durable)
		resp->alloc = false;
	if (http->state != HTTP_STATE_TX)
		if ((err = http_resp_send_hdr(http)) != ERR_OK)
			return err;

	PICOW_HTTP_ASSERT(http->state == HTTP_STATE_TX);
	if (resp->hdr.queued < resp->hdr.len)
		/*
		 * Header is not yet fully queued.
		 * Dup the body and save for later send.
		 */
		return resp_body_dup(resp, buf, durable);

	PICOW_HTTP_ASSERT(resp->hdr.len == resp->hdr.queued);
	if (resp->hdr_only) {
		HTTP_LOG_DEBUG("http_resp_send_buf() called for a "
			       "header-only response, body ignored");
		return ERR_OK;
	}

	if (!durable)
		flags = TCP_WRITE_FLAG_COPY;
	if (len > altcp_sndbuf(http->pcb)) {
		len = altcp_sndbuf(http->pcb);
		flags |= TCP_WRITE_FLAG_MORE;
	}
	while (len > 0) {
		if ((err = altcp_write(http->pcb, buf, len, flags)) != ERR_OK) {
			if (err != ERR_MEM)
				return err;
			len >>= 1;
			flags |= TCP_WRITE_FLAG_MORE;
			continue;
		}
		break;
	}
	if (len != resp->body.len) {
		if ((err = resp_body_dup(resp, buf, durable)) != ERR_OK)
			return err;
	}
	if (len > 0)
		altcp_output(http->pcb);

	resp->body.queued = len;
	resp->ack_pending += len;

	if (resp->body.queued == resp->body.len)
		return http_resp_fini(http);

	return ERR_OK;
}

static const uint8_t chunk_delim[] = { '\r', '\n' };
static const uint8_t end_buf[] = { '0', '\r', '\n', '\r', '\n' };
#define MAX_HEX_SZ (STRLEN_LTRL("ffffffff"))

static inline void
update_ctrs(struct resp *resp, size_t n)
{
	resp->body.len += n;
	resp->body.queued += n;
	resp->ack_pending += n;
}

err_t
http_resp_send_chunk(struct http *http, const uint8_t *buf, size_t len,
		     bool durable)
{
	struct resp *resp;
	err_t err;
	char hex[MAX_HEX_SZ];
	size_t hex_len = MAX_HEX_SZ;
	int flags = TCP_WRITE_FLAG_MORE;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);
	PICOW_HTTP_ASSERT(http->state == HTTP_STATE_HDR_PARSED
			  || http->state == HTTP_STATE_HDR_PARSE_FAIL
			  || http->state == HTTP_STATE_TX);
	cyw43_arch_lwip_check();
	resp = &http->resp;
	resp->alloc = false;
	resp->hdr_only = http_hdr_only(http);

	if (http->state != HTTP_STATE_TX)
		if ((err = http_resp_send_hdr(http)) != ERR_OK)
			return err;

	PICOW_HTTP_ASSERT(http->state == HTTP_STATE_TX);
	if (len == 0) {
		len = sizeof(end_buf);
		if ((err = altcp_write(http->pcb, end_buf, len, 0)) != ERR_OK)
			return err;
		update_ctrs(resp, len);
		altcp_output(http->pcb);
		return http_resp_fini(http);
	}

	if (resp->hdr_only) {
		HTTP_LOG_DEBUG("http_resp_send_chunk() called for a "
			       "header-only response, body ignored");
		return ERR_OK;
	}

	if (buf == NULL)
		return ERR_VAL;
	err = format_hex(hex, &hex_len, len, false);
	PICOW_HTTP_ASSERT(err == ERR_OK);
	if (!durable)
		flags |= TCP_WRITE_FLAG_COPY;

	if ((err = altcp_write(http->pcb, hex, hex_len, TCP_WRITE_FLAG_MORE))
	    != ERR_OK)
		return err;
	update_ctrs(resp, hex_len);

	if ((err = altcp_write(http->pcb, chunk_delim, 2, TCP_WRITE_FLAG_MORE))
	    != ERR_OK)
		return err;
	update_ctrs(resp, 2);

	if ((err = altcp_write(http->pcb, buf, len, flags)) != ERR_OK)
		return err;
	update_ctrs(resp, len);

	if ((err = altcp_write(http->pcb, chunk_delim, 2, TCP_WRITE_FLAG_MORE))
	    != ERR_OK)
		return err;
	update_ctrs(resp, 2);

	altcp_output(http->pcb);
	return ERR_OK;
}
