/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>

#include "hardware/rtc.h"
#include "pico/time.h"
#include "pico/stdio.h"
#include "pico/multicore.h"

#include "pico/cyw43_arch.h"

#include "picow_http/ntp.h"

#if HAVE_FREERTOS
#include "FreeRTOS.h"
#include "task.h"
#endif

#ifndef RUN_FREERTOS_ON_CORE
#define RUN_FREERTOS_ON_CORE 0
#endif

#if HAVE_FREERTOS
#define MAIN_TASK_PRIO   (tskIDLE_PRIORITY + 2UL)
#define OUTPUT_TASK_PRIO (tskIDLE_PRIORITY + 1UL)
#endif

#if HAVE_FREERTOS
static inline void delay_ms(uint32_t ms)
{
	vTaskDelay(ms / portTICK_PERIOD_MS);
}
#else
static inline void delay_ms(uint32_t ms)
{
	sleep_ms(ms);
}
#endif

static void
output_task(void *params)
{
	(void)params;

	while (!rtc_synced()) {
#if PICO_CYW43_ARCH_POLL
		cyw43_arch_poll();
#endif
		delay_ms(1000);
	}

	for (;;) {
		datetime_t t;

#if PICO_CYW43_ARCH_POLL
		cyw43_arch_poll();
#endif
		rtc_get_datetime(&t);
		printf("%s, %02d %s %4d %02d:%02d:%02d GMT\n",
		       wday_name[t.dotw], t.day, month_name[t.month - 1],
		       t.year, t.hour, t.min, t.sec);
		delay_ms(1000);
	}
}

static void
main_task(void *params)
{
	int link_status = CYW43_LINK_DOWN, err;
	struct ntp_cfg cfg;
	(void)params;

	if ((err = cyw43_arch_init()) != 0) {
		printf("Failed to initialize networking: %d", err);
		delay_ms(1000);
		return;
	}

	cyw43_arch_enable_sta_mode();
	puts("Connecting to " WIFI_SSID " ...");
	do {
		if (cyw43_arch_wifi_connect_async(WIFI_SSID, WIFI_PASSWORD,
						  CYW43_AUTH_WPA2_AES_PSK) != 0)
			continue;
		do {
#if PICO_CYW43_ARCH_POLL
			cyw43_arch_poll();
#endif
			link_status = cyw43_tcpip_link_status(&cyw43_state,
							      CYW43_ITF_STA);
			if (link_status != CYW43_LINK_UP) {
				if (link_status < 0) {
					printf("Wifi link error status: %d\n",
					       link_status);
					break;
				}
				delay_ms(1000);
			}
		} while (link_status != CYW43_LINK_UP);
	} while (link_status != CYW43_LINK_UP);

	cfg = ntp_default_cfg();
	/* Set the minimum refresh interval. */
	cfg.refresh_intvl_s = 64;
#ifdef NTP_SERVERS
	cfg.servers = NTP_SERVERS;
#endif
	time_init(&cfg);

#if HAVE_FREERTOS
	BaseType_t ret;
	TaskHandle_t task;

	if ((ret = xTaskCreate(output_task, "time output", PICO_STACK_SIZE,
			       NULL, OUTPUT_TASK_PRIO, &task)) != pdPASS) {
		printf("Failed to create output task: %d", ret);
		return;
	}
	vTaskDelete(NULL);
#else
	output_task(NULL);
#endif
}

#if HAVE_FREERTOS
static void
launch(void)
{
	BaseType_t ret;
	TaskHandle_t task;

	if ((ret = xTaskCreate(main_task, "main", PICO_STACK_SIZE, NULL,
			       MAIN_TASK_PRIO, &task)) != pdPASS) {
		printf("Failed to create main task: %d", ret);
		exit(-1);
	}

# if NO_SYS && configUSE_CORE_AFFINITY && (configNUM_CORES > 1)
	vTaskCoreAffinitySet(task, 1);
# endif

	vTaskStartScheduler();
}
#endif

int
main(void)
{
	stdio_init_all();

        busy_wait_ms(5);
        multicore_reset_core1();
        (void)multicore_fifo_pop_blocking();

#if HAVE_FREERTOS
# if (portSUPPORT_SMP == 1) && (configNUM_CORES == 2)
	puts("Launching on both cores");
	launch();
# elif (RUN_FREERTOS_ON_CORE == 1)
	puts("Launching on core 1");
	multicore_launch_core1(launch);
	for (;;)
		__wfi;
# else
	puts("Launching on core 0");
	launch();
# endif

#else	/* !HAVE_FREERTOS */
	main_task(NULL);
#endif

	/* Unreachable */
	return 0;
}
