/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

struct hndlr_map {
        const char      *name;
        struct hndlrs   * const * hndlrs;
};

static struct srv_data * const servers[] = {};
static const unsigned http_n_srv = 0;
