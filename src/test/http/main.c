/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>

#include <malloc.h>

#include "pico/stdio.h"
#include "pico/time.h"
#include "pico/cyw43_arch.h"
#include "hardware/sync.h"

#include "lwip/stats.h"
#include "lwip/memp.h"

#include "picow_http/http.h"

#include "cJSON.h"
#include "printable.h"

#if HAVE_FREERTOS
#include "pico/multicore.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#endif

#ifndef RUN_FREERTOS_ON_CORE
#define RUN_FREERTOS_ON_CORE 0
#endif

#if HAVE_FREERTOS
#define MAIN_TASK_PRIO  (tskIDLE_PRIORITY + 2UL)
#define STATS_TASK_PRIO (tskIDLE_PRIORITY + 1UL)
#endif

#ifndef NDEBUG
#define STATS_INTVL_MS (5000)
#endif

#define ALL_METHODS_BUT_HEAD						\
	((1U << HTTP_METHOD_GET)      | (1U << HTTP_METHOD_POST)    |	\
	 (1U << HTTP_METHOD_PUT)      | (1U << HTTP_METHOD_DELETE)  |	\
	 (1U << HTTP_METHOD_CONNECT)  | (1U << HTTP_METHOD_OPTIONS) |	\
	 (1U << HTTP_METHOD_TRACE))

#define PRIV_VAL (1234567890)
static int priv_int = PRIV_VAL;

LWIP_MEMPOOL_DECLARE(cx_priv, MAX_CONCURRENT_CX_HINT, sizeof(int),
		     "Connection-scoped private data");

struct name_val_priv {
	unsigned	magic;
#define NAME_VAL_PRIV_MAGIC (0xa6a6ad46)
	char		*buf;
	cJSON		*obj;
};

static const char * method_names[] = {
	[HTTP_METHOD_GET]     = "GET",
	[HTTP_METHOD_HEAD]    = "HEAD",
	[HTTP_METHOD_POST]    = "POST",
	[HTTP_METHOD_PUT]     = "PUT",
	[HTTP_METHOD_DELETE]  = "DELETE",
	[HTTP_METHOD_CONNECT] = "CONNECT",
	[HTTP_METHOD_OPTIONS] = "OPTIONS",
	[HTTP_METHOD_TRACE]   = "TRACE",
};

#if HAVE_FREERTOS
static inline void delay_ms(uint32_t ms)
{
	vTaskDelay(ms / portTICK_PERIOD_MS);
}
#else
static inline void delay_ms(uint32_t ms)
{
	sleep_ms(ms);
}
#endif

static inline void
run_stats(void)
{
	MEMP_STATS_DISPLAY(MEMP_PBUF);
	MEMP_STATS_DISPLAY(MEMP_TCP_PCB);
	MEMP_STATS_DISPLAY(MEMP_TCP_PCB_LISTEN);
	MEM_STATS_DISPLAY();
	TCP_STATS_DISPLAY();
	IP_STATS_DISPLAY();
	puts("");
}

#if HAVE_FREERTOS
static void
stats_timer_cb(TimerHandle_t timer)
{
	(void)timer;
	run_stats();
}
#else
static repeating_timer_t stats_timer;

static bool
show_lwip_stats(repeating_timer_t *rt)
{
	(void)rt;
	run_stats();
	return true;
}
#endif /* HAVE_FREERTOS */

#define BUFLEN (1024)
#define MAX_NAME_LEN (128)

static inline void
cpy(char *dst, const uint8_t *src, size_t len, size_t max)
{
	if (len > max - 1)
		len = max - 1;
	memcpy(dst, src, len);
	dst[len] = '\0';
}

static err_t
name_val_iter(const char *name, size_t name_len, const char *val,
	      size_t val_len, void *priv)
{
	struct name_val_priv *p;
	char n[MAX_NAME_LEN];

	CAST_OBJ_NOTNULL(p, priv, NAME_VAL_PRIV_MAGIC);
	AN(name);
	AN(val);

	cpy(n, name, name_len, MAX_NAME_LEN);
	cpy(p->buf, val, val_len, BUFLEN);
	if (cJSON_AddStringToObject(p->obj, n, p->buf) == NULL)
		return ERR_BUF;
	return ERR_OK;
}

static err_t
reflect_hndlr(struct http *http, void *priv)
{
	cJSON *root = NULL, *hdrs = NULL, *params = NULL, *cookies = NULL,
		*ip = NULL, *ip_local = NULL, *ip_remote = NULL;
	struct req *req;
	struct resp *resp;
	char buf[BUFLEN], *json = NULL;
	const uint8_t *p;
	size_t len, l;
	enum http_method_t method;
	struct name_val_priv name_val_priv;
	void *srv_priv;
	err_t err;

	req = http_req(http);
	resp = http_resp(http);
	(void)priv;

	/* Tests http_srv_priv() */
	srv_priv = http_srv_priv(http);
	AN(srv_priv);
	PICOW_HTTP_ASSERT(*((int *)srv_priv) == PRIV_VAL);

	root = cJSON_CreateObject();
	if (root == NULL)
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);

	method = http_req_method(req);
	PICOW_HTTP_ASSERT(method >= 0 && method <= 7);
	if (cJSON_AddStringToObject(root, "method", method_names[method])
	    == NULL)
		goto reflect_err;

	p = http_req_path(req, &len);
	AN(p);
	cpy(buf, p, len, BUFLEN);
	if (cJSON_AddStringToObject(root, "path", buf) == NULL)
		goto reflect_err;

	p = http_req_query(req, &len);
	if (p == NULL) {
		if (cJSON_AddStringToObject(root, "query", "") == NULL)
			goto reflect_err;
	}
	else {
		cpy(buf, p, len, BUFLEN);
		if (cJSON_AddStringToObject(root, "query", buf) == NULL)
			goto reflect_err;
	}
	if ((params = cJSON_AddObjectToObject(root, "params")) == NULL)
		goto reflect_err;

	INIT_OBJ(&name_val_priv, NAME_VAL_PRIV_MAGIC);
	name_val_priv.buf = buf;
	if (p != NULL && len != 0) {
		name_val_priv.obj = params;
		if (http_req_query_iter(p, len, name_val_iter, &name_val_priv)
		    != ERR_OK)
			goto reflect_err;
	}

	if (cJSON_AddNumberToObject(root, "contentLength", req->clen) == NULL)
		goto reflect_err;

	if ((hdrs = cJSON_AddObjectToObject(root, "headers")) == NULL)
		goto reflect_err;
	name_val_priv.obj = hdrs;
	if (http_req_hdr_iter(req, name_val_iter, &name_val_priv) != ERR_OK)
		goto reflect_err;

	if ((cookies = cJSON_AddObjectToObject(root, "cookies")) == NULL)
		goto reflect_err;
	name_val_priv.obj = cookies;
	if (http_req_cookie_iter(req, name_val_iter, &name_val_priv) != ERR_OK)
		goto reflect_err;

	if (cJSON_AddStringToObject(root, "version", PICOW_HTTP_VERSION)
	    == NULL)
		goto reflect_err;

	if ((ip = cJSON_AddObjectToObject(root, "ip")) == NULL)
		goto reflect_err;
	if ((ip_local = cJSON_AddObjectToObject(ip, "local")) == NULL)
		goto reflect_err;
	if (cJSON_AddStringToObject(ip_local, "addr",
				    ipaddr_ntoa(http_cx_local_ip(http)))
	    == NULL)
		goto reflect_err;
	if (cJSON_AddNumberToObject(ip_local, "port", http_cx_local_port(http))
	    == NULL)
		goto reflect_err;
	if ((ip_remote = cJSON_AddObjectToObject(ip, "remote")) == NULL)
		goto reflect_err;
	if (cJSON_AddStringToObject(ip_remote, "addr",
				    ipaddr_ntoa(http_cx_remote_ip(http)))
	    == NULL)
		goto reflect_err;
	if (cJSON_AddNumberToObject(ip_remote, "port",
				    http_cx_remote_port(http)) == NULL)
		goto reflect_err;

	if (!http_req_hdr_eq_ltrl(req, "Zero-Copy", "true")) {
		len = BUFLEN;
		if (http_req_body(http, buf, &len, 0) != ERR_OK)
			goto reflect_err;
	}
	else {
		size_t off = 0, body_len = http_req_body_len(req);
		const uint8_t *pp;
		const char *end = buf + body_len;

		len = 0;
		for (char *b = buf; b < end; b += l) {
			if ((err = http_req_body_ptr(http, &pp, &l, off))
			    != ERR_OK) {
				HTTP_LOG_ERROR("http_req_body_ptr(): %d", err);
				goto reflect_err;
			}
			memcpy(b, pp, l);
			off += l;
			len += l;
		}
	}
	if (len < BUFLEN)
		buf[len] = '\0';
	else
		buf[BUFLEN - 1] = '\0';
	if (cJSON_AddStringToObject(root, "body", buf) == NULL)
		goto reflect_err;

	if (http_resp_set_type_ltrl(resp, "application/json") != ERR_OK)
		goto reflect_err;
	if (http_resp_set_hdr_ltrl(resp, "Cache-Control", "no-store")
	    != ERR_OK)
		goto reflect_err;
	if ((json = cJSON_PrintUnformatted(root)) == NULL)
		goto reflect_err;
	cJSON_Delete(root);

	len = strlen(json);
	if (http_resp_set_len(resp, len) != ERR_OK) {
		free(json);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	err = http_resp_send_buf(http, json, len, false);
	free(json);
	return err;

 reflect_err:
	if (hdrs != NULL)
		cJSON_Delete(hdrs);
	if (params != NULL)
		cJSON_Delete(params);
	if (cookies != NULL)
		cJSON_Delete(cookies);
	if (ip != NULL)
		cJSON_Delete(ip);
	if (ip_local != NULL)
		cJSON_Delete(ip_local);
	if (ip_remote != NULL)
		cJSON_Delete(ip_remote);
	if (root != NULL)
		cJSON_Delete(root);
	return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
}

static err_t
query_val_hndlr(struct http *http, void *priv)
{
	struct req *req = http_req(http);
	struct resp *resp = http_resp(http);
	const char *name;
	const uint8_t *query, *val;
	size_t query_len, name_len, val_len;
	err_t err;

	query = http_req_query(req, &query_len);
	if (query == NULL) {
		if ((err = http_resp_set_len(resp, 0)) != ERR_OK) {
			HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
			return http_resp_err(http,
					     HTTP_STATUS_INTERNAL_SERVER_ERROR);
		}
		return http_resp_send_buf(http, "", 0, true);
	}

	name = http_req_hdr(req, "Query", STRLEN_LTRL("Query"), &name_len);
	if (name == NULL) {
		if ((err = http_resp_set_len(resp, 0)) != ERR_OK) {
			HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
			return http_resp_err(http,
					     HTTP_STATUS_INTERNAL_SERVER_ERROR);
		}
		return http_resp_send_buf(http, "", 0, true);
	}

	val = http_req_query_val(query, query_len, name, name_len, &val_len);
	if (val == NULL) {
		if ((err = http_resp_set_len(resp, 0)) != ERR_OK) {
			HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
			return http_resp_err(http,
					     HTTP_STATUS_INTERNAL_SERVER_ERROR);
		}
		return http_resp_send_buf(http, "", 0, true);
	}

	if ((err = http_resp_set_len(resp, val_len)) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	if ((err = http_resp_set_type_ltrl(resp, "text/plain")) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_type_ltrl(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	return http_resp_send_buf(http, val, val_len, false);
}

static err_t
cookie_val_hndlr(struct http *http, void *priv)
{
	struct req *req = http_req(http);
	struct resp *resp = http_resp(http);
	const char *name;
	const uint8_t *val;
	size_t name_len, val_len;
	err_t err;

	name = http_req_hdr(req, "Cookie-Name", STRLEN_LTRL("Cookie-Name"),
			    &name_len);
	if (name == NULL) {
		if ((err = http_resp_set_len(resp, 0)) != ERR_OK) {
			HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
			return http_resp_err(http,
					     HTTP_STATUS_INTERNAL_SERVER_ERROR);
		}
		return http_resp_send_buf(http, "", 0, true);
	}

	val = http_req_cookie(req, name, name_len, &val_len);
	if (val == NULL) {
		if ((err = http_resp_set_len(resp, 0)) != ERR_OK) {
			HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
			return http_resp_err(http,
					     HTTP_STATUS_INTERNAL_SERVER_ERROR);
		}
		return http_resp_send_buf(http, "", 0, true);
	}

	if ((err = http_resp_set_len(resp, val_len)) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	if ((err = http_resp_set_type_ltrl(resp, "text/plain")) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_type_ltrl(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	return http_resp_send_buf(http, val, val_len, false);
}

static err_t
chunk_hndlr(struct http *http, void *priv)
{
	struct req *req = http_req(http);
	struct resp *resp = http_resp(http);
	err_t err;

	if (http_resp_set_xfer_chunked(resp) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_chunked(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	if ((err = http_resp_set_type_ltrl(resp, "text/plain")) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_type_ltrl(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}

	for (int i = 10; i >= 0; i--) {
		if ((err = http_resp_send_chunk(http, printable, (1 << i),
						true)) != ERR_OK) {
			HTTP_LOG_ERROR("http_resp_send_chunk(): %d", err);
			return http_resp_err(http,
					     HTTP_STATUS_INTERNAL_SERVER_ERROR);
		}
	}
	if ((err = http_resp_send_chunk(http, NULL, 0, false)) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_send_chunk(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	return ERR_OK;
}

#define DECODE_SZ (512)

static err_t
decode_hndlr(struct http *http, void *priv)
{
	struct req *req = http_req(http);
	struct resp *resp = http_resp(http);
	uint8_t in[DECODE_SZ], out[DECODE_SZ];
	size_t inlen = DECODE_SZ, outlen = DECODE_SZ;
	const uint8_t *query;
	size_t query_len;
	bool plus = false;
	err_t err;

	query = http_req_query(req, &query_len);
	if (query != NULL) {
		const uint8_t *val;
		size_t val_len;

		val = http_req_query_val(query, query_len, "plus", 4, &val_len);
		plus = ((val != NULL) && (val_len == 4) &&
			(strncasecmp(val, "true", 4) == 0));
	}

	if ((err = http_req_body(http, in, &inlen, 0)) != ERR_OK) {
		HTTP_LOG_ERROR("http_req_body(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	if ((err = url_decode(out, &outlen, in, inlen, plus)) != ERR_OK) {
		HTTP_LOG_ERROR("url_decode(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	if ((err = http_resp_set_len(resp, outlen)) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	if ((err = http_resp_set_type_ltrl(resp, "text/plain")) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_type_ltrl(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	return http_resp_send_buf(http, out, outlen, false);
}

static void
cx_priv_free(void *p)
{
	AN(p);
	LWIP_MEMPOOL_FREE(cx_priv, p);
}

#define MAX_NUM_SZ (sizeof("2147483647"))

static err_t
cx_priv_hndlr(struct http *http, void *priv)
{
	struct resp *resp = http_resp(http);
	char body[MAX_NUM_SZ];
	size_t body_len = MAX_NUM_SZ;
	int *cx_ctr;
	err_t err;

	if ((cx_ctr = http_cx_priv(http)) != NULL)
		(*cx_ctr)++;
	else {
		cx_ctr = LWIP_MEMPOOL_ALLOC(cx_priv);
		if (cx_ctr == NULL) {
			HTTP_LOG_ERROR("cx memory pool: out of memory");
			return http_resp_err(http,
					     HTTP_STATUS_INTERNAL_SERVER_ERROR);
		}
		*cx_ctr = 0;
		http_cx_set_priv(http, cx_ctr, cx_priv_free);
	}

	if ((err = http_resp_set_type_ltrl(resp, "text/plain")) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_type_ltrl(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	if ((err = format_decimal(body, &body_len, *cx_ctr)) != ERR_OK) {
		HTTP_LOG_ERROR("format_decimal(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	if ((err = http_resp_set_len(resp, body_len)) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}

	return http_resp_send_buf(http, body, body_len, false);
}

static err_t
chunk_req_hndlr(struct http *http, void *priv)
{
	struct req *req = http_req(http);
	struct resp *resp = http_resp(http);
	uint8_t parity = 0;
	char body[2];
	size_t body_len = 2;
	err_t err;

	if (!http_req_hdr_eq_ltrl(req, "Transfer-Encoding", "chunked")) {
		HTTP_LOG_ERROR("Request hdr Transfer-Encoding:chunked missing");
		return http_resp_err(http, HTTP_STATUS_UNPROCESSABLE_CONTENT);
	}
	if (http_req_body_len(req) > 0) {
		HTTP_LOG_ERROR("Request has both Transfer-Encoding and "
			       "Content-Length headers");
		return http_resp_err(http, HTTP_STATUS_BAD_REQUEST);
	}

	for (;;) {
		uint8_t buf[BUFLEN];
		size_t len = BUFLEN;

		if ((err = http_req_chunk(http, buf, &len)) != ERR_OK) {
			HTTP_LOG_ERROR("http_req_chunk(): %d", err);
			return http_resp_err(http,
					     HTTP_STATUS_INTERNAL_SERVER_ERROR);
		}
		if (len == 0)
			break;
		for (uint8_t *b = buf; b < buf + len; b++)
			parity ^= *b;
	}
	if ((err = format_hex(body, &body_len, parity, true)) != ERR_OK) {
		HTTP_LOG_ERROR("format_hex(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	PICOW_HTTP_ASSERT(body_len == 1 || body_len == 2);
	if (body_len == 1) {
		body[1] = body[0];
		body[0] = '0';
	}
	if ((err = http_resp_set_type_ltrl(resp, "text/plain")) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_type_ltrl(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	if ((err = http_resp_set_len(resp, 2)) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	return http_resp_send_buf(http, body, 2, false);
}

static err_t
clen_parity_hndlr(struct http *http, void *priv)
{
	struct req *req = http_req(http);
	struct resp *resp = http_resp(http);
	uint8_t parity = 0;
	char resp_body[2];
	size_t len, req_len, resp_len = 2, off;
	err_t err;

	if ((req_len = http_req_body_len(req)) == 0) {
		HTTP_LOG_ERROR("No Content-Length header");
		return http_resp_err(http, HTTP_STATUS_UNPROCESSABLE_CONTENT);
	}

	for (off = 0; req_len > 0; req_len -= len, off += len) {
		const uint8_t *buf;

		if ((err = http_req_body_ptr(http, &buf, &len, off))
		    != ERR_OK) {
			HTTP_LOG_ERROR("http_req_body_ptr(): %d", err);
			return http_resp_err(http,
					     HTTP_STATUS_INTERNAL_SERVER_ERROR);
		}
		for (const uint8_t *b = buf; b < buf + len; b++)
			parity ^= *b;
		PICOW_HTTP_ASSERT(req_len >= len);
	}
	if ((err = format_hex(resp_body, &resp_len, parity, true)) != ERR_OK) {
		HTTP_LOG_ERROR("format_hex(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	PICOW_HTTP_ASSERT(resp_len == 1 || resp_len == 2);
	if (resp_len == 1) {
		resp_body[1] = resp_body[0];
		resp_body[0] = '0';
	}
	if ((err = http_resp_set_type_ltrl(resp, "text/plain")) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_type_ltrl(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	if ((err = http_resp_set_len(resp, 2)) != ERR_OK) {
		HTTP_LOG_ERROR("http_resp_set_len(): %d", err);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	return http_resp_send_buf(http, resp_body, 2, false);
}

extern char __StackLimit, __end__;
#define HEAPSZ (&__StackLimit - &__end__)

static err_t
heap_hndlr(struct http *http, void *priv)
{
	err_t err;
	size_t len;
	char buf[BUFLEN], *json = NULL;
	cJSON *root = NULL;
	struct resp *resp = http_resp(http);
	struct mallinfo minfo = mallinfo();

	root = cJSON_CreateObject();
	if (root == NULL)
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);

	if (cJSON_AddNumberToObject(root, "used", minfo.uordblks) == NULL)
		goto heap_err;
	if (cJSON_AddNumberToObject(root, "arena", minfo.arena) == NULL)
		goto heap_err;
	if (cJSON_AddNumberToObject(root, "unused", minfo.fordblks) == NULL)
		goto heap_err;
	if (cJSON_AddNumberToObject(root, "heap", HEAPSZ) == NULL)
		goto heap_err;

	if (http_resp_set_type_ltrl(resp, "application/json") != ERR_OK)
		goto heap_err;
	if (http_resp_set_hdr_ltrl(resp, "Cache-Control", "no-store")
	    != ERR_OK)
		goto heap_err;
	if ((json = cJSON_PrintUnformatted(root)) == NULL)
		goto heap_err;
	cJSON_Delete(root);

	len = strlen(json);
	if (http_resp_set_len(resp, len) != ERR_OK) {
		free(json);
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	}
	err = http_resp_send_buf(http, json, len, false);
	free(json);
	return err;

 heap_err:
	if (root != NULL)
		cJSON_Delete(root);
	return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
}

#ifndef NDEBUG
static inline void
launch_stats(void)
{
# if HAVE_FREERTOS
	TickType_t intvl = STATS_INTVL_MS / portTICK_PERIOD_MS;
	TimerHandle_t timer =
		xTimerCreate("stats", intvl, pdTRUE, NULL, stats_timer_cb);
	if (timer == NULL) {
		HTTP_LOG_ERROR("Insufficient heap for LWIP stats timer");
		return;
	}
	if (xTimerStart(timer, 0) != pdPASS)
		HTTP_LOG_ERROR("Failed to start stats timer");
# else
	if (!add_repeating_timer_ms(STATS_INTVL_MS, show_lwip_stats, NULL,
				    &stats_timer))
		HTTP_LOG_ERROR("No alarm available for LWIP stats display");
# endif /* HAVE_FREERTOS */
}
#endif /* NDEBUG */

static void
main_task(void *params)
{
	struct server *srv;
	struct server_cfg cfg;
	int link_status = CYW43_LINK_DOWN, init_status;
	struct netif *netif;
	err_t err;
	(void)params;

	if ((init_status = cyw43_arch_init()) != 0) {
		HTTP_LOG_ERROR("Failed to initialize networking: %d",
			       init_status);
		return;
	}

	cyw43_arch_enable_sta_mode();
	HTTP_LOG_INFO("Connecting to " WIFI_SSID " ...");
	do {
		if ((err = cyw43_arch_wifi_connect_async(
			     WIFI_SSID, WIFI_PASSWORD,
			     CYW43_AUTH_WPA2_AES_PSK)) != 0) {
			HTTP_LOG_ERROR("wifi connect error: %d", err);
			continue;
		}
		do {
#if PICO_CYW43_ARCH_POLL
			cyw43_arch_poll();
#endif
			link_status = cyw43_tcpip_link_status(&cyw43_state,
							      CYW43_ITF_STA);
			if (link_status != CYW43_LINK_UP) {
				if (link_status < 0) {
					HTTP_LOG_ERROR("wifi error status: %d",
						       link_status);
					break;
				}
				delay_ms(100);
			}
		} while (link_status != CYW43_LINK_UP);
	} while (link_status != CYW43_LINK_UP);

	netif = &cyw43_state.netif[CYW43_ITF_STA];
	HTTP_LOG_INFO("Network initialized, host=%s IP=%s", CYW43_HOST_NAME,
		      ipaddr_ntoa(netif_ip4_addr(netif)));

	cfg = http_default_cfg();
	cfg.ntp_cfg.refresh_intvl_s = 64;
#ifdef NTP_SERVERS
	cfg.ntp_cfg.servers = NTP_SERVERS;
#endif
#if LWIP_ALTCP_TLS
	cfg.port = LWIP_IANA_PORT_HTTPS;
	cfg.idle_tmo_s = 2 * HTTP_DEFAULT_IDLE_TMO_S;
#endif

	if ((err = register_hndlr_methods(&cfg, "/reflect", reflect_hndlr,
					  ALL_METHODS_BUT_HEAD, NULL))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Register /reflect: %d\n", err);
		return;
	}

	if ((err = register_hndlr_methods(&cfg, "/query_val", query_val_hndlr,
					  HTTP_METHODS_GET_HEAD, NULL))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Register /query_val: %d\n", err);
		return;
	}

	if ((err = register_hndlr_methods(&cfg, "/cookie_val", cookie_val_hndlr,
					  HTTP_METHODS_GET_HEAD, NULL))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Register /cookie_val: %d\n", err);
		return;
	}

	if ((err = register_hndlr_methods(&cfg, "/chunked", chunk_hndlr,
					  HTTP_METHODS_GET_HEAD, NULL))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Register /chunked: %d\n", err);
		return;
	}

	if ((err = register_hndlr_methods(&cfg, "/heap", heap_hndlr,
					  HTTP_METHODS_GET_HEAD, NULL))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Register /heap: %d\n", err);
		return;
	}

	if ((err = register_hndlr(&cfg, "/decode", decode_hndlr,
				  HTTP_METHOD_POST, NULL))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Register /decode: %d\n", err);
		return;
	}

	if ((err = register_hndlr_methods(&cfg, "/cx_priv", cx_priv_hndlr,
					  HTTP_METHODS_GET_HEAD, NULL))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Register /cx_priv: %d\n", err);
		return;
	}
	LWIP_MEMPOOL_INIT(cx_priv);

	if ((err = register_hndlr(&cfg, "/chunked_req", chunk_req_hndlr,
				  HTTP_METHOD_POST, NULL))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Register /chunked_req: %d\n", err);
		return;
	}

	if ((err = register_hndlr(&cfg, "/clen_parity", clen_parity_hndlr,
				  HTTP_METHOD_POST, NULL))
	    != ERR_OK) {
		HTTP_LOG_ERROR("Register /clen_parity: %d\n", err);
		return;
	}

	while ((err = http_srv_init(&srv, &cfg)) != ERR_OK)
		HTTP_LOG_ERROR("http_init: %d\n", err);
	HTTP_LOG_INFO("http listening at %s:%u", ipaddr_ntoa(http_srv_ip(srv)),
		      http_srv_port(srv));
	http_srv_set_priv(srv, &priv_int, NULL);

#ifndef NDEBUG
	launch_stats();
#endif

#if HAVE_FREERTOS
	vTaskDelete(NULL);
#else
	for (;;) {
# if PICO_CYW43_ARCH_POLL
		cyw43_arch_poll();
		delay_ms(1);
# else
		__wfi();
# endif /* PICO_CYW43_ARCH_POLL */
	}
#endif /* HAVE_FREERTOS */
}

#if HAVE_FREERTOS

#define PICO_STACK_WORDS (PICO_STACK_SIZE / sizeof(configSTACK_DEPTH_TYPE))

static void
launch(void)
{
	BaseType_t ret;
	TaskHandle_t task;

	if ((ret = xTaskCreate(main_task, "main", PICO_STACK_WORDS, NULL,
			       MAIN_TASK_PRIO, &task)) != pdPASS) {
		HTTP_LOG_ERROR("Failed to create main task: %d", ret);
		exit(-1);
	}

# if NO_SYS && configUSE_CORE_AFFINITY && (configNUMBER_OF_CORES > 1)
	/* 0x03 is the bitmask representing core0 and core1. */
	vTaskCoreAffinitySet(task, (1 << 0) | (1 << 1));
# endif

	vTaskStartScheduler();
}
#endif

int
main(void)
{
	stdio_init_all();

#if HAVE_FREERTOS
        busy_wait_ms(5);
        multicore_reset_core1();
        (void)multicore_fifo_pop_blocking();

# if (FREE_RTOS_KERNEL_SMP) && (configNUMBER_OF_CORES == 2)
	HTTP_LOG_INFO("Launching on both cores");
	launch();
# elif (RUN_FREERTOS_ON_CORE == 1)
	HTTP_LOG_INFO("Launching on core 1");
	multicore_launch_core1(launch);
	for (;;)
		__wfi;
# else
	HTTP_LOG_INFO("Launching on core 0");
	launch();
# endif

#else	/* !HAVE_FREERTOS */
	main_task(NULL);
#endif

	/* Unreachable */
	return 0;
}
