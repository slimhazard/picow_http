#!/usr/bin/env python3

# Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
#
# SPDX-License-Identifier: BSD-2-Clause
# See LICENSE

import unittest
import urllib3
from urllib3.response import HTTPResponse
from urllib3.util import Url
from urllib.parse import quote, quote_plus
import json
import random
import string
import os
import socket

all = [
    '/',
    '/index.html',
    '/index.htm',
    '/hello.html',
    '/goodbye.html',
    '/picow.css',
    '/img/favicon.png',
]

compressible = [
    '/',
    '/index.html',
    '/index.htm',
    '/hello.html',
    '/goodbye.html',
    '/picow.css',
]

methods = [
    'GET',
    'HEAD',
    'POST',
    'PUT',
    'DELETE',
    'CONNECT',
    'TRACE',
    'OPTIONS',
]

static_methods = [ 'GET', 'HEAD' ]

CX = 6
CX_POOLS = urllib3.PoolManager(maxsize=CX, cert_reqs='CERT_NONE')

SCHEME = 'http'
DEFAULT_HOST = 'PicoW'

STATUS_OK = 200
STATUS_NOT_MODIFIED = 304
STATUS_NOT_FOUND = 404
STATUS_NOT_ALLOWED = 405
STATUS_NOT_ACCEPTABLE = 406
STATUS_REQ_BODY_TOO_LARGE = 413
STATUS_REQ_HDR_TOO_LARGE = 431
STATUS_NOT_IMPLEMENTED = 501

def tearDownModule():
    CX_POOLS.clear()

class TestStatic(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._host = os.getenv('HOST', default=DEFAULT_HOST)

    def all_responses(self, http, method, url):
        resp = http.request(method, url,
                            headers = {'Accept-Encoding':'br, gzip'})

        self.assertEqual(resp.status, STATUS_OK)

        self.assertIn('Date', resp.headers)
        self.assertIn('ETag', resp.headers)
        self.assertIn('Cache-Control', resp.headers)
        self.assertIn('Content-Type', resp.headers)
        self.assertIn('Content-Length', resp.headers)
        if method == 'GET':
            self.assertNotEqual(len(resp.data), 0)
        else:
            self.assertEqual(method, 'HEAD')
            self.assertEqual(len(resp.data), 0)

    def test_common(self):
        http = self._cx
        host = self._host
        for path in all:
            url = Url(scheme=SCHEME, host=host, path=path).url
            self.all_responses(http, 'GET', url)
            self.all_responses(http, 'HEAD', url)

    def compressible(self, http, url):
        resp = http.request('GET', url,
                            headers = {'Accept-Encoding':'br, gzip'})
        self.assertEqual(resp.status, STATUS_OK)
        self.assertIn('Content-Encoding', resp.headers)
        self.assertIn('Vary', resp.headers)
        self.assertEqual(resp.headers['Vary'], 'Accept-Encoding')

        resp = http.request('GET', url)
        self.assertEqual(resp.status, STATUS_NOT_ACCEPTABLE)
        self.assertIn('Date', resp.headers)
        self.assertIn('Content-Type', resp.headers)
        self.assertIn('Content-Length', resp.headers)
        self.assertIn('Connection', resp.headers)
        self.assertEqual(resp.headers['Connection'], 'close')

    def test_compressible(self):
        http = self._cx
        host = self._host
        for path in compressible:
            url = Url(scheme=SCHEME, host=host, path=path).url
            self.compressible(http, url)

    def test_not_compressible(self):
        http = self._cx
        host = self._host
        for path in all:
            if path in compressible:
                continue
            url = Url(scheme=SCHEME, host=host, path=path).url
            resp = http.request('GET', url)
            self.assertEqual(resp.status, STATUS_OK)
            self.assertNotIn('Content-Encoding', resp.headers)
            self.assertNotIn('Vary', resp.headers)

            resp = http.request('GET', url,
                                headers = {'Accept-Encoding':'br, gzip'})
            self.assertEqual(resp.status, STATUS_OK)
            self.assertNotIn('Content-Encoding', resp.headers)
            self.assertNotIn('Vary', resp.headers)

    def test_304(self):
        http = self._cx
        host = self._host
        for path in all:
            for method in static_methods:
                url = Url(scheme=SCHEME, host=host, path=path).url
                resp = http.request(method, url,
                                    headers = {'Accept-Encoding':'br, gzip'})
                self.assertEqual(resp.status, STATUS_OK)
                self.assertIn('ETag', resp.headers)
                etag = resp.headers['ETag']

                resp = http.request(method, url, headers = {
                    'Accept-Encoding':'br, gzip', 'If-None-Match':etag})
                self.assertEqual(resp.status, STATUS_NOT_MODIFIED)
                self.assertEqual(len(resp.data), 0)
                self.assertIn('Date', resp.headers)
                self.assertIn('ETag', resp.headers)
                self.assertIn('Cache-Control', resp.headers)
                self.assertNotIn('Content-Type', resp.headers)
                self.assertNotIn('Content-Length', resp.headers)
                self.assertNotIn('Content-Encoding', resp.headers)
                self.assertNotIn('Vary', resp.headers)

    def test_not_allowed(self):
        http = self._cx
        host = self._host
        for path in all:
            for method in methods:
                if method in static_methods:
                    continue
                url = Url(scheme=SCHEME, host=host, path=path).url
                resp = http.request(method, url,
                                    headers = {'Accept-Encoding':'br, gzip'})
                self.assertEqual(resp.status, STATUS_NOT_ALLOWED)
                self.assertIn('Allow', resp.headers)
                self.assertEqual(resp.headers['Allow'], 'GET, HEAD')

class TestHandlers(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._host = os.getenv('HOST', default=DEFAULT_HOST)

    def test404(self):
        http = self._cx
        host = self._host
        url = Url(scheme=SCHEME, host=host, path='/bazquux.html').url
        resp = http.request('GET', url,
                            headers = {'Accept-Encoding':'br, gzip'})
        self.assertEqual(resp.status, STATUS_NOT_FOUND)
        self.assertIn('Date', resp.headers)
        self.assertIn('Content-Type', resp.headers)
        self.assertIn('Content-Length', resp.headers)
        self.assertIn('Connection', resp.headers)
        self.assertEqual(resp.headers['Connection'], 'close')

class TestRequestParse(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._host = os.getenv('HOST', default=DEFAULT_HOST)

    def test_req_parse(self):
        http = self._cx
        host = self._host
        url = Url(scheme=SCHEME, host=host, path='/reflect').url
        resp = http.request('PUT', url, headers={ 'Foo': 'bar' })
        self.assertEqual(resp.status, STATUS_OK)
        self.assertIn('Content-Type', resp.headers)
        self.assertEqual(resp.headers['Content-Type'], 'application/json')
        self.assertGreater(len(resp.data), 0)

        data = json.loads(resp.data.decode('utf-8'))
        self.assertEqual(data['method'], 'PUT')
        self.assertEqual(data['path'], '/reflect')
        self.assertEqual(data['query'], '')
        self.assertEqual(data['params'], {})
        self.assertEqual(data['contentLength'], 0)
        self.assertGreater(len(data['headers']), 0)
        self.assertIn('Host', data['headers'])
        self.assertEqual(data['headers']['Host'].lower(), host.lower())
        self.assertIn('Foo', data['headers'])
        self.assertEqual(data['headers']['Foo'], 'bar')

    def test_too_many_req_hdrs(self):
        http = self._cx
        host = self._host
        # Exceeds default MAX_REQ_HDRS = 16
        rnd_dict = {
            ''.join(random.choices(string.ascii_letters, k=10)):
            ''.join(random.choices(string.ascii_letters, k=10))
            for i in range(17) }
        url = Url(scheme=SCHEME, host=host, path='/reflect').url
        resp = http.request('GET', url, headers=rnd_dict)
        self.assertEqual(resp.status, STATUS_REQ_HDR_TOO_LARGE)

        # Standard tests for the error response handler
        self.assertIn('Date', resp.headers)
        self.assertIn('Content-Type', resp.headers)
        self.assertEqual(resp.headers['Content-Type'], 'text/plain')
        self.assertIn('Content-Length', resp.headers)
        self.assertIn('Connection', resp.headers)
        self.assertEqual(resp.headers['Connection'], 'close')
        self.assertGreater(len(resp.data), 0)

    def test_large_req_hdrs(self):
        http = self._cx
        host = self._host
        # Exceeds the typical MSS = 1460
        rnd_dict = {
            ''.join(random.choices(string.ascii_letters, k=75)):
            ''.join(random.choices(string.ascii_letters, k=75))
            for i in range(10) }
        url = Url(scheme=SCHEME, host=host, path='/reflect').url
        resp = http.request('GET', url, headers=rnd_dict)
        self.assertEqual(resp.status, STATUS_OK)

        data = json.loads(resp.data.decode('utf-8'))
        for h in rnd_dict:
            self.assertIn(h, data['headers'])
            self.assertEqual(rnd_dict[h], data['headers'][h])

    def test_req_body(self):
        http = self._cx
        host = self._host
        for h in [{}, {'Zero-Copy':'true'}]:
            for body_len in [2, 4, 8, 16, 32, 64, 128, 256, 512]:
                rnd_body = ''.join(random.choices(string.ascii_letters,
                                                  k=body_len))
                url = Url(scheme=SCHEME, host=host, path='/reflect').url
                resp = http.request('POST', url, body=rnd_body, headers=h)
                self.assertEqual(resp.status, STATUS_OK)

                data = json.loads(resp.data.decode('utf-8'))
                self.assertEqual(data['contentLength'], body_len)
                self.assertEqual(len(data['body']), body_len)
                self.assertEqual(data['body'], rnd_body)

        for body_len in [1500, 3000, 4500, 6000]:
            parity = 0
            rnd_body = ''.join(random.choices(string.ascii_letters,
                                              k=body_len))
            for b in rnd_body.encode():
                parity ^= b
            url = Url(scheme=SCHEME, host=host, path='/clen_parity').url
            resp = http.request('POST', url, body=rnd_body,
                                headers={'Zero-Copy':'true'})

            self.assertEqual(resp.status, STATUS_OK)
            resp_parity = bytes.fromhex(resp.data.decode('utf-8'))
            self.assertEqual(resp_parity, parity.to_bytes(1, 'big'))

    def test_req_line_fold(self):
        host = self._host
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, 80))
        s.send(f"GET / HTTP/1.1\r\nHost: {host}\r\n Fold: no\r\n\r\n".encode())
        resp = s.recv(1024)
        s.shutdown(socket.SHUT_RDWR)
        s.close()
        self.assertTrue(resp.startswith(b'HTTP/1.1 400'))
        self.assertIn(b'\r\nConnection: close\r\n', resp)

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, 80))
        s.send(f"GET / HTTP/1.1\r\nHost: {host}\r\n\tFold: no\r\n\r\n".encode())
        resp = s.recv(1024)
        s.shutdown(socket.SHUT_RDWR)
        s.close()
        self.assertTrue(resp.startswith(b'HTTP/1.1 400'))
        self.assertIn(b'\r\nConnection: close\r\n', resp)

    def test_req_host_hdr(self):
        host = self._host
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, 80))
        s.send(f"GET / HTTP/1.1\r\n\r\n".encode())
        resp = s.recv(1024)
        s.shutdown(socket.SHUT_RDWR)
        s.close()
        self.assertTrue(resp.startswith(b'HTTP/1.1 400'))

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, 80))
        s.send(f"GET / HTTP/1.1\r\nHost: {host}\r\nHost: {host}\r\n\r\n".
               encode())
        resp = s.recv(1024)
        s.shutdown(socket.SHUT_RDWR)
        s.close()
        self.assertTrue(resp.startswith(b'HTTP/1.1 400'))
        self.assertIn(b'\r\nConnection: close\r\n', resp)

    def test_req_hdr_case(self):
        host = self._host
        for h in ['host', 'HOST', 'Host'.swapcase()]:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((host, 80))
            s.send((f"GET / HTTP/1.1\r\n{h}: {host}\r\n"+
                    "Accept-Encoding: gzip\r\n\r\n").encode())
            resp = s.recv(1024)
            s.shutdown(socket.SHUT_RDWR)
            s.close()
            self.assertTrue(resp.startswith(b'HTTP/1.1 200'))

class TestRequest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._host = os.getenv('HOST', default=DEFAULT_HOST)

    def test_query_val(self):
        http = self._cx
        host = self._host
        n = random.randrange(5, 10)
        fields = {}
        alphanum = string.ascii_letters + string.digits
        for i in range(n):
            key_len = random.randrange(5, 15)
            val_len = random.randrange(5, 15)
            key = ''.join(random.choices(alphanum, k=key_len))
            val = ''.join(random.choices(alphanum, k=val_len))
            fields[key] = val

        url = Url(scheme=SCHEME, host=host, path='/reflect').url
        resp = http.request('GET', url, fields=fields)
        data = json.loads(resp.data.decode('utf-8'))
        for key in fields:
            self.assertEqual(data['params'][key], fields[key])
        for p in data['params']:
            self.assertIn(p, fields)

        for key in fields:
            url = Url(scheme=SCHEME, host=host, path='/query_val').url
            resp = http.request('GET', url, fields=fields,
                                headers = {'Query': key})
            self.assertEqual(resp.status, STATUS_OK)
            body = resp.data.decode('utf-8')
            self.assertEqual(body, fields[key])

            not_key = key
            while key == not_key:
                not_key = ''.join(random.sample(key, len(key)))
            resp = http.request('GET', url, fields=fields,
                                headers = {'Query': not_key})
            self.assertEqual(resp.status, STATUS_OK)
            self.assertEqual(len(resp.data), 0)

    def test_cookie_val(self):
        http = self._cx
        host = self._host
        n = random.randrange(5, 10)
        cookie = {}
        pairs = []
        alphanum = string.ascii_letters + string.digits
        for i in range(n):
            key_len = random.randrange(5, 15)
            val_len = random.randrange(0, 15)
            key = ''.join(random.choices(alphanum, k=key_len))
            val = ''.join(random.choices(alphanum, k=val_len))
            cookie[key] = val
            pairs.append(f'{key}={val}')
        cookies = "; ".join(pairs)

        url = Url(scheme=SCHEME, host=host, path='/reflect').url
        resp = http.request('GET', url, headers = {'Cookie': cookies})
        data = json.loads(resp.data.decode('utf-8'))
        for key in cookie:
            self.assertEqual(data['cookies'][key], cookie[key])
        for p in data['cookies']:
            self.assertIn(p, cookie)

        for key in cookie:
            url = Url(scheme=SCHEME, host=host, path='/cookie_val').url
            resp = http.request('GET', url,
                                headers = {'Cookie-Name': key,
                                           'Cookie': cookies})
            self.assertEqual(resp.status, STATUS_OK)
            body = resp.data.decode('utf-8')
            self.assertEqual(body, cookie[key])

            not_key = key
            while key == not_key:
                not_key = ''.join(random.sample(key, len(key)))
            resp = http.request('GET', url,
                                headers = {'Cookie-Name': not_key,
                                           'Cookie': cookies})
            self.assertEqual(resp.status, STATUS_OK)
            self.assertEqual(len(resp.data), 0)

    def test_url_decode(self):
        http = self._cx
        host = self._host
        test_str = [
            'Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.',
            'foo:bar:baz:quux',
            'Übergrößenträger',
            '日扼語',
            'مرحبا',
        ]

        url = Url(scheme=SCHEME, host=host, path='/decode').url
        for s in test_str:
            encoded = quote(s)
            resp = http.request('POST', url, body=encoded)
            self.assertEqual(resp.status, STATUS_OK)

            decoded = resp.data.decode('utf-8')
            self.assertEqual(decoded, s)

        url = Url(scheme=SCHEME, host=host, path='/decode?plus=true').url
        encoded = quote_plus(test_str[0])
        resp = http.request('POST', url, body=encoded)
        self.assertEqual(resp.status, STATUS_OK)

        decoded = resp.data.decode('utf-8')
        self.assertEqual(decoded, test_str[0])

    def test_chunked_req_body(self):
        host = self._host
        parity = 0
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, 80))
        s.send(f"POST /chunked_req HTTP/1.1\r\nHost: {host}\r\n".encode())
        s.send(b'Transfer-Encoding: chunked\r\n\r\n')
        for _ in range(random.randrange(5, 10)):
            l = random.randrange(64, 128)
            rand_str = ''.join(random.choices(string.ascii_letters, k=l))
            for b in rand_str.encode():
                parity ^= b
            s.send(f"{l:x}\r\n{rand_str}\r\n".encode())
        s.send(b'0\r\n\r\n')
        resp = s.recv(1024)
        s.shutdown(socket.SHUT_RDWR)
        s.close()

        self.assertTrue(resp.startswith(b'HTTP/1.1 200'))
        self.assertIn(b'\r\nContent-Length: 2\r\n', resp)
        resp_parity = resp[-2:].decode()
        for c in resp_parity:
            self.assertIn(c, string.hexdigits)
        self.assertEqual(parity.to_bytes(1, 'big'), bytes.fromhex(resp_parity))

class TestInfrastructure(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._host = os.getenv('HOST', default=DEFAULT_HOST)

    def test_version(self):
        http = self._cx
        host = self._host
        url = Url(scheme=SCHEME, host=host, path='/reflect').url
        resp = http.request('GET', url)
        self.assertEqual(resp.status, STATUS_OK)

        data = json.loads(resp.data.decode('utf-8'))
        self.assertTrue(len(data['version']) > 0)

    def test_cx_priv(self):
        http = self._cx
        host = self._host
        url = Url(scheme=SCHEME, host=host, path='/cx_priv').url
        n = random.randrange(3, 9)

        for ctr in range(n):
            resp = http.request('GET', url)
            self.assertEqual(resp.status, STATUS_OK)
            cnt = int(resp.data.decode('ascii'))
            self.assertEqual(ctr, cnt)

        # Close the connection
        resp = http.request('GET', url, headers = {'Connection':'close'})
        self.assertEqual(resp.status, STATUS_OK)

        # The next connection restarts the count
        n = random.randrange(3, 9)
        for ctr in range(n):
            resp = http.request('GET', url)
            self.assertEqual(resp.status, STATUS_OK)
            cnt = int(resp.data.decode('ascii'))
            self.assertEqual(ctr, cnt)

class TestResponse(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._host = os.getenv('HOST', default=DEFAULT_HOST)

    def test_chunked(self):
        # Expected response is constructed from 1024 of the printable
        # characters.
        s = ''.join([chr(i) for i in range(32, 127)])
        print1k = (s * 11)[:1024]
        exp = ''.join([print1k[:1 << n] for n in reversed(range(11))])

        http = self._cx
        host = self._host
        url = Url(scheme=SCHEME, host=host, path='/chunked').url
        resp = http.request('GET', url)
        self.assertEqual(resp.status, STATUS_OK)

        self.assertIn('Transfer-Encoding', resp.headers)
        self.assertEqual(resp.headers['Transfer-Encoding'], 'chunked')
        data = resp.data.decode('ascii')
        self.assertEqual(data, exp)

    def test_hndlr_not_registered(self):
        http = self._cx
        host = self._host
        url = Url(scheme=SCHEME, host=host, path='/not_registered').url
        for method in ['GET', 'HEAD']:
            resp = http.request(method, url)
            self.assertEqual(resp.status, STATUS_NOT_IMPLEMENTED)
        for method in methods:
            if method == 'GET' or method == 'HEAD':
                continue
            resp = http.request(method, url)
            self.assertEqual(resp.status, STATUS_NOT_ALLOWED)

if __name__ == '__main__':
    unittest.main()
