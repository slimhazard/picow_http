/*
 * Copyright (c) 2024 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>

#include "pico/stdio.h"
#include "pico/time.h"
#include "pico/cyw43_arch.h"
#include "hardware/sync.h"

#include "lwip/stats.h"

#include "picow_http/http.h"

#if HAVE_FREERTOS
#include "pico/multicore.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

#define TASK_PRIO  (tskIDLE_PRIORITY + 1UL)

#define PICO_STACK_WORDS (PICO_STACK_SIZE / sizeof(configSTACK_DEPTH_TYPE))
#endif /* HAVE_FREERTOS */

#ifndef NDEBUG
#define STATS_INTVL_MS (5000)

static void
run_stats(async_context_t *ctx, async_at_time_worker_t *wrk)
{
	MEMP_STATS_DISPLAY(MEMP_PBUF);
	MEMP_STATS_DISPLAY(MEMP_TCP_PCB);
	MEMP_STATS_DISPLAY(MEMP_TCP_PCB_LISTEN);
	MEM_STATS_DISPLAY();
	TCP_STATS_DISPLAY();
	IP_STATS_DISPLAY();
	puts("");

	PICOW_HTTP_ASSERT(
		async_context_add_at_time_worker_in_ms(ctx, wrk,
						       STATS_INTVL_MS));
}

static async_at_time_worker_t stats_worker = {
        .do_work = run_stats,
        .user_data = NULL,
};
#endif

void
init_stats(void *params)
{
	(void)params;

#ifndef NDEBUG
	async_context_add_at_time_worker_in_ms(
                cyw43_arch_async_context(), &stats_worker, 0);
#endif

# if HAVE_FREERTOS
	vTaskDelete(NULL);
# endif
}

static const char * method_names[] = {
        [HTTP_METHOD_GET]     = "GET",
        [HTTP_METHOD_HEAD]    = "HEAD",
        [HTTP_METHOD_POST]    = "POST",
        [HTTP_METHOD_PUT]     = "PUT",
        [HTTP_METHOD_DELETE]  = "DELETE",
        [HTTP_METHOD_CONNECT] = "CONNECT",
        [HTTP_METHOD_OPTIONS] = "OPTIONS",
        [HTTP_METHOD_TRACE]   = "TRACE",
};

#define BODY_SZ (512)

static int priv_val = 4711;

static void
default_fini(void *priv)
{
	int val;

	AN(priv);
	val = *((int *)priv);
	PICOW_HTTP_ASSERT(val == priv_val);
}

static err_t
default_hndlr(struct http *http, void *priv)
{
	struct req *req;
	struct resp *resp;
	uint8_t body[BODY_SZ];
	const uint8_t *path;
	size_t sz, path_len;
	int val;
	err_t err;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	req = http_req(http);
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);

	AN(priv);
	val = *((int *)priv);
	PICOW_HTTP_ASSERT(val == priv_val);

	path = http_req_path(req, &path_len);
	if (memcmp(path, "/status-404", path_len) == 0)
		return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
	if (memcmp(path, "/status-405", path_len) == 0)
		return http_resp_err(http, HTTP_STATUS_METHOD_NOT_ALLOWED);
	if (memcmp(path, "/status-500", path_len) == 0)
		return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
	if (memcmp(path, "/status-503", path_len) == 0)
		return http_resp_err(http, HTTP_STATUS_SERVICE_UNAVAILABLE);

	sz = snprintf(body, BODY_SZ, "{\"method\":\"%s\",\"path\":\"%.*s\"}",
		      method_names[http_req_method(req)], path_len, path);

	if (sz >= BODY_SZ) {
		(void)http_resp_set_status(resp, HTTP_STATUS_URI_TOO_LONG);
		return ERR_BUF;
	}

	if ((err = http_resp_set_type_ltrl(resp, "application/json"))
	    != ERR_OK)
		return err;
	if ((err = http_resp_set_hdr_ltrl(resp, "Cache-Control", "no-store"))
	    != ERR_OK)
		return err;
	if ((err = http_resp_set_len(resp, sz)) != ERR_OK)
		return err;
	return http_resp_send_buf(http, body, sz, false);
}

static const uint8_t body40x[] =
	" __________\n"
	"<   Moo?   >\n"
	" ----------\n"
	"  \\   ^__^\n"
	"   \\  (OO)\\\n"
	"      (__)\\\n";

static const uint8_t body503[] =
	" __________\n"
	"<    ...   >\n"
	" ----------\n"
	"  \\   ^__^\n"
	"   \\  (xx)\\\n"
	"      (__)\\\n"
	"       U   \n";

static err_t
err_hndlr(struct http *http, void *priv)
{
	struct resp *resp;
	const uint8_t *body;
	size_t body_len;
	int val;
	err_t err;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	resp = http_resp(http);
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);

	AN(priv);
	val = *((int *)priv);
	PICOW_HTTP_ASSERT(val == priv_val);

	switch(http_resp_status(resp)) {
	case HTTP_STATUS_NOT_FOUND:
	case HTTP_STATUS_METHOD_NOT_ALLOWED:
		body = body40x;
		body_len = STRLEN_LTRL(body40x);
		break;
	case HTTP_STATUS_SERVICE_UNAVAILABLE:
		body = body503;
		body_len = STRLEN_LTRL(body503);
		break;
	default:
		panic("Not a configured status");
	}

	if ((err = http_resp_set_type_ltrl(resp, "text/plain")) != ERR_OK)
		return err;
	if ((err = http_resp_set_hdr_ltrl(resp, "Cache-Control", "no-store"))
	    != ERR_OK)
		return err;
	if ((err = http_resp_set_len(resp, body_len)) != ERR_OK)
		return err;
	return http_resp_send_buf(http, body, body_len, true);
}

static void
main_task(void *params)
{
	struct server *srv;
	struct server_cfg cfg;
	int link_status = CYW43_LINK_DOWN, init_status;
	err_t err;
	(void)params;

	if ((init_status = cyw43_arch_init()) != 0) {
		HTTP_LOG_ERROR("Failed to initialize networking: %d",
			       init_status);
		return;
	}

#if !HAVE_FREERTOS
	init_stats(NULL);
#endif

	cyw43_arch_enable_sta_mode();
	HTTP_LOG_INFO("Connecting to " WIFI_SSID " ...");
	do {
		if (cyw43_arch_wifi_connect_async(WIFI_SSID, WIFI_PASSWORD,
						  CYW43_AUTH_WPA2_AES_PSK) != 0)
			continue;
		do {
#if PICO_CYW43_ARCH_POLL
			cyw43_arch_poll();
#endif
			link_status = cyw43_tcpip_link_status(&cyw43_state,
							      CYW43_ITF_STA);
			if (link_status != CYW43_LINK_UP) {
				if (link_status < 0) {
					HTTP_LOG_ERROR("WiFi link error: %d",
						       link_status);
					break;
				}
				sleep_ms(100);
			}
		} while (link_status != CYW43_LINK_UP);
	} while (link_status != CYW43_LINK_UP);
	HTTP_LOG_INFO("Connected");

	cfg = http_default_cfg();
#ifdef NTP_SERVERS
	cfg.ntp_cfg.servers = NTP_SERVERS;
#endif
	cfg.idle_tmo_s = 2 * HTTP_DEFAULT_IDLE_TMO_S;

	err = register_default_hndlr(&cfg, default_hndlr, &priv_val,
				     default_fini);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	err = register_error_hndlr(&cfg, err_hndlr, &priv_val, default_fini);
	PICOW_HTTP_ASSERT(err == ERR_OK);

	while ((err = http_srv_init(&srv, &cfg)) != ERR_OK)
		HTTP_LOG_ERROR("http_init: %d", err);
	HTTP_LOG_INFO("http started");

#if HAVE_FREERTOS
	vTaskDelete(NULL);
#else
	for (;;) {
# if PICO_CYW43_ARCH_POLL
		cyw43_arch_poll();
		sleep_ms(1);
# else
		__wfi();
# endif /* PICO_CYW43_ARCH_POLL */
	}
#endif /* HAVE_FREERTOS */
}

#if HAVE_FREERTOS
void
init_freertos(void)
{
	BaseType_t ret;

        busy_wait_ms(5);
        multicore_reset_core1();
        (void)multicore_fifo_pop_blocking();

	ret = xTaskCreate(init_stats, "stats", configMINIMAL_STACK_SIZE, NULL,
			  TASK_PRIO, NULL);
	PICOW_HTTP_ASSERT(ret == pdPASS);

	ret = xTaskCreate(main_task, "main", PICO_STACK_WORDS, NULL, TASK_PRIO,
			  NULL);
	PICOW_HTTP_ASSERT(ret == pdPASS);

	vTaskStartScheduler();
}
#endif

int
main(void)
{
	stdio_init_all();

#if HAVE_FREERTOS
	init_freertos();
#else
	main_task(NULL);
#endif

	/* Unreachable */
	return 0;
}
