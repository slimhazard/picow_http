#!/usr/bin/env python3

# Copyright (c) 2024 Geoff Simmons <geoff@simmons.de>
#
# SPDX-License-Identifier: BSD-2-Clause
# See LICENSE

import unittest
import urllib3
from urllib3.response import HTTPResponse
from urllib3.util import Url
import json
import random
import string
import os

all = [
    '/',
    '/index.html',
    '/index.htm',
    '/hello.html',
    '/goodbye.html',
    '/picow.css',
    '/img/favicon.png',
]

methods = [
    'GET',
    'HEAD',
    'POST',
    'PUT',
    'DELETE',
    'CONNECT',
    'TRACE',
    'OPTIONS',
]

static_methods = [ 'GET', 'HEAD' ]

CX = 6
CX_POOLS = urllib3.PoolManager(maxsize=CX, cert_reqs='CERT_NONE')

SCHEME = 'http'
DEFAULT_HOST = 'PicoW'

STATUS_OK = 200
STATUS_NOT_FOUND = 404
STATUS_NOT_ALLOWED = 405
STATUS_INTERNAL_SERVER_ERROR = 500
STATUS_SERVICE_UNAVAILABLE = 503

def tearDownModule():
    CX_POOLS.clear()

class TestHandlers(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS
        cls._host = os.getenv('HOST', default=DEFAULT_HOST)

    def test_static(self):
        http = self._cx
        host = self._host
        for path in all:
            for method in static_methods:
                url = Url(scheme=SCHEME, host=host, path=path).url
                resp = http.request(method, url,
                                    headers = {'Accept-Encoding':'br, gzip'})
                self.assertEqual(resp.status, STATUS_OK)
                # The default handler doesn't set ETag
                self.assertIn('ETag', resp.headers)

    def test_static_methods(self):
        http = self._cx
        host = self._host
        for path in all:
            for method in methods:
                if method in static_methods:
                    continue
                url = Url(scheme=SCHEME, host=host, path=path).url
                resp = http.request(method, url,
                                    headers = {'Accept-Encoding':'br, gzip'})
                self.assertEqual(resp.status, STATUS_NOT_ALLOWED)

    def test_default(self):
        http = self._cx
        host = self._host
        alphanum = string.ascii_letters + string.digits
        for method in methods:
            path_len = random.randrange(5, 20)
            path = '/' + ''.join(random.choices(alphanum, k=path_len))
            url = Url(scheme=SCHEME, host=host, path=path).url
            resp = http.request(method, url,
                                headers = {'Accept-Encoding':'br, gzip'})
            self.assertEqual(resp.status, STATUS_OK)
            self.assertNotIn('ETag', resp.headers)
            self.assertEqual(resp.headers['Content-Type'], 'application/json')
            if method == 'HEAD':
                continue

            self.assertGreater(len(resp.data), 0)
            data = json.loads(resp.data.decode('utf-8'))
            self.assertIn('method', data)
            self.assertIn('path', data)
            self.assertEqual(data['method'], method)
            self.assertEqual(data['path'], path)

    def test_error(self):
        http = self._cx
        host = self._host

        url = Url(scheme=SCHEME, host=host, path="/status-404").url
        resp = http.request('GET', url)
        self.assertEqual(resp.status, STATUS_NOT_FOUND)
        self.assertIn("Connection", resp.headers)
        self.assertEqual(resp.headers['Connection'], 'close')
        self.assertGreater(len(resp.data), 0)
        body = resp.data.decode('utf-8')
        self.assertIn('Moo?', body)
        self.assertNotIn('HTTP error status ', body)
        self.assertNotIn('xx', body)

        url = Url(scheme=SCHEME, host=host, path="/status-405").url
        resp = http.request('GET', url)
        self.assertEqual(resp.status, STATUS_NOT_ALLOWED)
        self.assertIn("Connection", resp.headers)
        self.assertEqual(resp.headers['Connection'], 'close')
        self.assertGreater(len(resp.data), 0)
        body = resp.data.decode('utf-8')
        self.assertIn('Moo?', body)
        self.assertNotIn('HTTP error status ', body)
        self.assertNotIn('xx', body)

        url = Url(scheme=SCHEME, host=host, path="/status-503").url
        resp = http.request('GET', url)
        self.assertEqual(resp.status, STATUS_SERVICE_UNAVAILABLE)
        self.assertIn("Connection", resp.headers)
        self.assertEqual(resp.headers['Connection'], 'close')
        self.assertGreater(len(resp.data), 0)
        body = resp.data.decode('utf-8')
        self.assertIn('xx', body)
        self.assertNotIn('HTTP error status ', body)
        self.assertNotIn('Moo?', body)

        # Not handled by the custom error handler
        url = Url(scheme=SCHEME, host=host, path="/status-500").url
        resp = http.request('GET', url)
        self.assertEqual(resp.status, STATUS_INTERNAL_SERVER_ERROR)
        self.assertIn("Connection", resp.headers)
        self.assertEqual(resp.headers['Connection'], 'close')
        self.assertGreater(len(resp.data), 0)
        body = resp.data.decode('utf-8')
        self.assertIn('HTTP error status 500', body)
        self.assertNotIn('Moo?', body)
        self.assertNotIn('xx', body)

if __name__ == '__main__':
    unittest.main()
