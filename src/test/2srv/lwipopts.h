/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#ifndef _LWIPOPTS_H
#define _LWIPOPTS_H

#ifndef NO_SYS
# if PICO_CYW43_ARCH_FREERTOS
# define NO_SYS                     0
# else
# define NO_SYS                     1
# endif
#endif

#define MAX_CONCURRENT_CX_HINT      6
#define NUM_SERVER_HINT             2

#ifndef LWIP_SOCKET
#define LWIP_SOCKET                 0
#endif
#if PICO_CYW43_ARCH_POLL
#define MEM_LIBC_MALLOC             1
#else
// MEM_LIBC_MALLOC is incompatible with non polling versions
#define MEM_LIBC_MALLOC             0
#endif
#define MEM_ALIGNMENT               4
#define MEMP_NUM_ARP_QUEUE          10
#define PBUF_POOL_SIZE              24
#define LWIP_ARP                    1
#define LWIP_ETHERNET               1
#define LWIP_ICMP                   1
#define LWIP_RAW                    1
#define TCP_MSS                     1460
#if !(LWIP_ALTCP_TLS)
# define TCP_WND                    (MAX_CONCURRENT_CX_HINT * TCP_MSS)
# elif  (MAX_CONCURRENT_CX_HINT * TCP_MSS) > (16 * 1024)
# define TCP_WND                    (MAX_CONCURRENT_CX_HINT * TCP_MSS)
# else
# define TCP_WND                    (16 * 1024)
#endif
#define TCP_SND_BUF                 (MAX_CONCURRENT_CX_HINT * TCP_MSS)
#define TCP_SND_QUEUELEN            ((4 * (TCP_SND_BUF) + (TCP_MSS - 1)) / (TCP_MSS))
#define TCP_LISTEN_BACKLOG          1
#define MEM_SIZE                    (5 * 1024)

#if !(LWIP_ALTCP_TLS)
# define MEMP_NUM_TCP_SEG           32
# else
# define MEMP_NUM_TCP_SEG           TCP_SND_QUEUELEN
#endif

#define MEMP_NUM_TCP_PCB            (MAX_CONCURRENT_CX_HINT)
#define MEMP_NUM_TCP_PCB_LISTEN     (NUM_SERVER_HINT)

#define LWIP_NETIF_STATUS_CALLBACK  1
#define LWIP_NETIF_LINK_CALLBACK    1
#define LWIP_NETIF_HOSTNAME         1
#define LWIP_NETCONN                0
#define LINK_STATS                  0
// #define ETH_PAD_SIZE                2
#define LWIP_CHKSUM_ALGORITHM       3
#define LWIP_DHCP                   1
#define LWIP_IPV4                   1
#define LWIP_TCP                    1
#define LWIP_UDP                    1
#define LWIP_DNS                    1
#define LWIP_TCP_KEEPALIVE          1
#define LWIP_NETIF_TX_SINGLE_PBUF   1
#define DHCP_DOES_ARP_CHECK         0
#define LWIP_DHCP_DOES_ACD_CHECK    0

#ifndef NDEBUG
#define LWIP_DEBUG                  1
#define LWIP_STATS                  1
#define LWIP_STATS_DISPLAY          1
#endif

#ifndef NDEBUG
#define MEM_OVERFLOW_CHECK          1
#define MEM_SANITY_CHECK            1
#define MEMP_OVERFLOW_CHECK         1
#define MEMP_SANITY_CHECK           1
#else
#define MEM_OVERFLOW_CHECK          0
#define MEM_SANITY_CHECK            0
#define MEMP_OVERFLOW_CHECK         0
#define MEMP_SANITY_CHECK           0
#endif

#ifndef NDEBUG
#define TCP_OUTPUT_DEBUG            LWIP_DBG_ON
#define TCP_DEBUG                   LWIP_DBG_ON
#define ALTCP_MBEDTLS_DEBUG         LWIP_DBG_ON
#define MEM_STATS                   1
#define SYS_STATS                   1
#define TCP_STATS                   1
#define IP_STATS                    1
#define MEMP_STATS                  1
#else
#define TCP_OUTPUT_DEBUG            LWIP_DBG_OFF
#define TCP_DEBUG                   LWIP_DBG_OFF
#define ALTCP_MBEDTLS_DEBUG         LWIP_DBG_OFF
#define MEM_STATS                   0
#define SYS_STATS                   0
#define TCP_STATS                   0
#define IP_STATS                    0
#define MEMP_STATS                  0
#endif

#define PBUF_DEBUG                  LWIP_DBG_OFF
#define ETHARP_DEBUG                LWIP_DBG_OFF
#define NETIF_DEBUG                 LWIP_DBG_OFF
#define API_LIB_DEBUG               LWIP_DBG_OFF
#define API_MSG_DEBUG               LWIP_DBG_OFF
#define SOCKETS_DEBUG               LWIP_DBG_OFF
#define ICMP_DEBUG                  LWIP_DBG_OFF
#define INET_DEBUG                  LWIP_DBG_OFF
#define IP_DEBUG                    LWIP_DBG_OFF
#define MEM_DEBUG                   LWIP_DBG_OFF
#define MEMP_DEBUG                  LWIP_DBG_OFF
#define IP_REASS_DEBUG              LWIP_DBG_OFF
#define TCPIP_DEBUG                 LWIP_DBG_OFF
#define TCP_INPUT_DEBUG             LWIP_DBG_OFF
#define TCP_RTO_DEBUG               LWIP_DBG_OFF
#define TCP_CWND_DEBUG              LWIP_DBG_OFF
#define TCP_WND_DEBUG               LWIP_DBG_OFF
#define TCP_FR_DEBUG                LWIP_DBG_OFF
#define TCP_QLEN_DEBUG              LWIP_DBG_OFF
#define TCP_RST_DEBUG               LWIP_DBG_OFF
#define RAW_DEBUG                   LWIP_DBG_OFF
#define SYS_DEBUG                   LWIP_DBG_OFF
#define UDP_DEBUG                   LWIP_DBG_OFF
#define PPP_DEBUG                   LWIP_DBG_OFF
#define SLIP_DEBUG                  LWIP_DBG_OFF
#define DHCP_DEBUG                  LWIP_DBG_OFF

# if PICO_CYW43_ARCH_FREERTOS
#  if !NO_SYS
#  define TCPIP_THREAD_STACKSIZE 1024
#  define DEFAULT_THREAD_STACKSIZE 1024
#  define DEFAULT_RAW_RECVMBOX_SIZE 8
#  define TCPIP_MBOX_SIZE 8
#  define LWIP_TIMEVAL_PRIVATE 0
#  define LWIP_TCPIP_CORE_LOCKING_INPUT 1
#  endif /* !NO_SYS */
# endif /* PICO_CYW43_ARCH_FREERTOS */

#endif /* __LWIPOPTS_H__ */
