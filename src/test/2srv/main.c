/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <stdio.h>

#include "pico/stdio.h"
#include "pico/time.h"
#include "pico/cyw43_arch.h"
#include "hardware/sync.h"

#include "lwip/stats.h"

#include "picow_http/http.h"

#if HAVE_FREERTOS
#include "pico/multicore.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#endif

#ifndef RUN_FREERTOS_ON_CORE
#define RUN_FREERTOS_ON_CORE 0
#endif

#if HAVE_FREERTOS
#define MAIN_TASK_PRIO  (tskIDLE_PRIORITY + 2UL)
#define STATS_TASK_PRIO (tskIDLE_PRIORITY + 1UL)

static inline void delay_ms(uint32_t ms)
{
	vTaskDelay(ms / portTICK_PERIOD_MS);
}
#else
static inline void delay_ms(uint32_t ms)
{
	sleep_ms(ms);
}
#endif /* HAVE_FREERTOS */

#ifndef NDEBUG
#define STATS_INTVL_MS (5000)

static inline void
run_stats(void)
{
	MEMP_STATS_DISPLAY(MEMP_PBUF);
	MEMP_STATS_DISPLAY(MEMP_TCP_PCB);
	MEMP_STATS_DISPLAY(MEMP_TCP_PCB_LISTEN);
	MEM_STATS_DISPLAY();
	TCP_STATS_DISPLAY();
	IP_STATS_DISPLAY();
	puts("");
}

# if HAVE_FREERTOS
static void
stats_timer_cb(TimerHandle_t timer)
{
	(void)timer;
	run_stats();
}
# else
static repeating_timer_t stats_timer;

static bool
show_lwip_stats(repeating_timer_t *rt)
{
	(void)rt;
	run_stats();
	return true;
}
# endif /* HAVE_FREERTOS */

static inline void
launch_stats(void)
{
# if HAVE_FREERTOS
	TickType_t intvl = STATS_INTVL_MS / portTICK_PERIOD_MS;
	TimerHandle_t timer =
		xTimerCreate("stats", intvl, pdTRUE, NULL, stats_timer_cb);
	if (timer == NULL) {
		HTTP_LOG_ERROR("Insufficient heap for LWIP stats timer");
		return;
	}
	if (xTimerStart(timer, 0) != pdPASS)
		HTTP_LOG_ERROR("Failed to start stats timer");
# else
	if (!add_repeating_timer_ms(STATS_INTVL_MS, show_lwip_stats, NULL,
				    &stats_timer))
		HTTP_LOG_ERROR("No alarm available for LWIP stats display");
# endif /* HAVE_FREERTOS */
}
#endif

static void
main_task(void *params)
{
	struct server *srv_default, *srv_secure;
	struct server_cfg default_cfg, secure_cfg;
	int init_status, link_status = CYW43_LINK_DOWN;
	err_t err;
	(void)params;

	if ((init_status = cyw43_arch_init()) != 0) {
		HTTP_LOG_ERROR("Failed to initialize networking: %d",
			       init_status);
		goto err_exit;
	}

	cyw43_arch_enable_sta_mode();
	HTTP_LOG_INFO("Connecting to " WIFI_SSID " ...");
	do {
		if (cyw43_arch_wifi_connect_async(WIFI_SSID, WIFI_PASSWORD,
						  CYW43_AUTH_WPA2_AES_PSK) != 0)
			continue;
		do {
#if PICO_CYW43_ARCH_POLL
			cyw43_arch_poll();
#endif
			link_status = cyw43_tcpip_link_status(&cyw43_state,
							      CYW43_ITF_STA);
			if (link_status != CYW43_LINK_UP) {
				if (link_status < 0) {
					HTTP_LOG_ERROR("WiFi link error: %d",
						       link_status);
					break;
				}
				sleep_ms(100);
			}
		} while (link_status != CYW43_LINK_UP);
	} while (link_status != CYW43_LINK_UP);
	HTTP_LOG_INFO("Connected");

	if ((err = http_cfg("secure", &secure_cfg)) != ERR_OK) {
		HTTP_LOG_ERROR("http_cfg(secure) failed: %d");
		goto err_exit;
	}
	default_cfg = http_default_cfg();
	default_cfg.port = LWIP_IANA_PORT_HTTP;
	default_cfg.tls = false;
#ifdef NTP_SERVERS
	default_cfg.ntp_cfg.servers = NTP_SERVERS;
#endif

	while ((err = http_srv_init(&srv_default, &default_cfg)) != ERR_OK)
		HTTP_LOG_ERROR("http_init default: %d", err);
	HTTP_LOG_INFO("http default started");

	while ((err = http_srv_init(&srv_secure, &secure_cfg)) != ERR_OK)
		HTTP_LOG_ERROR("http_init secure: %d", err);
	HTTP_LOG_INFO("http secure started");

#ifndef NDEBUG
	launch_stats();
#endif

 err_exit:
#if HAVE_FREERTOS
	vTaskDelete(NULL);
#else
	for (;;) {
# if PICO_CYW43_ARCH_POLL
		cyw43_arch_poll();
		sleep_ms(1);
# else
		__wfi();
# endif /* PICO_CYW43_ARCH_POLL */
	}
#endif /* HAVE_FREERTOS */
}

#if HAVE_FREERTOS

#define PICO_STACK_WORDS (PICO_STACK_SIZE / sizeof(configSTACK_DEPTH_TYPE))

static void
launch(void)
{
	BaseType_t ret;
	TaskHandle_t task;

	if ((ret = xTaskCreate(main_task, "main", 2 * PICO_STACK_WORDS, NULL,
			       MAIN_TASK_PRIO, &task)) != pdPASS) {
		HTTP_LOG_ERROR("Failed to create main task: %d", ret);
		exit(-1);
	}

# if NO_SYS && configUSE_CORE_AFFINITY && (configNUMBER_OF_CORES > 1)
	/* 0x03 is the bitmask representing core0 and core1. */
	vTaskCoreAffinitySet(task, (1 << 0) | (1 << 1));
# endif

	vTaskStartScheduler();
}
#endif

int
main(void)
{
	stdio_init_all();

#if HAVE_FREERTOS
        busy_wait_ms(5);
        multicore_reset_core1();
        (void)multicore_fifo_pop_blocking();

# if (FREE_RTOS_KERNEL_SMP) && (configNUMBER_OF_CORES == 2)
	HTTP_LOG_INFO("Launching on both cores");
	launch();
# elif (RUN_FREERTOS_ON_CORE == 1)
	HTTP_LOG_INFO("Launching on core 1");
	multicore_launch_core1(launch);
	for (;;)
		__wfi;
# else
	HTTP_LOG_INFO("Launching on core 0");
	launch();
# endif

#else	/* !HAVE_FREERTOS */
	main_task(NULL);
#endif

	/* Unreachable */
	return 0;
}
