#!/usr/bin/env python3

# Copyright (c) 2024 Geoff Simmons <geoff@simmons.de>
#
# SPDX-License-Identifier: BSD-2-Clause
# See LICENSE

import unittest
import urllib3
from urllib3.response import HTTPResponse
from urllib3.util import Url
import os

all = [
    '/',
    '/index.html',
    '/index.htm',
    '/hello.html',
    '/goodbye.html',
    '/picow.css',
    '/img/favicon.png',
]

compressible = [
    '/',
    '/index.html',
    '/index.htm',
    '/hello.html',
    '/goodbye.html',
    '/picow.css',
]

script_path = os.path.dirname(os.path.realpath(__file__))
ca_crt = os.path.join(script_path, 'www/crt/testca.crt')

CX = 6
CX_POOLS = urllib3.PoolManager(maxsize=CX, cert_reqs='CERT_REQUIRED',
                               ca_certs=ca_crt)

HOST = 'PicoW'

STATUS_OK = 200

def tearDownModule():
    CX_POOLS.clear()

class TestTwoListeners(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._cx = CX_POOLS

    def all_responses(self, scheme, method, path):
        http = self._cx
        url = Url(scheme=scheme, host=HOST, path=path).url
        resp = http.request(method, url,
                            headers = {'Accept-Encoding':'br, gzip'})

        self.assertEqual(resp.status, STATUS_OK)

        self.assertIn('Date', resp.headers)
        self.assertIn('ETag', resp.headers)
        self.assertIn('Cache-Control', resp.headers)
        self.assertIn('Content-Type', resp.headers)
        self.assertIn('Content-Length', resp.headers)

        if method == 'GET':
            self.assertNotEqual(len(resp.data), 0)
        else:
            self.assertEqual(method, 'HEAD')
            self.assertEqual(len(resp.data), 0)

        if path in compressible:
            self.assertIn('Content-Encoding', resp.headers)
        else:
            url = Url(scheme=scheme, host=HOST, path=path).url
            resp = http.request('GET', url)
            self.assertEqual(resp.status, STATUS_OK)
            self.assertNotIn('Content-Encoding', resp.headers)

            resp = http.request('GET', url,
                                headers = {'Accept-Encoding':'br, gzip'})
            self.assertEqual(resp.status, STATUS_OK)
            self.assertNotIn('Content-Encoding', resp.headers)

    def test_http(self):
        for path in all:
            self.all_responses('http', 'GET', path)
            self.all_responses('http', 'HEAD', path)

    def test_https(self):
        for path in all:
            self.all_responses('https', 'GET', path)
            self.all_responses('https', 'HEAD', path)

if __name__ == '__main__':
    unittest.main()
