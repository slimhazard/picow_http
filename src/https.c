/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "picow_http/assertion.h"
#include "https.h"

#if LWIP_ALTCP_TLS

#include "picow_http/log.h"

#include "lwip/altcp_tls.h"
#include "https_certs.h"

int
get_allocator(const struct cert_data *data, altcp_allocator_t *alloc,
	      altcp_allocator_t **allocp, bool tls)
{
	struct altcp_tls_config *conf;

	AN(allocp);
	if (!tls) {
		*allocp = NULL;
		return 0;
	}

	CHECK_OBJ_NOTNULL(data, CERT_DATA_MAGIC);
	AN(alloc);
	conf = altcp_tls_create_config_server_privkey_cert(
		data->key.ptr, data->key.sz, data->pass.ptr, data->pass.sz,
		data->cert.ptr,	data->cert.sz);
	if (conf == NULL) {
		HTTP_LOG_ERROR("Cannot parse certificate/key");
		*allocp = NULL;
		return -1;
	}
	alloc->alloc = altcp_tls_alloc;
	alloc->arg = conf;
	*allocp = alloc;
	return 0;
}

void
free_tls_conf(void *p)
{
	altcp_allocator_t *alloc;
	struct altcp_tls_config *conf;

	AN(p);
	alloc = p;
	AN(alloc->arg);
	conf = alloc->arg;
	altcp_tls_free_config(conf);
}

#else

int
get_allocator(const struct cert_data *data, altcp_allocator_t *alloc,
	      altcp_allocator_t **allocp, bool tls)
{
	AN(allocp);
	(void)alloc;
	(void)data;
	(void)tls;

	*allocp = NULL;
	return 0;
}

void
free_tls_conf(void *p)
{
	(void)p;
}

#endif
