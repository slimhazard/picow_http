/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include <string.h>
#include <stdio.h>

#include "pico/cyw43_arch.h"
#include "hardware/rtc.h"

#include "lwip/prot/iana.h"

#include "picow_http/http.h"
#include "picow_http/assertion.h"
#include "picow_http/ntp.h"

#include "https.h"

LWIP_MEMPOOL_DECLARE(http_pool, HTTP_N, sizeof(struct http), "struct http");

enum req_parse_t {
	REQ_PARSE_OK,
	REQ_PARSE_MORE,
	REQ_PARSE_TOO_LARGE,
	REQ_BODY_TOO_LARGE,
	REQ_PROTO_NOT_SUPPORTED,
	REQ_PARSE_ERR,
};

struct server {
	unsigned		magic;
#define SERVER_MAGIC 0x3c58a6f9
	struct altcp_pcb	*pcb;
	struct srv_data		*srv_data;
	void			*tls_alloc;
	priv_fini_f		*fini;
	int64_t			idle_tmo_us;
	int64_t			send_tmo_us;
};

static struct server srvs[SRV_N];
static bool srv_alloced[SRV_N] = { false };
static unsigned n_srv_running = 0;

static inline struct http *
http_take(void)
{
	struct http *http = LWIP_MEMPOOL_ALLOC(http_pool);
	if (http != NULL)
		INIT_OBJ(http, HTTP_MAGIC);
	return http;
}

static inline void
http_free(struct http *http)
{
	if (http == NULL)
		return;
	ZERO_OBJ(http, sizeof(*http));
	LWIP_MEMPOOL_FREE(http_pool, http);
	http = NULL;
}

static inline struct server *
srv_take(void)
{
	struct server *srv = NULL;

	for (int i = 0; i < SRV_N; i++)
		if (!srv_alloced[i]) {
			srv = &srvs[i];
			srv_alloced[i] = true;
			break;
		}
	if (srv != NULL) {
		INIT_OBJ(srv, SERVER_MAGIC);
		AZ(srv->srv_data);
		AZ(srv->fini);
	}
	return srv;
}

static inline void
srv_free(struct server *srv)
{
	int idx = -1;

	if (srv == NULL)
		return;
	ZERO_OBJ(srv, sizeof(*srv));
	for (int i = 0; i < SRV_N; i++)
		if (srv == &srvs[i]) {
			idx = i;
			break;
		}
	PICOW_HTTP_ASSERT(idx != -1);
	PICOW_HTTP_ASSERT(srv_alloced[idx]);
	CHECK_OBJ_NOTNULL(srv->srv_data, SRV_DATA_MAGIC);
	srv_alloced[idx] = false;
	srv->srv_data->srv_priv = NULL;
	srv->srv_data->default_hndlr = NULL;
	srv->srv_data->default_hndlr_priv.priv = NULL;
	srv->srv_data->default_hndlr_priv.fini = NULL;
	srv->srv_data->err_hndlr = NULL;
	srv->srv_data->err_hndlr_priv.priv = NULL;
	srv->srv_data->err_hndlr_priv.fini = NULL;
}

/* Forward declarations */
static void http_err(void *arg, err_t err);
static err_t http_poll(void *arg, struct altcp_pcb *tpcb);

static inline void
http_free_hdr_body(struct http *http)
{
	if (http == NULL)
		return;
	CHECK_OBJ(http, HTTP_MAGIC);

	if (http->resp.hdr.buf != NULL)
		resp_free_buf(&http->resp.hdr);
	if (http->resp.alloc && http->resp.body.buf != NULL)
		resp_free_buf(&http->resp.body);
}

static void inline
pcb_fini(struct http *http)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	http->pcb = NULL;
}

static void
http_fini(struct http *http)
{
	if (http == NULL)
		return;
	CHECK_OBJ(http, HTTP_MAGIC);

	AZ(http->cx_priv.priv);
	AZ(http->cx_priv.fini);

	if (http->pcb != NULL)
		pcb_fini(http);

	http_free_hdr_body(http);
	http_free(http);
}

static inline size_t
total_queued(struct resp *resp)
{
	return resp->hdr.queued + resp->body.queued;
}

static err_t
http_close(struct http *http)
{
	err_t err;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->req, REQ_MAGIC);

	if (http->cx_priv.priv != NULL && http->cx_priv.fini != NULL)
		http->cx_priv.fini(http->cx_priv.priv);
	http->cx_priv.priv = NULL;
	http->cx_priv.fini = NULL;

	if (http->req.p != NULL)
		pbuf_free(http->req.p);
	if (http->pcb == NULL)
		return ERR_OK;

	http->state = HTTP_STATE_CLOSING;
	if (http->resp.ack_pending > 0)
		HTTP_LOG_DEBUG("http_close(): ACKs pending");

	cyw43_arch_lwip_check();
	if ((err = altcp_close(http->pcb)) != ERR_OK) {
		if (err == ERR_MEM) {
			/* Try again in http_sent() or http_poll(). */
			altcp_recv(http->pcb, NULL);
			return ERR_MEM;
		}
		altcp_abort(http->pcb);
		pcb_fini(http);
		return ERR_ABRT;
	}
	return ERR_OK;
}

static inline err_t
http_abort(struct http *http)
{
	err_t err;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	if ((err = http_close(http)) != ERR_OK)
		HTTP_LOG_ERROR("http_close(): err %d", err);
	http_fini(http);
	return err;
}

static err_t
http_resp_send(struct http *http)
{
	err_t err;
	hndlr_f hndlr;
	void *priv;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	AN(http->pcb);
	AN(http->req.p);
	cyw43_arch_lwip_check();

	hndlr = get_hndlr(http, &priv);
	AN(hndlr);
	err = hndlr(http, priv);
	if (err != ERR_OK) {
		HTTP_LOG_ERROR("Handler error: %d", err);
		http->resp.status = HTTP_STATUS_INTERNAL_SERVER_ERROR;
		err = http_hndlr_err(http, NULL);
		if (err != ERR_OK)
			HTTP_LOG_ERROR("Error handler error: %d", err);
	}

	return err;
}

err_t
http_resp_fini(struct http *http)
{
	struct resp *resp;
	struct req *req;
	err_t err;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	PICOW_HTTP_ASSERT(http->state == HTTP_STATE_TX);
	CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);
	resp = &http->resp;
	CHECK_OBJ_NOTNULL(&http->req, REQ_MAGIC);
	req = &http->req;
	PICOW_HTTP_ASSERT(resp->hdr.queued == resp->hdr.len);
	PICOW_HTTP_ASSERT(resp->hdr_only
			  || resp->body.queued == resp->body.len);
	PICOW_HTTP_ASSERT(resp->ack_pending <= total_queued(resp));

	if (resp->ack_pending > 0)
		/* Wait for ACK in http_sent(). */
		return ERR_OK;

	cyw43_arch_lwip_check();
#if HTTP_REQUEST_LOG
	if (http->req.recv_t.year != 0)
		http_log("%s - - [%02d/%s/%d:%02d:%02d:%02d +0000] \"%.*s\" %u %u",
			 ipaddr_ntoa(http_remote_ip(http->pcb)),
			 req->recv_t.day,  month_name[req->recv_t.month - 1],
			 req->recv_t.year, req->recv_t.hour, req->recv_t.min,
			 req->recv_t.sec, req->req_line_end, req->p->payload,
			 resp->status, resp->body.len);
	else
		http_log("%s - - [] \"%.*s\" %u %u",
			 ipaddr_ntoa(http_remote_ip(http->pcb)),
			 req->req_line_end, req->p->payload, resp->status,
			 resp->body.len);
#endif
	pbuf_free(req->p);
	req->p = NULL;

	if (http->resp.status >= 400 || http->req.close)
		return http_abort(http);

	http->idle_t = get_absolute_time();
	http_free_hdr_body(http);
	http->state = HTTP_STATE_IDLE;
	INIT_OBJ(&http->req, REQ_MAGIC);
	INIT_OBJ(&http->resp, RESP_MAGIC);

	return ERR_OK;
}

static err_t
send_unqueued(struct altcp_pcb *pcb, struct msg *msg, size_t *pending,
	      int flags)
{
	size_t len;
	err_t err;

	AN(pcb);
	AN(msg);

	len = msg->len - msg->queued;
	if (len > altcp_sndbuf(pcb)) {
		len = altcp_sndbuf(pcb);
		flags = TCP_WRITE_FLAG_MORE;
	}
	while (len > 0) {
		HTTP_LOG_DEBUG("send_unqueued(len=%u)", len);
		if ((err = altcp_write(pcb, msg->buf + msg->queued, len, flags))
		    != ERR_OK) {
			if (err != ERR_MEM)
				return err;
			len >>= 1;
			continue;
		}
		break;
	}
	if (len == 0)
		return ERR_MEM;
	msg->queued += len;
	*pending += len;
	return altcp_output(pcb);
}

static err_t
http_send_continue(struct http *http)
{
	struct resp *resp;

	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	PICOW_HTTP_ASSERT(http->state == HTTP_STATE_TX);
	CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);
	resp = &http->resp;
	cyw43_arch_lwip_check();

	if (resp->hdr.buf != NULL)
		if (resp->hdr.queued >= resp->hdr.len) {
			PICOW_HTTP_ASSERT(resp->hdr.queued == resp->hdr.len);
			resp_free_buf(&resp->hdr);
		}
		else {
			int flags = 0;

			/* Still sending the header. */
			if (!resp->hdr_only)
				flags = TCP_WRITE_FLAG_MORE;
			return send_unqueued(http->pcb, &resp->hdr,
					     &resp->ack_pending, flags);
		}

	PICOW_HTTP_ASSERT(resp->hdr.queued == resp->hdr.len);
	if (!resp->hdr_only && resp->body.queued < resp->body.len)
		return send_unqueued(http->pcb, &resp->body,
				     &resp->ack_pending, 0);

	/* Response TX is complete. */
	PICOW_HTTP_ASSERT(resp->hdr_only
			  || resp->body.queued == resp->body.len);
	return http_resp_fini(http);
}

/*
 * If in the future we provide a user callback to free memory allocated
 * for the response body after the send has been ACKed, it will be called
 * here.
 */
static err_t
http_sent(void *arg, struct altcp_pcb *tpcb, uint16_t len)
{
	struct http *http;

	CAST_OBJ_NOTNULL(http, arg, HTTP_MAGIC);
	PICOW_HTTP_ASSERT(http->pcb == tpcb);
	PICOW_HTTP_ASSERT(http->state == HTTP_STATE_TX ||
		http->state == HTTP_STATE_CLOSING);
	PICOW_HTTP_ASSERT(len <= http->resp.ack_pending);

	http->resp.ack_pending -= len;
	if (http->state == HTTP_STATE_TX)
		return http_send_continue(http);
	if (http->state == HTTP_STATE_CLOSING)
		return http_abort(http);
}

static uint16_t
hdr_atoi(struct pbuf *p, uint16_t off, uint16_t len)
{
	uint8_t *buf;
	uint16_t n = 0;

	AN(p);
	cyw43_arch_lwip_check();

	buf = (uint8_t *)p->payload;
	for (unsigned i = off; i < off + len; i++) {
		n *= 10;
		n += buf[i] - '0';
	}
	return n;
}

/* Any ASCII horizontal or vertical whitespace */
static inline bool
any_ws(char c)
{
	return c == ' ' || (c <= '\r' && c >= '\t');
}

#define HTTP_PROTO11_LEN	(STRLEN_LTRL("HTTP/1.1"))
#define HTTP_CONTENT_LENGTH_LEN	(STRLEN_LTRL("Content-Length"))
#define HTTP_CONNECTION_LEN	(STRLEN_LTRL("Connection"))
#define HTTP_CLOSE_LEN		(STRLEN_LTRL("close"))
#define HTTP_HOST_LEN		(STRLEN_LTRL("Host"))

static enum req_parse_t
http_req_hdr_parse(struct req *req)
{
	uint8_t *start, *p, *next, *end;
	struct pbuf *new;
	err_t err;

	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	AN(req->p);
	AZ(req->close);
	cyw43_arch_lwip_check();

	req->hdr_end = pbuf_memfind(req->p, "\r\n\r\n", 4, req->hdr_end);
	if (req->hdr_end == PBUF_NOT_FOUND) {
		req->hdr_end = req->p->tot_len - 3;
		return REQ_PARSE_MORE;
	}
	req->chunk_off = body_off(req);
	if (req->hdr_end > req->p->len) {
		if ((new = pbuf_coalesce(req->p, PBUF_TRANSPORT)) == req->p)
			return REQ_PARSE_TOO_LARGE;
		else
			req->p = new;
	}
	start = p = req->p->payload;
	end = start + req->hdr_end;
	PICOW_HTTP_ASSERT(memcmp(end, "\r\n\r\n", 4) == 0);

	/* Method */
	switch(*p) {
	case 'G':
		if (memcmp(p + 1, "ET", 2) != 0)
			return REQ_PARSE_ERR;
		req->method = HTTP_METHOD_GET;
		p += 3;
		break;
	case 'H':
		if (memcmp(p + 1, "EAD", 3) != 0)
			return REQ_PARSE_ERR;
		req->method = HTTP_METHOD_HEAD;
		p += 4;
		break;
	case 'P':
		if (memcmp(p + 1, "OST", 3) == 0) {
			req->method = HTTP_METHOD_POST;
			p += 4;
			break;
		}
		else if (memcmp(p + 1, "UT", 2) == 0) {
			req->method = HTTP_METHOD_PUT;
			p += 3;
			break;
		}
		return REQ_PARSE_ERR;
	case 'D':
		if (memcmp(p + 1, "ELETE", 5) != 0)
			return REQ_PARSE_ERR;
		req->method = HTTP_METHOD_DELETE;
		p += 6;
		break;
	case 'C':
		if (memcmp(p + 1, "ONNECT", 6) != 0)
			return REQ_PARSE_ERR;
		req->method = HTTP_METHOD_CONNECT;
		p += 7;
		break;
	case 'O':
		if (memcmp(p + 1, "PTIONS", 6) != 0)
			return REQ_PARSE_ERR;
		req->method = HTTP_METHOD_OPTIONS;
		p += 7;
		break;
	case 'T':
		if (memcmp(p + 1, "RACE", 4) != 0)
			return REQ_PARSE_ERR;
		req->method = HTTP_METHOD_TRACE;
		p += 5;
		break;
	default:
		return REQ_PARSE_ERR;
	}

	/* Find the end of the first request line. */
	next = p;
	do {
		PICOW_HTTP_ASSERT(end > next);
		next = memchr(next, '\r', end - next);
		if (next == NULL)
			/*
			 * No \r\n before the \r\n\r\n separator, so there
			 * are no headers, in particular no Host. MUST
			 * reject with 400 (RFC 9112 3.2).
			 */
			return REQ_PARSE_ERR;
		if (*(next + 1) == '\n')
			break;
		next++;
	} while (next < end);
	PICOW_HTTP_ASSERT(*next == '\r' && *(next + 1) == '\n');
	req->req_line_end = next - start;
	/*
	 * RFC 9112 section 2.2: MUST reject a request with whitespace
	 * between the start line and first header field.
	 */
	if (any_ws(*(next + 2)))
		return REQ_PARSE_ERR;

	/* URL path */
	while (http_ows(*p))
		p++;
	req->path.off = p - start;
	/* Query string */
	next = memchr(p, '?', req->req_line_end - req->path.off);
	if (next != NULL) {
		req->path.len = next - p;
		req->query.off = next + 1 - start;
	}
	else
		req->query.off = 0;
	next = memchr(p, ' ', req->req_line_end - req->path.off);
	if (next == NULL) {
		next = memchr(p, '\t', req->req_line_end - req->path.off);
		if (next == NULL)
			return REQ_PARSE_ERR;
	}
	if (req->query.off == 0)
		req->path.len = next - p;
	else
		req->query.len = next - (start + req->query.off);
	p = next;

	/* Protocol */
	while (http_ows(*p))
		p++;
	/* XXX for now strictly HTTP/1.1 */
	if (memcmp(p, "HTTP/1.1", HTTP_PROTO11_LEN) != 0)
		return REQ_PROTO_NOT_SUPPORTED;
	p += HTTP_PROTO11_LEN;
	while (http_ows(*p))
		p++;
	if (memcmp(p, "\r\n", 2) != 0)
		return REQ_PARSE_ERR;
	PICOW_HTTP_ASSERT(p == start + req->req_line_end);
	p += 2;

	/* Headers */
	req->nhdrs = 0;
	for (int idx = 0; p < end; idx++, req->nhdrs++) {
		struct hdr *hdr;
		uint8_t *eol;

		if (idx == HTTP_REQ_MAX_HDRS)
			return REQ_PARSE_TOO_LARGE;
		hdr = &req->hdr[idx];

		/* XXX DRY */
		eol = p;
		PICOW_HTTP_ASSERT(end >= eol);
		do {
			eol = memchr(eol, '\r', end - eol - 1);
			if (eol == NULL) {
				eol = end;
				break;
			}
			if (*(eol + 1) == '\n')
				break;
			eol++;
		} while (eol < end);
		PICOW_HTTP_ASSERT(*eol == '\r' && *(eol + 1) == '\n');

		/* Reject line folding; RFC 9112 5.2 */
		if (eol != end && http_ows(*(eol + 2)))
			return REQ_PARSE_ERR;

		hdr->name.off = p - start;
		next = memchr(p, ':', eol - p);
		if (next == NULL || next >= eol || http_ows(*(next - 1)))
			return REQ_PARSE_ERR;
		hdr->name.len = next - p;

		p = next + 1;
		while (http_ows(*p))
			p++;
		hdr->val.off = p - start;
		next = eol;
		while (http_ows(*next))
			next--;
		hdr->val.len = next - p;
		p = eol + 2;

		if (hdr->name.len == HTTP_CONTENT_LENGTH_LEN)
			if (hdr_cmp(req->p, "Content-Length",
				    hdr->name.off, hdr->name.len)
			    == 0)
				req->clen = hdr_atoi(req->p, hdr->val.off,
						     hdr->val.len);

		if (hdr->name.len == HTTP_CONNECTION_LEN &&
		    hdr->val.len == HTTP_CLOSE_LEN &&
		    hdr_cmp(req->p, "Connection", hdr->name.off, hdr->name.len)
		    == 0 &&
		    memcmp("close", req->p->payload + hdr->val.off,
			   HTTP_CLOSE_LEN) == 0)
			req->close = true;

		if (hdr->name.len == HTTP_HOST_LEN &&
		    hdr_cmp(req->p, "Host", hdr->name.off,
			    hdr->name.len) == 0) {
			/*
			 * RFC 9112 3.2: reject with 400 if Host appears
			 * more than once.
			 */
			if (req->host)
				return REQ_PARSE_ERR;
			req->host = true;
		}
	}

	/* RFC 9112 3.2: reject with 400 if there is no Host header. */
	if (!req->host)
		return REQ_PARSE_ERR;

	return REQ_PARSE_OK;
}

static err_t
http_recv(void *arg, struct altcp_pcb *tpcb, struct pbuf *p, err_t err)
{
	struct http *http;
	enum req_parse_t req_parse_result;
	struct req *req;

	CAST_OBJ_NOTNULL(http, arg, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->req, REQ_MAGIC);
	AN(http->pcb);
	PICOW_HTTP_ASSERT(http->pcb == tpcb);
	req = &http->req;

	if (p == NULL) {
		HTTP_LOG_DEBUG("%s:%u closed connection, http state %d",
			       ipaddr_ntoa(http_remote_ip(http->pcb)),
			       http_remote_port(http->pcb), http->state);
		if (http->state == HTTP_STATE_CLOSING) {
			http_fini(http);
			return ERR_OK;
		}
		if (http->state == HTTP_STATE_TX)
			if ((err = http_send_continue(http)) != ERR_OK)
				return err;
		return http_abort(http);
	}

	if (err != ERR_OK) {
		pbuf_free(p);
		return err;
	}

	http->req.recvd += p->len;
	altcp_recved(http->pcb, p->len);

	switch (http->state) {
	case HTTP_STATE_IDLE:
		/* first chunk in p->payload */
		http->state = HTTP_STATE_HDR_RX;
		req->p = p;
		req->host = false;
		if (rtc_synced())
			rtc_get_datetime(&req->recv_t);
		else
			memset(&req->recv_t, 0, sizeof(req->recv_t));
		/* fall through */
	case HTTP_STATE_HDR_RX:
		if (req->p != p)
			pbuf_cat(req->p, p);
		req_parse_result = http_req_hdr_parse(req);
		switch (req_parse_result) {
		case REQ_PARSE_OK:
			http->state = HTTP_STATE_HDR_PARSED;
			if (req->clen > 0 &&
			    req->p->tot_len < req->clen + body_off(req))
				return ERR_OK;

			http->resp.status = HTTP_STATUS_OK;
			return http_resp_send(http);
		case REQ_PARSE_MORE:
			/* keep receiving */
			return ERR_OK;
		case REQ_PARSE_TOO_LARGE:
			http->state = HTTP_STATE_HDR_PARSE_FAIL;
			http->resp.status
				= HTTP_STATUS_REQ_HDR_FIELDS_TOO_LARGE;
			return http_resp_send(http);
		case REQ_BODY_TOO_LARGE:
			http->state = HTTP_STATE_HDR_PARSE_FAIL;
			http->resp.status = HTTP_STATUS_CONTENT_TOO_LARGE;
			return http_resp_send(http);
		case REQ_PROTO_NOT_SUPPORTED:
			http->state = HTTP_STATE_HDR_PARSE_FAIL;
			http->resp.status
				= HTTP_STATUS_HTTP_VERSION_NOT_SUPPORTED;
			return http_resp_send(http);
		case REQ_PARSE_ERR:
			http->state = HTTP_STATE_HDR_PARSE_FAIL;
			http->resp.status = HTTP_STATUS_BAD_REQUEST;
			return http_resp_send(http);
		default:
			http->state = HTTP_STATE_HDR_PARSE_FAIL;
			http->resp.status = HTTP_STATUS_INTERNAL_SERVER_ERROR;
			return http_resp_send(http);
		}
	case HTTP_STATE_HDR_PARSED:
		if (req->p != p)
			pbuf_cat(req->p, p);
		if (req->clen > 0 &&
		    req->p->tot_len < req->clen + body_off(req))
			return ERR_OK;
		http->resp.status = HTTP_STATUS_OK;
		return http_resp_send(http);
	case HTTP_STATE_HDR_PARSE_FAIL:
	default:
		http->state = HTTP_STATE_HDR_PARSE_FAIL;
		http->resp.status = HTTP_STATUS_INTERNAL_SERVER_ERROR;
		return http_resp_send(http);
	}
}

static err_t
http_poll(void *arg, struct altcp_pcb *tpcb)
{
	struct http *http;

	CAST_OBJ_NOTNULL(http, arg, HTTP_MAGIC);
	PICOW_HTTP_ASSERT(http->pcb == tpcb);

	/* XXX in recv state, check a recv timeout */

	if (http->state == HTTP_STATE_TX) {
		if (absolute_time_diff_us(http->resp.send_t,
					  get_absolute_time())
		    > http->send_tmo_us) {
			HTTP_LOG_DEBUG("%s: send timeout elapsed, "
				       "closing connection",
				       ipaddr_ntoa(http_remote_ip(http->pcb)));
			return http_abort(http);
		}

		return http_send_continue(http);
	}

	if (http->state == HTTP_STATE_CLOSING)
		return http_abort(http);
	if (http->state != HTTP_STATE_IDLE)
		return ERR_OK;

	if (absolute_time_diff_us(http->idle_t, get_absolute_time())
	    < http->idle_tmo_us)
		return ERR_OK;
	HTTP_LOG_DEBUG("%s: idle timeout elapsed, closing connection",
		       ipaddr_ntoa(http_remote_ip(http->pcb)));
	return http_abort(http);
}

static void
http_err(void *arg, err_t err)
{
	struct http *http;

	cyw43_arch_lwip_check();
	HTTP_LOG_ERROR("http err %d", err);
	if (err == ERR_CLSD || err == ERR_RST) {
		HTTP_LOG_DEBUG("http_err: closed or reset");
		return;
	}

	/* Just checking validity in a debug build. */
	CAST_OBJ_NOTNULL(http, arg, HTTP_MAGIC);
}

err_t
http_accept(void *arg, struct altcp_pcb *newpcb, err_t err)
{
	struct server *srv;
	struct http *http;

	CAST_OBJ_NOTNULL(srv, arg, SERVER_MAGIC);
	cyw43_arch_lwip_check();

	if (newpcb == NULL) {
		HTTP_LOG_ERROR("http_accept(): newpcb = NULL");
		return ERR_MEM;
	}
	if (err != ERR_OK) {
		HTTP_LOG_ERROR("http_accept(): err = %d", err);
		return err;
	}

	http = http_take();
	if (http == NULL) {
		HTTP_LOG_ERROR("Cannot allocate data structure for incoming "
			       "connection (struct http)");
		return ERR_MEM;
	}

	CHECK_OBJ(http, HTTP_MAGIC);
	INIT_OBJ(&http->req, REQ_MAGIC);
	INIT_OBJ(&http->resp, RESP_MAGIC);
	http->pcb = newpcb;
	http->srv_data = srv->srv_data;
	http->idle_tmo_us = srv->idle_tmo_us;
	http->send_tmo_us = srv->send_tmo_us;
	http->cx_priv.priv = NULL;
	http->cx_priv.fini = NULL;

	http->state = HTTP_STATE_IDLE;
	altcp_arg(newpcb, http);
	altcp_recv(newpcb, http_recv);
	altcp_err(newpcb, http_err);
	altcp_sent(newpcb, http_sent);
	altcp_poll(newpcb, http_poll, 1);
	altcp_nagle_disable(newpcb);
	HTTP_LOG_DEBUG("accepted from %s:%u",
		       ipaddr_ntoa(http_remote_ip(newpcb)),
		       http_remote_port(newpcb));
	http->idle_t = get_absolute_time();
	return ERR_OK;
}

static void
srv_err(void *arg, err_t err)
{
	(void)arg;
	HTTP_LOG_ERROR("Server error: %d", err);
}

err_t
http_cfg(const char *name, struct server_cfg *cfg)
{
	struct srv_data *srv_data;

	if (name == NULL || cfg == NULL)
		return ERR_ARG;
	srv_data = http_srv_data(name);
	if (srv_data == NULL)
		return ERR_VAL;
	CHECK_OBJ(srv_data, SRV_DATA_MAGIC);
	srv_data->srv_priv = NULL;
	srv_data->default_hndlr = NULL;
	srv_data->default_hndlr_priv.priv = NULL;
	srv_data->default_hndlr_priv.fini = NULL;
	srv_data->err_hndlr = NULL;
	srv_data->err_hndlr_priv.priv = NULL;
	srv_data->err_hndlr_priv.fini = NULL;

	cfg->ntp_cfg = ntp_default_cfg();
	cfg->srv_data = srv_data;
	cfg->ipaddr = IP_ADDR_ANY;
	cfg->ip_type = IPADDR_TYPE_ANY;
#if LWIP_ALTCP_TLS
	cfg->port = LWIP_IANA_PORT_HTTPS;
	cfg->tls = true,
#else
	cfg->port = LWIP_IANA_PORT_HTTP;
	cfg->tls = false,
#endif
	cfg->idle_tmo_s = HTTP_DEFAULT_IDLE_TMO_S;
	cfg->send_tmo_s = HTTP_DEFAULT_SEND_TMO_S;
	cfg->listen_backlog = HTTP_DEFAULT_LISTEN_BACKLOG;
	return ERR_OK;
}

err_t
http_srv_init(struct server **server, struct server_cfg *cfg)
{
	struct server *srv;
	altcp_allocator_t alloc, *allocp;
	err_t err, listen_err;
	static bool resp_hdr_pools_inited = false;

	if (cfg == NULL)
		return ERR_VAL;
	if (n_srv_running == 0 && cfg->ntp_cfg.servers == NULL)
		return ERR_VAL;
	if ((uint64_t)cfg->idle_tmo_s * 1000 * 1000 > INT64_MAX)
		return ERR_VAL;
	if ((uint64_t)cfg->send_tmo_s * 1000 * 1000 > INT64_MAX)
		return ERR_VAL;
	srv = srv_take();
	if (srv == NULL) {
		HTTP_LOG_ERROR("Server allocation failed");
		return ERR_MEM;
	}
	CHECK_OBJ(srv, SERVER_MAGIC);
	CHECK_OBJ_NOTNULL(cfg->srv_data, SRV_DATA_MAGIC);
	srv->srv_data = cfg->srv_data;
	srv->idle_tmo_us = cfg->idle_tmo_s * 1000 * 1000;
	srv->send_tmo_us = cfg->send_tmo_s * 1000 * 1000;

	if (get_allocator(srv->srv_data->cert_data, &alloc, &allocp,
			  cfg->tls) != 0) {
		srv_free(srv);
		return ERR_VAL;
	}
	srv->tls_alloc = allocp;

	cyw43_arch_lwip_begin();
	struct altcp_pcb *pcb = altcp_new_ip_type(allocp, cfg->ip_type);
	if (pcb == NULL) {
		cyw43_arch_lwip_end();
		HTTP_LOG_ERROR("Listener PCB allocation failed");
		if (srv->tls_alloc != NULL)
			free_tls_conf(srv->tls_alloc);
		srv_free(srv);
		return ERR_MEM;
	}

	err = altcp_bind(pcb, cfg->ipaddr, cfg->port);
	if (err != ERR_OK) {
		cyw43_arch_lwip_end();
		HTTP_LOG_ERROR("bind() failed: %d", err);
		if (srv->tls_alloc != NULL)
			free_tls_conf(srv->tls_alloc);
		srv_free(srv);
		return err;
	}

	srv->pcb = altcp_listen_with_backlog_and_err(pcb, cfg->listen_backlog,
						     &listen_err);
	if (srv->pcb == NULL) {
		altcp_close(pcb);
		cyw43_arch_lwip_end();
		HTTP_LOG_ERROR("listen() failed: %d", listen_err);
		if (srv->tls_alloc != NULL)
			free_tls_conf(srv->tls_alloc);
		srv_free(srv);
		return listen_err;
	}

	LWIP_MEMPOOL_INIT(http_pool);
	if (!resp_hdr_pools_inited) {
		LWIP_MEMPOOL_INIT(resp_hdr_small_pool);
		LWIP_MEMPOOL_INIT(resp_hdr_large_pool);
	}
	time_init(&cfg->ntp_cfg);
	altcp_arg(srv->pcb, srv);
	altcp_accept(srv->pcb, http_accept);
	altcp_err(srv->pcb, srv_err);

	cyw43_arch_lwip_end();
	*server = srv;
	n_srv_running++;
	return ERR_OK;
}

err_t
http_srv_fini(struct server *server)
{
	struct srv_data *srv_data;
	err_t err;

	if (server == NULL)
		return ERR_VAL;
	CHECK_OBJ(server, SERVER_MAGIC);
	CHECK_OBJ_NOTNULL(server->srv_data, SRV_DATA_MAGIC);
	srv_data = server->srv_data;

	cyw43_arch_lwip_begin();
	altcp_accept(server->pcb, NULL);
	altcp_err(server->pcb, NULL);
	altcp_arg(server->pcb, NULL);

	if ((err = altcp_close(server->pcb)) != ERR_OK) {
		cyw43_arch_lwip_end();
		return err;
	}
	cyw43_arch_lwip_end();

	if (server->tls_alloc != NULL)
		free_tls_conf(server->tls_alloc);
	if (n_srv_running <= 1)
		time_fini();
	if (srv_data->srv_priv != NULL && server->fini != NULL)
		server->fini(srv_data->srv_priv);
	if (srv_data->default_hndlr_priv.priv != NULL &&
	    srv_data->default_hndlr_priv.fini != NULL)
		srv_data->default_hndlr_priv.fini(
			srv_data->default_hndlr_priv.priv);
	if (srv_data->err_hndlr_priv.priv != NULL &&
	    srv_data->err_hndlr_priv.fini != NULL)
		srv_data->err_hndlr_priv.fini(
			srv_data->err_hndlr_priv.priv);
	srv_free(server);
	if (n_srv_running > 0)
		n_srv_running--;
	return ERR_OK;
}

ip_addr_t *
http_srv_ip(struct server *srv)
{
	CHECK_OBJ_NOTNULL(srv, SERVER_MAGIC);
	return http_local_ip(srv->pcb);
}

uint16_t
http_srv_port(struct server *srv)
{
	CHECK_OBJ_NOTNULL(srv, SERVER_MAGIC);
	return http_local_port(srv->pcb);
}

void
http_srv_set_priv(struct server *srv, void *priv, priv_fini_f *fini)
{
	CHECK_OBJ_NOTNULL(srv, SERVER_MAGIC);
	CHECK_OBJ_NOTNULL(srv->srv_data, SRV_DATA_MAGIC);
	srv->srv_data->srv_priv = priv;
	srv->fini = fini;
}
