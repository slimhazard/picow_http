## Sample `lwipopts.h`, `mbedtls_config.h` and `FreeRTOSConfig.h`

This directory contains samples for:

  * [`lwipopts.h`](lwipopts.h), the [configuration for the lwIP TCP/IP
	stack](https://gitlab.com/slimhazard/picow_http/-/wikis/Configuring-lwipopts.h)
	that is required for any use of picow-http.
  * [`mbedtls_config.h`](mbedtls_config.h) the [configuration for
	mbedtls](https://github.com/Mbed-TLS/mbedtls/blob/v2.28.1/include/mbedtls/config.h)
	that is required for use with the `picow_https` library, i.e. for
	TLS support.
  * [`FreeRTOSConfig.h`](FreeRTOSConfig.h), the
	[configuration](https://www.freertos.org/a00110.html) for an
	application based on [FreeRTOS](https://www.freertos.org/).

Developers of an application using picow-http can use the samples as a
starting point. It might be possible to use the sample headers as is,
or with only a few changed values.

### `lwipopts.h`

An important consideration for the configuration in `lwipopts.h` is
the number of concurrent client connections that the server should be
able to support. That in turn determines requirements for some of the
values that are set in the header.

The sample has a macro `MAX_CONCURRENT_CX_HINT`, for "maximum
concurrent connections hint". This can be set to the concurrency that
you would like your app to support. The sample sets a number of other
lwIP options based on this value, so that the concurrency goal can be
met.

The value for `MAX_CONCURRENT_CX_HINT` in the sample is conservative,
so that the "default" memory footprint of picow-http is low: it is a
common value among most browsers for the maximum number of connections
to a single server IP. It should suffice if one client application
connects to the server at a time.

By default, the sample sets `NO_SYS=0` for FreeRTOS builds, and
`NO_SYS=1` for other architectures. The `NO_SYS` setting can be
overridden, for example by setting a value with
`target_compile_definitions` in `CMakeLists.txt`.

Much of the rest of the sample `lwipopts.h` is based on the configuration
used for the
[pico_w examples](https://github.com/raspberrypi/pico-examples/blob/master/pico_w/lwipopts_examples_common.h)
in the [pico-examples repository](https://github.com/raspberrypi/pico-examples).

Bear in mind that the configuration in `lwipopts.h` impacts the
performance and resource usage of the app. In particular,
configuration for high concurrency increases memory footprint, and may
limit resources available to other functions that your app executes on
the PicoW.

See also:

  * [Configuring lwipopts.h](https://gitlab.com/slimhazard/picow_http/-/wikis/Configuring-lwipopts.h)
	on the [picow-http project Wiki](https://gitlab.com/slimhazard/picow_http/-/wikis/home)
  * [lwipopts.h](https://www.nongnu.org/lwip/2_1_x/group__lwip__opts.html)
	in the [lwip documentation](https://www.nongnu.org/lwip/2_1_x/)

### `mbedtls_config.h`

The values that may be set in `mbedtls_config.h` are documented in the
source of mbedtls'
[`config.h`](https://github.com/Mbed-TLS/mbedtls/blob/v2.28.1/include/mbedtls/config.h)
header (`mbedtls_config.h` overrides the definitions set there). In
many cases, it suffices to define a preprocessor macro -- `#define`
without any value.

The configuration determines the choice of ciphersuites, key exchange
algorithms, entropy gathering and numerous other details that impact
the security, performance and memory footprint of the application.
The details are beyond the scope of picow-http's documentation; see
`config.h` in mbedtls. Decisions about the configuration require
knowledge of TLS and its implementation in mbedtls.

The configuration in the sample header can be summarized as including:

  * TLS 1.2
	* TLS 1.3 is not available; it is only available in versions 3.x
	  of mbedtls, but lwIP is only compatible with mbedtls versions
	  2.x.
  * Ciphersuites:
	* RSA
	* ECDHE-ECDSA
  * Hash algorithms:
	* MD5
	* SHA1
	* SHA-224
	* SHA-256
	* SHA-384
	* SHA-512
  * Some specific choices for elliptic curves, see the source for
	details
  * CTR_DRBG random generator with AES256
  * PKCS#1 v1.5 encoding
  * CBC and GCM modes of operation (Cipher Block Chaining and
    Galois/Counter Mode)
  * SSL Server Name Indication (SNI)

The configuration also enables SHA256 and AES implementations that
trade off performance for lower memory footprint
(`MBEDTLS_SHA256_SMALLER` and `MBEDTLS_AES_FEWER_TABLES`).

Some configuration items are required for `picow_https` and are
defined in the library as compiler definitions; these should *not* be
undefined in a project's implementation of `mbedtls_config.h`. These
are:

  * `MBEDTLS_NO_PLATFORM_ENTROPY`
  * `MBEDTLS_ENTROPY_HARDWARE_ALT`
  * `MBEDTLS_HAVE_TIME`
  * `MBEDTLS_HAVE_TIME_DATE`

In short, these mean that the library provides a random function to
[seed cryptographic random number
generators](https://mbed-tls.readthedocs.io/en/latest/kb/how-to/add-entropy-sources-to-entropy-pool/),
and that system time functions may be used to check certificate
validity (due to [NTP time
synchronization](https://slimhazard.gitlab.io/picow_http/group__ntp.html)).

`mbedtls_config.h` is only required for use with the `picow_https`
library; that is, for TLS support. It may be left out of a build that
links `picow_http` (and hence TLS is not required).

### `FreeRTOSConfig.h`

The configuration in
[`FreeRTOSConfig.h`](https://www.freertos.org/a00110.html) tailors
properties of the FreeRTOS kernel to an application, including
properties of the scheduler, synchronization primitives, memory
allocation, timers, and numerous other details. Some of them determine
which FreeRTOS headers are included and which API functions are
compiled, so as to limit the kernel's binary footprint.

Two configuration items are specific to the RP2040 port:

* `configSUPPORT_PICO_SYNC_INTEROP`: if non-zero, synchronization primitives
  from the SDK's
  [pico_sync](https://www.raspberrypi.com/documentation/pico-sdk/high_level.html#pico_sync)
  library, such as
  [mutexes](https://www.raspberrypi.com/documentation/pico-sdk/group__mutex.html)
  and [semaphores](https://www.raspberrypi.com/documentation/pico-sdk/group__sem.html),
  as well as [queues](https://www.raspberrypi.com/documentation/pico-sdk/group__queue.html)
  from [pico_util](https://www.raspberrypi.com/documentation/pico-sdk/high_level.html#pico_util),
  will work correctly when called from FreeRTOS tasks.
* `configSUPPORT_PICO_TIME_INTEROP`: if non-zero, the functions
  [`sleep_ms`](https://www.raspberrypi.com/documentation/pico-sdk/group__sleep.html#gae617a6842d4f3a192064c4354b88fcff),
  [`sleep_us`](https://www.raspberrypi.com/documentation/pico-sdk/group__sleep.html#rpipf892cd315803e95b5009),
  and [`sleep_until`](https://www.raspberrypi.com/documentation/pico-sdk/group__sleep.html#rpip1318f1b8a12bfbb80a57)
  from the SDK's
  [pico_time](https://www.raspberrypi.com/documentation/pico-sdk/high_level.html#pico_time)
  work correctly when called from FreeRTOS tasks, blocking at the FreeRTOS
  level.

The default value for both of `configSUPPORT_PICO_SYNC_INTEROP` and
`configSUPPORT_PICO_TIME_INTEROP` is 1. picow-http doesn't depend on
any of these functions; but the default is probably the best choice
for most applications, particularly those that do use the SDK
functions.

A [set of configuration
options](https://www.freertos.org/symmetric-multiprocessing-introduction.html#smp-specific-configuration-options)
is specific to support of [symmetric
multiprocessing](https://www.freertos.org/symmetric-multiprocessing-introduction.html)
(SMP) in FreeRTOS. One of these is
[`configNUMBER_OF_CORES`](https://www.freertos.org/symmetric-multiprocessing-introduction.html#confignumberofcores),
which specifies the number of processor cores available to the
scheduler.  By default, `configNUMBER_OF_CORES` is set to 2 in the
sample header; hence SMP is the default in the sample
configuration. But `configNUMBER_OF_CORES` may be overridden by
compile definition; set `configNUMBER_OF_CORES=1` for single-core
FreeRTOS.

  * Prior to the merge of the smp development branch into the main branch
	as of FreeRTOS V11.0.0, the name of this item was `configNUM_CORES`.
	The [sample
	header](FreeRTOSConfig.h) includes this definition for compatibility:

```
#define configNUM_CORES configNUMBER_OF_CORES
```

Another SMP-related item is
[`configRUN_MULTIPLE_PRIORITIES`](https://www.freertos.org/symmetric-multiprocessing-introduction.html#configrunmultiplepriorities),
which is set to 1 in the sample, but may be overridden. The default
setting allows tasks with different priorities to run simultaneously
on the two cores. It may be an issue if you are adapting a previous
single-core FreeRTOS app to use picow-http and SMP, and the app
depends on the property that lower-priority tasks may never run before
higher-priority tasks. If so, consider setting
`configRUN_MULTIPLE_PRIORITIES` to 0. The sample's default is based on
the assumption that new picow-http projects using FreeRTOS will be
designed for SMP from the outset, and follow [FreeRTOS'
advice](https://www.freertos.org/symmetric-multiprocessing-introduction.html#porting)
that SMP apps should rely on synchronization primitives, not
priorities, for mutual exclusion.

Details of FreeRTOS configuration are beyond the scope of this
documentation; see the [FreeRTOS
docs](https://www.freertos.org/a00110.html) for more information.  The
[sample](FreeRTOSConfig.h) provided here is largely based on the
versions in the [FreeRTOS demo
apps](https://github.com/FreeRTOS/FreeRTOS-Community-Supported-Demos/tree/main/CORTEX_M0%2B_RP2040)
for the RP2040 port, and the [sample
apps](https://github.com/raspberrypi/pico-examples/tree/master/pico_w/wifi/freertos)
in the [pico-examples repository](https://github.com/raspberrypi/pico-examples).

`FreeRTOSConfig.h` is always required for applications that use the
`pico_cyw43_arch_lwip_sys_freertos` [network
architecture](https://www.raspberrypi.com/documentation/pico-sdk/networking.html#detailed-description33).
It is also required for FreeRTOS in "nosys" mode, where FreeRTOS is
linked with `pico_cyw43_arch_lwip_threadsafe_background` and `NO_SYS`
is set to 1 in the lwip configuration. `FreeRTOSConfig.h` can be left
out of an application that uses one of the other architectures.

### Including the headers

For an application build, the headers must be specified as source
files (in the CMake `add_executable()` directive), and their
directories must be added to the include path (with
`target_include_directories()`).

If you include picow-http as a git submodule in your project, _and_
you are using the sample headers without changes, this is simple to
achieve:

```cmake
# In CMakeLists.txt; assuming that CMakeLists.txt is in the project root
# directory.

# Assume furthermore that picow-http is a git submodule in the
# lib/picow-http subdirectory.

# Specify the headers at the etc/ subdirectory in picow-http as source
# files.
#
# 	mbedtls_config.h is only needed for the picow_https library
# 	(i.e. for TLS support).
#
# 	FreeRTOSConfig.h is only needed for FreeRTOS builds.
add_executable(my_app
	# ...
	${CMAKE_CURRENT_LIST_DIR}/lib/picow-http/etc/lwipopts.h
	${CMAKE_CURRENT_LIST_DIR}/lib/picow-http/etc/mbedtls_config.h
	${CMAKE_CURRENT_LIST_DIR}/lib/picow-http/etc/FreeRTOSConfig.h
	# ... other source files here ...
)

# Add the etc/ subdirectory of picow-http to the include path.
target_include_directories(my_app
	# ...
	${CMAKE_CURRENT_LIST_DIR}/lib/picow-http/etc
	# ... other include directories here ...
)
```

If you make copies of the headers with modified values, then the
locations of the copies must be specified in `add_executable()`, and
their directories in `target_include_directories()`.
