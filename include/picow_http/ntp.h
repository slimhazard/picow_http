/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

#include "pico/time.h"

/**
 * @file
 * NTP time synchronization
 *
 * @defgroup ntp NTP time synchronization
 *
 * The server requires an accurately synchronized clock, among other things
 * because:
 *
 * * for TLS, the certficate's [validity
 *   period](https://en.wikipedia.org/wiki/X.509#Structure_of_a_certificate)
 *   is checked. When an https connection is initiated and the TLS
 *   handshake is performed, the client will verify that the current time
 *   lies between the "Not Before" and "Not After" times of the
 *   certificate. With an accurate clock, mbedtls can be configured to
 *   check the certificate's validity.
 * * for HTTP generally (with or without TLS), the [`Date` response
 *   header](https://datatracker.ietf.org/doc/html/rfc9110#name-date)
 *   must represent an accurate time. The value of `Date` is evaluated in the
 *   [age calculation](https://httpwg.org/specs/rfc7234.html#age.calculations)
 *   of a response, which supports a client's decisions about caching
 *   responses.

 * The `Date` header is generated automatically by the server, so
 * application code does not need to do so.
 *
 * picow-http uses the PicoW's [real time
 * clock](https://raspberrypi.github.io/pico-sdk-doxygen/group__hardware__rtc.html)
 * (RTC) to keep the time, synchronized to UTC time using the [Network
 * Time Protocol](https://en.wikipedia.org/wiki/Network_Time_Protocol)
 * (NTP). When an HTTP server is started, the following actions are
 * initiated (running asynchronously):
 *
 * * the RTC is set to run from the PicoW's [crystal
 *   oscillator](https://raspberrypi.github.io/pico-sdk-doxygen/group__hardware__xosc.html)
 *   (XOSC)
 * * a DNS lookup is initiated for the first NTP server listed in the
 *   configuration (see struct ntp_cfg)
 * * if the lookup succeeds, a request is sent to the server
 * * if a response is received successfully from the NTP server, then
 *   the RTC is set to UTC time according to the response
 * * the time is also set so that
 *   [`time(NULL)`](https://sourceware.org/newlib/libc.html#time) from
 *   C's `time.h` returns the synchronized epoch time
 * * at relatively long intervals, a "refresh" request is sent to the next
 *   NTP server to update the clock (in order to minimize clock drift)
 *
 * These operations are started as soon as the server starts, so there is
 * a brief interval just after server startup (usually a few seconds) in
 * which the RTC is not synchronized. During that interval:
 *
 * * no `Date` header is generated in response headers
 * * the date/time field in @ref reqlog "request logs" is empty
 * * `time(NULL)` does not return the current time
 *
 * rtc_synced() can be used to check whether the RTC has been successfully
 * synced with an NTP server.
 *
 * If any of the operations described above fail (such DNS lookup or the
 * NTP request), they are retried after an interval set in the
 * configuration (see struct ntp_cfg). For refreshes and retries, the NTP
 * servers listed in the configuration are attempted in order.
 *
 * NTP synchronization is initialized when @ref http_srv_init() is called
 * for the first (and possibly only) HTTP server. If you call
 * @ref http_srv_init() for one or more additional servers (for example to
 * implement a server without TLS listening on port 80 and a server with
 * TLS on port 443), the NTP configuration of the second and any other
 * initialized HTTP server is ignored.
 *
 * NTP synchronization stops when the last running HTTP server is stopped
 * with @ref http_srv_fini(). If a server is stopped while other HTTP
 * servers are still running, NTP synchronization continues.
 */

#ifndef _NTP_H
#define _NTP_H

#include <stdbool.h>

/**
 * @brief Default NTP server addresses
 * @ingroup ntp
 *
 * The NTP server addresses set in the default configuration -- a
 * whitespace-separated list of host names or IP addresses.
 *
 * The default references an open source vendor pool with worldwide scope;
 * consider using a pool or server that is "closer" to the location where
 * your PicoW is deployed.
 *
 * @see ntp_default_cfg()
 */
#ifndef NTP_DEFAULT_SERVERS
#define NTP_DEFAULT_SERVERS					\
	"0.slimhazard.pool.ntp.org 1.slimhazard.pool.ntp.org "	\
	"2.slimhazard.pool.ntp.org 3.slimhazard.pool.ntp.org"
#endif

/**
 * @brief Default NTP response timeout
 * @ingroup ntp
 *
 * Default timeout in seconds for the response to an NTP client request.
 *
 * @see ntp_default_cfg()
 */
#ifndef NTP_DEFAULT_RESPONSE_TMO_S
#define NTP_DEFAULT_RESPONSE_TMO_S	(15)
#endif

/**
 * @brief Default NTP retry interval
 * @ingroup ntp
 *
 * Default interval in seconds after which failed NTP-related operations
 * are re-attempted, such as an NTP client request or DNS lookup.
 *
 * The default is a customary minimum interval for NTP clients between
 * retry attempts, so as not to overload NTP servers.
 *
 * @see ntp_default_cfg()
 */
#ifndef NTP_DEFAULT_RETRY_INTVL_S
#define NTP_DEFAULT_RETRY_INTVL_S	(15)
#endif

/**
 * @brief Default timeout for DNS resolution of the NTP server hostname
 * @ingroup ntp
 *
 * Default timeout in seconds for the DNS request used to resolve the NTP
 * server hostname.
 *
 * @see ntp_default_cfg()
 */
#ifndef NTP_DEFAULT_DNS_TMO_S
#define NTP_DEFAULT_DNS_TMO_S		(10)
#endif

/**
 * @brief Default interval for NTP refresh requests
 * @ingroup ntp
 *
 * Default interval in seconds after which NTP synchronization is refreshed.
 * After a synchronization succeeds, and then this interval elapses, a new
 * NTP request is sent to the next configured server.
 *
 * The default interval is chosen so that the clock is not likely to drift
 * by much more than a second, assuming the accuracy of 30ppm documented
 * for the crystal oscillator used on the PicoW.
 *
 * @see ntp_default_cfg()
 */
#ifndef NTP_DEFAULT_REFRESH_INTVL_S
#define NTP_DEFAULT_REFRESH_INTVL_S	(30000)
#endif

/**
 * @brief NTP configuration
 * @ingroup ntp
 *
 * @see ntp_default_cfg(), struct server_cfg
 */
struct ntp_cfg {
	/**
	 * Whitespace-separated list of NTP server addresses; may be host
	 * names or IP addresses. MAY NOT be `NULL`.
	 */
	const char	*servers;
	/** NTP client response timeout in seconds */
	unsigned	response_tmo_s;
	/** NTP retry interval in seconds. Failed operations (such
	 *  as an NTP client request or DNS lookups) are re-attempted
	 *  after this interval.
	 */
	unsigned	retry_intvl_s;
	/** Response timeout in seconds for DNS resolution of the NTP
	 *  server hostname
	 */
	unsigned	dns_tmo_s;
	/** NTP refresh interval in seconds. After an NTP synchronization
	 *  succeeds and then this interval elapses, a new NTP client
	 *  request is sent (to minimize clock drift).
	 *
	 * The minimum refresh interval is 64 seconds. Lower values are
	 * silently set to the minimum.
	 */
	unsigned	refresh_intvl_s;
};

/**
 * @brief Weekday name abbreviations
 * @ingroup util
 *
 * Weekday name abbreviations as used in the [preferred HTTP date
 * format](https://www.rfc-editor.org/rfc/rfc9110.html#http.date).
 *
 * `"Sun"` (for Sunday) is at index 0; so the indices correspond to the
 * numbering of the `wday` field of the Pico SDK's
 * [`datetime_t`](https://raspberrypi.github.io/pico-sdk-doxygen/structdatetime__t.html),
 * and of the `tm_wday` member of `struct tm` from C's `time.h`.
 */
static const char *wday_name[] = {
	"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
};

/**
 * @brief Month name abbreviations
 * @ingroup util
 *
 * Month name abbreviations as used in the [preferred HTTP date
 * format](https://www.rfc-editor.org/rfc/rfc9110.html#http.date).
 *
 * `"Jan"` (for January) is at index 0; so the indices correspond to the
 * numbering of the `tm_mon` member of `struct tm` from C's `time.h`.
 *
 * Subtract 1 from the `month` field of the Pico SDK's
 * [`datetime_t`](https://raspberrypi.github.io/pico-sdk-doxygen/structdatetime__t.html)
 * to get the index for this array -- January is 1 in `datetime_t`'s
 * `month`.
 */
static const char *month_name[] = {
	"Jan", "Feb", "Mar", "Apr", "May", "Jun",
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
};

void time_init(struct ntp_cfg *cfg);
void time_fini(void);

/**
 * @brief Return true if the RTC has been synchronized
 * @ingroup ntp
 *
 * @return true if an NTP client request has succeeded, and the
 *         RTC has been set to UTC time
 */
bool rtc_synced(void);

/**
 * @brief Get the NTP default configuration
 * @ingroup ntp
 *
 * Returns an instance of struct ntp_cfg whose fields are set as follows:
 *
 * * `servers`: `NTP_DEFAULT_SERVERS`
 * * `response_tmo_s`: `NTP_DEFAULT_RESPONSE_TMO_S`
 * * `retry_intvl_s`: `NTP_DEFAULT_RETRY_INTVL_S`
 * * `dns_tmo_s`: `NTP_DEFAULT_DNS_TMO_S`
 * * `refresh_intvl_s`: `NTP_DEFAULT_REFRESH_INTVL_S`
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // Initialize cfg to defaults, so that we only have to change
 * // individual fields.
 * struct ntp_cfg cfg = ntp_default_cfg();
 *
 * // Use the NTP pool in Germany
 * cfg.server = "0.de.pool.ntp.org 1.de.pool.ntp.org 2.de.pool.ntp.org";
 * @endcode
 *
 * @return an NTP configuration set to default values
 */
static inline struct ntp_cfg
ntp_default_cfg(void)
{
	return (struct ntp_cfg) {
		.servers		= NTP_DEFAULT_SERVERS,
		.response_tmo_s		= NTP_DEFAULT_RESPONSE_TMO_S,
		.retry_intvl_s		= NTP_DEFAULT_RETRY_INTVL_S,
		.dns_tmo_s		= NTP_DEFAULT_DNS_TMO_S,
		.refresh_intvl_s	= NTP_DEFAULT_REFRESH_INTVL_S,
	};
}

#endif /* _NTP_H */
