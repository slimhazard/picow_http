/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

/**
 * @file
 * Public API of the picow-http server.
 *
 * This API may be used to configure, start and stop an HTTP server; and
 * to implement custom handlers, which generate dynamic HTTP responses.
 *
 * @defgroup server Server management and HTTP connections
 * Configure, start and stop HTTP servers; and access the current HTTP
 * connection, including the current request and response.
 *
 * @section priv Application private data
 *
 * Several functions of the API support optional access to private data --
 * a pointer to any object provided by the application. These include:
 *
 * * http_srv_set_priv() for server-scoped private data; the pointer is
 *   retrieved with http_srv_priv().
 * * http_cx_set_priv() for connection scope; retrieve with http_cx_priv().
 * * Handler-scoped private data may be set with register_hndlr() or
 *   register_hndlr_methods(). The pointer is passed in as a parameter
 *   to the @ref hndlr_f "custom handler function".
 *
 * For example, the private object may be a pointer to a struct storing
 * configuration data, or to a sensor measurement value that is updated
 * elsewhere in the application. The server never touches these objects;
 * it just stores the pointers and hands them back when requested.
 *
 * Private data is optional; if the application doesn't need it, set the
 * pointer parameter to `NULL`, or just don't call the setter function.
 *
 * As indicated above, each type of private object has a @em scope -- a
 * lifetime during which the pointer may be dereferenced:
 *
 * * Server scope: from server start with http_srv_init() until that
 *   server is stopped with http_srv_fini()
 * * Connection scope: from setting the private object with
 *   http_cx_set_priv() for a @ref http "struct http" that represents a
 *   network connection with a client, until the connection is closed. In
 *   other words, for the duration of a keep-alive connection (which may
 *   encompass several HTTP request/response transactions).
 * * Handler scope: from setting the private object with register_hndlr()
 *   or register_hndlr_methods(), until the server is stopped.
 *
 * @attention A private pointer MUST point to memory that can be safely
 * accessed throughout the lifetime of its scope. For example, a pointer
 * to static storage is safe, or a pointer that was returned by `malloc()`
 * or obtained through [lwIP's memory pool
 * facility](https://www.nongnu.org/lwip/2_1_x/group__mempool.html). In
 * most cases, it is @em not safe to use a pointer to stack memory, such
 * as a function local variable.
 *
 * If the contents of private data will be modified by both cores, it may
 * be necessary to synchronize access, for example with the help of the
 * SDK's
 * [pico_sync](https://www.raspberrypi.com/documentation/pico-sdk/high_level.html#pico_sync)
 * library.
 *
 * An application may also specify an optional @ref priv_fini_f
 * "finalizer" for private data. A finalizer function, defined by the
 * application, is invoked for the private pointer when it goes out of
 * scope. This gives the application an opportunity to free any resources
 * stored by the private object, and/or to free the pointer itself. If the
 * pointer was returned by `malloc`, the finalizer function can just be
 * `free` (if nothing else requires cleanup).
 *
 * A finalizer may be unnecessary; for example, a pointer to static
 * storage must @em not be freed. In such cases, set the finalizer
 * parameter to `NULL`. On the other hand, it may be critical to provide a
 * finalizer in order to prevent resource leakage.
 *
 * This is an example of connection-scoped private data, using
 * http_cx_set_priv() and http_cx_priv(). The private object is a struct
 * with an integer member that functions as a counter for the
 * request/response transactions on a keep-alive connection. This
 * illustrates allocation and finalization of private data with [lwIP
 * memory pools](https://www.nongnu.org/lwip/2_1_x/group__mempool.html),
 * which are usually more efficient and lead to less memory fragmentation
 * than `malloc` and `free`.
 *
 * @code{.c}
 * #include "picow_http/http.h"
 * #include "lwip/memp.h"
 *
 * // The private object will have this type.
 * // Using the "magic number" idiom from picow_http/assertions.h.
 * struct cx_data {
 *         unsigned magic;
 * #define CX_DATA_MAGIC (0x47110815)
 *         int ctr;
 * }
 *
 * // Declare an lwIP memory pool.
 * // Assume that N_OBJS was #defined as the number of objects in the pool.
 * LWIP_MEMPOOL_DECLARE(cx_priv, N_OBJS, sizeof(cx_data),
 *                      "Connection-scoped private data");
 *
 * // A private pointer is passed into the finalizer when it goes out
 * // of scope; in this case when the connection closes.
 * // Use lwIP's mechanism to free the object in the pool.
 * static void
 * cx_priv_free(void *p)
 * {
 *         LWIP_MEMPOOL_FREE(cx_priv, p);
 * }
 *
 * // A custom response handler that uses connection-scoped private data.
 * err_t
 * cx_priv_hndlr(struct http *http, void *priv)
 * {
 *         struct resp *resp = http_resp(http);
 *         struct cx_data *priv_data;
 *         err_t err;
 *
 *         // Get the private object in the current connection scope.
 *         priv_data = http_cx_priv(http);
 *
 *         // If http_cx_priv() returns NULL, then no private object
 *         // was set previously for this connection.
 *         if (priv_data == NULL) {
 *              // Allocate an object from the pool.
 *              priv_data = LWIP_MEMPOOL_ALLOC(cx_priv);
 *              if (priv_data == NULL) {
 *         		// Error handling ...
 *              }
 *              // Initialize the counter to 1, and set it as the
 *              // the private object for the connection. The finalizer
 *              // function is specified here.
 *              INIT_OBJ(priv_data, CX_DATA_MAGIC);
 *              priv_data->ctr = 1;
 *              http_cx_set_priv(http, priv_data, cx_priv_free);
 *         }
 *         else {
 *              // A non-NULL pointer returned from http_cx_priv() was
 *              // set previously on the current connection. Check the magic
 *              // number for safety, and increment the counter.
 *              CHECK_OBJ(priv_data, CX_DATA_MAGIC);
 *              priv_data->ctr++;
 *         }
 *
 *         // ...
 * }
 *
 * int
 * main(void)
 * {
 *          // ...
 *
 *          // Initialize the memory pool in main().
 *          LWIP_MEMPOOL_INIT(cx_priv);
 *
 *          // ...
 * }
 * @endcode
 *
 * @defgroup req Requests
 * Read properties of the current HTTP request.
 *
 * @defgroup resp Responses
 * Set HTTP response headers and body, and send responses to the client
 *
 * @section resp_bodies Sending response bodies
 *
 * The API includes two functions for sending response bodies:
 *
 * * http_resp_send_buf(), to send the contents of a fixed-length buffer,
 *   using the `Content-Length` response header.
 * * http_resp_send_chunk(), to send the body incrementally in chunks
 *   using [chunked transer encoding](https://datatracker.ietf.org/doc/html/rfc9112#section-7.1),
 *   with the header field `Transfer-Encoding: chunked`.
 *
 * Use only one or the other of these two mechanisms to send a response
 * body. Exactly one of `Content-Length` or `Transfer-Encoding: chunked`
 * must be included in the response header.
 *
 * Both of these functions have a boolean parameter `durable` to indicate
 * whether the response contents can be queued for send without copying
 * the buffer.
 *
 * Packets containing the response contents are sent asynchronously, and
 * are retained by the TCP stack (potentially for re-send if not
 * acknowledged by the network peer). This process may continue after the
 * handler calling the send function has completed. Set `durable` to true
 * if the buffer passed to the function is allocated for a lifetime that
 * persists at least until the TCP send operation is fully complete -- in
 * this case the buffer will not be copied. Set `durable` to false
 * otherwise, so that the buffer is copied for the TCP stack.
 *
 * For example, if the pointer to response contents points to static
 * storage, or has been returned from `malloc()`, then `durable` may be
 * set to true. If it points to memory on the stack, such as an array
 * declared as a local variable within a handler, then `durable` MUST be
 * false, since the TCP stack must retain a copy that persists after
 * completion of the handler invocation.
 *
 * Zero-copy send is more efficient, in terms of both RAM usage and
 * processing time. If your application allows it, prefer sending response
 * bodies with the `durable` parameter set to true.
 *
 * http_resp_err() is an alternative to http_resp_send_buf() and
 * http_resp_send_chunk() that may be used when the response status
 * is >= 400. By default, it sends a simple, standard error response body
 * built in to the library. Optionally, register_error_hndlr() may be used
 * to register a custom error handler for response codes specified in the
 * [build
 * configuration](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration).
 *
 * @section resp_hdr Sending response headers
 *
 * The response header may be sent separately from the body with
 * http_resp_send_hdr(). If the response status has not been set (with
 * http_resp_set_status()) before sending the header, it set to 200.
 *
 * Functions that send the response body also send the header, if it has
 * not already been sent. http_resp_send_chunk() sends the header, if
 * needed, before sending the first chunk. Calling http_resp_send_hdr()
 * before sending the body is not necessary (but harmless).
 *
 * To use the API properly, be aware of the kinds of responses that,
 * according to [HTTP
 * standards](https://datatracker.ietf.org/doc/html/rfc9110#section-6.4.1),
 * may only contain a header, but no body. These are:
 *
 * * Responses to requests with the `HEAD` method.
 * * Responses with status [204 ("No
 *   content")](https://datatracker.ietf.org/doc/html/rfc9110#name-204-no-content)
 *   or [304 ("Not
 *   Modified")](https://datatracker.ietf.org/doc/html/rfc9110#section-15.4.5).
 * * Response with a status less than 200.
 *
 * In these cases, just http_resp_send_hdr() is sufficient. If either of
 * http_resp_send_buf() or http_resp_send_chunk() are called for such a
 * response, a debug level message is printed to the log, the body
 * contents are ignored, but the functions return `ERR_OK`. Only using a
 * function that sends a body is a convenient way for a handler to respond
 * to both `GET` and `HEAD` requests -- the function does the right thing
 * for `HEAD` requests transparently.
 *
 * @section resp_hdr_pools Memory allocation for response headers
 *
 * picow-http uses lwIP's [memory
 * pools](https://www.nongnu.org/lwip/2_1_x/group__mempool.html) to
 * allocate space for response headers; no dynamic allocation for response
 * headers takes place at runtime. This is done transparently by the
 * library, with no requirements for the application developer. But you
 * can configure the pools and buffer sizes, for example to lower the
 * memory footprint, or if the application requires large headers.
 *
 * Two pools are used, for "small" and "large" response headers. The
 * assumption is that most response headers will require no more than the
 * small size, and the large size will only be used in rare exceptions. A
 * buffer from the small pool is always used initially; a buffer from the
 * large pool is used if the header exceeds the small buffer size. No
 * response header may exceed the large size. Buffers are returned to
 * their pools after a response has been sent.
 *
 * http_resp_set_hdr() and its variants fail with the `ERR_MEM` error code
 * if it needs a buffer from a memory pool that is exhausted, of if the
 * header's size exceeds the large buffer size.
 *
 * The memory pools are configured by these macros, whose default values
 * can be overridden with compile-time definitions:
 *
 * * #RESP_HDR_SMALL_BUF_SZ: size of buffers in the small pool
 * * #RESP_HDR_SMALL_POOL_SZ: number of buffers in the small pool
 * * #RESP_HDR_LARGE_BUF_SZ: size of buffers in the large pool
 * * #RESP_HDR_LARGE_POOL_SZ: number of buffers in the large pool
 *
 * @defgroup util Utilities
 * Helper functions and structures that are used by the server core code
 * and may also be useful for application code.
 */

#ifndef _PICOW_HTTP_H
#define _PICOW_HTTP_H

#include <string.h>

#include "lwip/ip_addr.h"
#include "lwip/prot/iana.h"
#include "lwip/err.h"

#include "assertion.h"
#include "log.h"
#include "ntp.h"
#include "version.h"

/*
 * Declarations required *before* including http_priv.h.
 */

/**
 * @brief Maximum number of request headers
 * @ingroup req
 *
 * Maximum number of headers permitted in an HTTP request. If the server
 * receives a request with more headers, it returns an error response with
 * status 431 ("Request header fields too large").
 *
 * The default (if `HTTP_REQ_MAX_HDRS` is not previously defined) is 16.
 * Note that most browsers usually send fewer than 10 headers.
 *
 * To change the maximum, define `HTTP_REQ_MAX_HDRS`, for example by
 * `#define`-ing it prior to including `picow_http/http.h`, or by passing
 * in a definition as a compile option.
 */
#ifndef HTTP_REQ_MAX_HDRS
#define HTTP_REQ_MAX_HDRS (16)
#endif

/**
 * @struct http
 * @brief Current HTTP connection, request and response
 * @ingroup server
 *
 * The `http` object represents a persistent connection with a client,
 * and is passed into custom response handlers.
 *
 * The member fields of `struct http` are opaque in the public API.
 * Functions for accessing its properties include:
 *
 * * http_req() and http_resp() to access the current HTTP request and
 *   response.
 * * http_cx_local_ip(), http_cx_local_port(), http_cx_remote_ip() and
 *   http_cx_remote_port() to access properties of the current network
 *   connection.
 * * http_cx_set_priv() and http_cx_priv(), http_cx_remote_ip() to use
 *   @ref priv "connection-scoped private data".
 *
 * @see http_req(), http_resp(), http_cx_local_ip(), http_cx_local_port(),
 *      http_cx_remote_ip(), http_cx_remote_port(), http_cx_set_priv(),
 *      http_cx_priv(), @ref priv
 */
struct http;

/**
 * @brief Function type for custom response handlers
 * @ingroup resp
 *
 * Function typedef for custom response handlers. Custom handlers
 * implement a function with this signature, which is responsible for
 * generating HTTP responses to requests received at the endpoint for
 * which it is registered.
 *
 * @param[in] http HTTP object for the current connection.<br>
 *                 Objects representing the current request and
 *                 response are accessed via http_req() and http_resp().
 * @param[in] priv Optional pointer passed to the register function<br>
 *                 `NULL` if no such object was configured
 * @return         `ERR_OK` on success, or an error status on failure
 * @see            http_req(), http_resp(), register_hndlr(),
 *                 register_hndlr_methods(), register_default_hndlr(),
 *                 register_error_hndlr(),
 *                 [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
typedef err_t (*hndlr_f)(struct http *http, void *priv);

/**
 * @enum http_method_t
 * @brief Request method
 * @ingroup req
 *
 * Enum representing
 * [HTTP request methods](https://www.rfc-editor.org/rfc/rfc9110.html#name-methods).
 *
 * @see http_req_method()
 */
enum http_method_t {
	HTTP_METHOD_GET = 0,	/**< Method `GET` */
	HTTP_METHOD_HEAD,	/**< Method `HEAD` */
	HTTP_METHOD_POST,	/**< Method `POST` */
	HTTP_METHOD_PUT,	/**< Method `PUT` */
	HTTP_METHOD_DELETE,	/**< Method `DELETE` */
	HTTP_METHOD_CONNECT,	/**< Method `CONNECT` */
	HTTP_METHOD_OPTIONS,	/**< Method `OPTIONS` */
	HTTP_METHOD_TRACE,	/**< Method `TRACE` */
	__HTTP_METHOD_MAX,	/**< Symbol for the numer of methods */
};

/**
 * @brief Bitmap for methods GET and HEAD
 * @ingroup req
 *
 * Bitmap in which the bit positions at `HTTP_METHOD_GET` and
 * `HTTP_METHOD_HEAD` are set. This is a convenience for use with
 * register_hndlr_methods(), to register custom response handlers for the
 * `GET` and `HEAD` methods and the same path.
 *
 * @see enum http_method_t, register_hndlr_methods()
 */
#define HTTP_METHODS_GET_HEAD \
	((1U << HTTP_METHOD_GET) | (1U << HTTP_METHOD_HEAD))

/**
 * @brief Function type for private data finalizers
 * @ingroup server
 *
 * Function typedef for callbacks that finalize private data. The
 * finalizer is called when the private object goes out of scope, so that,
 * for example, allocated memory can be freed, or other kinds of resource
 * cleanup can be performed.
 *
 * See the discussion and example in @ref priv.
 *
 * @param[in] p pointer to private data that was initially provided by
 *              the application. SHALL NOT be `NULL`.
 * @see http_cx_set_priv(), @ref priv
 */
typedef void priv_fini_f(void *p);

#include "http_priv.h"

/**
 * @brief Set a connection-scoped private object
 * @ingroup server
 *
 * Sets a private object for `http`, which can be retrieved with
 * http_cx_priv(). `priv` can be a pointer to an object of any type. It is
 * available for the lifetime of the network connection represented by
 * `http`; that is, it can be retrieved during every request/response
 * transaction on the same keep-alive connection.
 *
 * `fini` is an optional finalizer function for `priv`. If `fini` is not
 * `NULL`, then it is invoked with `priv` as its argument when the
 * connection closes. That gives an application the opportunity to
 * deallocate resources and do any necessary cleanup. Set `fini` to `NULL`
 * if no finalizer is needed.
 *
 * See the discussion and example in @ref priv.
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @param[in] priv pointer to an object of any type
 * @param[in] fini optional finalizer function for `priv`; set to `NULL`
 *                 if no finalizer is needed
 * @see http_cx_priv(), typedef priv_fini_f, @ref priv
 */
static inline void
http_cx_set_priv(struct http *http, void *priv, priv_fini_f *fini)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	http->cx_priv.priv = priv;
	http->cx_priv.fini = fini;
}

/**
 * @brief Return the connection-scoped private object, if any
 * @ingroup server
 *
 * Return the private object previously set by http_cx_set_priv() for the
 * client connection represented by `http`. If http_cx_set_priv() was
 * never called for the current connection, http_cx_priv() returns `NULL`.
 *
 * See the discussion and example in @ref priv.
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @return    pointer to an object set for `http` in http_cx_set_priv()<br/>
 *            `NULL` if http_cx_set_priv() was not previously called for
 *            `http`
 * @see http_cx_set_priv(), @ref priv
 */
static inline void *
http_cx_priv(struct http *http)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	return http->cx_priv.priv;
}

/**
 * @enum http_status_t
 * @brief enum for HTTP response status codes
 * @ingroup resp
 *
 * Enum representing [HTTP response status
 * codes](https://www.rfc-editor.org/rfc/rfc9110.html#name-status-codes)
 */
enum http_status_t {
	/** 100 Continue */
	HTTP_STATUS_CONTINUE			= 100,
	/** 101 Switching Protocols */
	HTTP_STATUS_SWITCHING_PROTOCOLS		= 101,
	/** 200 OK */
	HTTP_STATUS_OK				= 200,
	/** 201 Created */
	HTTP_STATUS_CREATED			= 201,
	/** 202 Accepted */
	HTTP_STATUS_ACCEPTED			= 202,
	/** 203 Non-Authoritative Information */
	HTTP_STATUS_NON_AUTHORITATIVE_INFO	= 203,
	/** 204 No Content */
	HTTP_STATUS_NO_CONTENT			= 204,
	/** 205 Reset Content */
	HTTP_STATUS_RESET_CONTENT		= 205,
	/** 206 Partial Content */
	HTTP_STATUS_PARTIAL_CONTENT		= 206,
	/** 300 Multiple Choices */
	HTTP_STATUS_MULTIPLE_CHOICES		= 300,
	/** 301 Moved Permanently */
	HTTP_STATUS_MOVED_PERMANENTLY		= 301,
	/** 302 Found */
	HTTP_STATUS_FOUND			= 302,
	/** 303 See Other */
	HTTP_STATUS_SEE_OTHER			= 303,
	/** 304 Not Modified */
	HTTP_STATUS_NOT_MODIFIED		= 304,
	/** 305 Use Proxy (deprecated) */
	HTTP_STATUS_USE_PROXY			= 305,
	/** 307 Temporary Redirect */
	HTTP_STATUS_TEMPORARY_REDIRECT		= 307,
	/** 308 Permanent Redirect */
	HTTP_STATUS_PERMANENT_REDIRECT		= 308,
	/** 400 Bad Request */
	HTTP_STATUS_BAD_REQUEST			= 400,
	/** 401 Unauthorized */
	HTTP_STATUS_UNAUTHORIZED		= 401,
	/** 402 Payment Required */
	HTTP_STATUS_PAYMENT_REQUIRED		= 402,
	/** 403 Forbidden */
	HTTP_STATUS_FORBIDDEN			= 403,
	/** 404 Not Found */
	HTTP_STATUS_NOT_FOUND			= 404,
	/** 405 Method Not Allowed */
	HTTP_STATUS_METHOD_NOT_ALLOWED		= 405,
	/** 406 Not Acceptable */
	HTTP_STATUS_NOT_ACCEPTABLE		= 406,
	/** 407 Proxy Authentication Required */
	HTTP_STATUS_PROXY_AUTH_REQUIRED		= 407,
	/** 408 Request Timeout */
	HTTP_STATUS_REQUEST_TIMEOUT		= 408,
	/** 409 Conflict */
	HTTP_STATUS_CONFLICT			= 409,
	/** 410 Gone */
	HTTP_STATUS_GONE			= 410,
	/** 411 Length Required */
	HTTP_STATUS_LENGTH_REQUIRED		= 411,
	/** 412 Precondition Failed */
	HTTP_STATUS_PRECONDITION_FAILED		= 412,
	/** 413 Content Too Large */
	HTTP_STATUS_CONTENT_TOO_LARGE		= 413,
	/** 414 URI Too Long */
	HTTP_STATUS_URI_TOO_LONG		= 414,
	/** 415 Unsupported Media Type */
	HTTP_STATUS_UNSUPPORTED_MEDIA_TYPE	= 415,
	/** 416 Range Not Satisfiable */
	HTTP_STATUS_RANGE_NOT_SATISFIABLE	= 416,
	/** 417 Expectation Failed */
	HTTP_STATUS_EXPECTATION_FAILED		= 417,
	/** 418 I'm A Teapot */
	HTTP_STATUS_IM_A_TEAPOT			= 418,
	/** 421 Misdirected Request */
	HTTP_STATUS_MISDIRECTED_REQUEST		= 421,
	/** 422 Unprocessable Content */
	HTTP_STATUS_UNPROCESSABLE_CONTENT	= 422,
	/** 426 Upgrade Required */
	HTTP_STATUS_UPGRADE_REQUIRED		= 426,
	/** 431 Request Header Fields Too Large */
	HTTP_STATUS_REQ_HDR_FIELDS_TOO_LARGE	= 431,
	/** 500 Internal Server Error */
	HTTP_STATUS_INTERNAL_SERVER_ERROR	= 500,
	/** 501 Not Implemented */
	HTTP_STATUS_NOT_IMPLEMENTED		= 501,
	/** 502 Bad Gateway */
	HTTP_STATUS_BAD_GATEWAY			= 502,
	/** 503 Service Unavailable */
	HTTP_STATUS_SERVICE_UNAVAILABLE		= 503,
	/** 504 Gateway Timeout */
	HTTP_STATUS_GATEWAY_TIMEOUT		= 504,
	/** 505 HTTP Version Not Supported */
	HTTP_STATUS_HTTP_VERSION_NOT_SUPPORTED	= 505,
};

/**
 * @brief Default idle timeout
 * @ingroup server
 *
 * Default idle timeout in seconds that is set by
 * http_default_cfg(). By default, idle connections are closed when
 * this timeout elapses.
 *
 * To change the default, define `HTTP_DEFAULT_IDLE_TMO_S` before the
 * including `picow_http/http.h`, for example with a `#define` before the
 * `#include`, or with a compiler definition.
 *
 * @see http_default_cfg()
 */
#ifndef HTTP_DEFAULT_IDLE_TMO_S
#define HTTP_DEFAULT_IDLE_TMO_S (5)
#endif

/**
 * @brief Default send timeout
 * @ingroup server
 *
 * Default send timeout in seconds that is set by http_default_cfg().
 *
 * To change the default, define `HTTP_DEFAULT_SEND_TMO_S` before the
 * including `picow_http/http.h`, for example with a `#define` before the
 * `#include`, or with a compiler definition.
 *
 * @see http_default_cfg()
 */
#ifndef HTTP_DEFAULT_SEND_TMO_S
#define HTTP_DEFAULT_SEND_TMO_S (5)
#endif

/**
 * @brief Default listener backlog
 * @ingroup server
 *
 * Default maximum length of the listen queue for incoming connections,
 * set by http_default_cfg().
 *
 * To change the default, define `HTTP_DEFAULT_LISTEN_BACKLOG` before the
 * including `picow_http/http.h`, for example with a `#define` before the
 * `#include`, or with a compiler definition.
 *
 * @see http_default_cfg()
 */
#ifndef HTTP_DEFAULT_LISTEN_BACKLOG
#define HTTP_DEFAULT_LISTEN_BACKLOG (10)
#endif

/**
 * @brief Length of a literal string
 * @ingroup util
 *
 * Return the length of a C literal string -- a compile-time constant
 * string, such as `"foo"`. This is useful where a string length is
 * required and it would be redundant to call `strlen()` on every
 * invocation.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * #define SHORT_BODY "Very short response body"
 *
 * if (http_resp_send_body(http, SHORT_BODY, STRLEN_LTRL(SHORT_BODY), true)
 *     != ERR_OK) {
 *         // Error handling
 * }
 * @endcode
 *
 * @param[in] s MUST be a literal string
 * @return length of `s`
 */
#define STRLEN_LTRL(s) (sizeof(s) - 1)

/**
 * @struct req
 * @brief HTTP request
 *
 * `struct req` represents the current HTTP request, obtained from the
 * @ref http object in a custom handler via http_req().
 *
 * The member fields of `struct req` are opaque in the public API.
 * Functions for reading properties of the request include:
 *
 * * method: http_req_method()
 * * URL path: http_req_path()
 * * query string: http_req_query(), http_req_query_val(),
 *   http_req_query_iter()
 * * headers: http_req_hdr() (and its equivalents for @link http_req_hdr_str()
 *   nul-terminated strings @endlink and @link http_req_hdr_ltrl() string
 *   literals @endlink) and http_req_hdr_iter()
 *   * use the http_req_hdr_eq() and http_req_hdr_contains() families of
 *     functions to efficiently check the values of headers
 * * cookies: the http_req_cookie() family and http_req_cookie_iter()
 * * request body: http_req_body(), http_req_body_ptr(), http_req_chunk()
     and http_req_body_len()
 *
 * @ingroup req
 */
struct req;

/**
 * @struct resp
 * @brief HTTP response
 *
 * `struct resp` represents the current HTTP response, obtained from the
 * @ref http object in a custom handler via http_resp().
 *
 * The member fields of `struct resp` are opaque in the public
 * API. Functions for writing properties of the response include:
 *
 * * response status: http_resp_set_status()
 * * headers: http_resp_set_hdr() (and its equivalents for @link
 *   http_resp_set_hdr_str() nul-terminated strings @endlink and
 *   @link http_resp_set_hdr_ltrl() string literals @endlink)
 *   * http_resp_set_type() (and its equivalents for @link
 *     http_resp_set_type_str() nul-terminated strings @endlink and
 *     @link http_resp_set_type_ltrl() string literals @endlink) may
 *     be used to set the `Content-Type` header
 *   * http_resp_set_len() may be used to set the `Content-Length`
 *     header
 *
 * Functions to initiate sending the response to the client include:
 *
 * * http_resp_send_hdr() to send the response header
 * * http_resp_send_buf() to send the response body (and header, if
 *   not already sent)
 * * http_resp_send_chunk() to send the response body using [chunked transfer
 *   encoding](https://datatracker.ietf.org/doc/html/rfc9112#section-7.1)
 * * http_resp_err() to send the server's standard error response,
 *   for response status >= 400
 *
 * @ingroup resp
 */
struct resp;

/**
 * @struct server_cfg
 * @brief HTTP server configuration
 * @ingroup server
 *
 * Configuration of the HTTP server used at startup by http_srv_init().
 *
 * Struct members `ipaddr` and `ip_type`, which configure the IP address
 * at which the server listens, use data types from [lwIP for IP address
 * handling](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html). To
 * configure a listen address that differs from the default, use one of
 * the mechanisms defined in lwIP's `ip_addr.h`, `ip4_addr.h` and/or
 * `ip6_addr.h`
 *
 * For example, to listen only at a specific IPv4 address (perhaps because
 * a static IPv4 address has been configured at the router for the PicoW's
 * MAC address):
 *
 * @code{.c}
 * #include "picow_http/http.h"
 * #include "lwip/ip4_addr.h"
 *
 * struct server_cfg cfg;
 *
 * // Start with the default config, so that we only need to change
 * // individual fields.
 * cfg = http_default_cfg();
 *
 * // Listen at 192.0.2.47
 * IP4_ADDR(cfg.ipaddr, 192, 0, 2, 47);
 *
 * // Listener is IPv4 only.
 * cfg.ip_type = IPADDR_TYPE_V4;
 *
 * if (http_srv_init(&srv, &cfg) != ERR_OK) {
 *         // Error handling
 * }
 * @endcode
 *
 * A simple and common solution is for the server to listen at any IPv4 or
 * IPv6 address assigned to the PicoW, using `IP_ADDR_ANY` for the
 * `ipaddr` member and `IPADDR_TYPE_ANY` for `ip_type`. This is the
 * configuration set by http_default_cfg(), so it requires no further
 * modification by application code.
 *
 * @see http_srv_init(), http_default_cfg(), http_cfg(),
 *      [lwIP IP address handling](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html)
 */
struct server_cfg {
	/** Configuration of NTP time synchronization */
	struct ntp_cfg		ntp_cfg;
	/** IP address at which the server listens. */
	const ip_addr_t		*ipaddr;
	struct srv_data		*srv_data;
	/**
	 * Idle connection timeout in seconds. The timeout begins
	 * after the most recent response is sent on an established
	 * client connection. If no new request has been received on
	 * the connection when the timeout elapses, the connection is
	 * closed. Thus this timeout serves to clean up unused
	 * connections.
	 */
	unsigned		idle_tmo_s;
	/** Network send timeout in seconds. */
	unsigned		send_tmo_s;
	/** Port at which the server listens. */
	uint16_t		port;
	/** Maximum length of the listen queue for incoming connections. */
	uint8_t			listen_backlog;
	/** Listen at an IPv4 or IPv6 address, or both. */
	enum lwip_ip_addr_type	ip_type;
	/** true iff this is the configuration of a TLS server. */
	bool			tls;
};

/**
 * @struct server
 * @brief HTTP server.
 * @ingroup server
 *
 * `struct server` is used for starting and stopping a server.  Its member
 * fields are opaque in the public API. Functions for managing a server
 * include:
 *
 * * http_srv_init() and http_srv_fini() to start and stop a server.
 * * http_default_cfg() to get a default server configuration.
 * * http_cfg() to get a default configuration for a server named
 *   in the build-time configuration
 *   ([www.yaml](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration)).
 * * http_srv_ip() and http_srv_port() for properties of the listener
 *   address of a running server.
 * * http_srv_set_priv() and http_srv_priv() for @ref priv
 *   "server-scoped private data"
 *
 * @see http_srv_init(), http_srv_fini(), http_default_cfg(), http_cfg(),
 *      http_srv_ip(), http_srv_port(), http_srv_set_priv(), http_srv_priv(),
 *      @ref priv
 */
struct server;

/**
 * @brief Get the default confiuration for a server by name.
 * @ingroup server
 *
 * Get the default configuration for the server identified by `name` in
 * the build-time configuration
 * ([www.yaml](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration)).
 * The values of `cfg` are set as documented for http_default_cfg().
 *
 * http_default_cfg() can be used for the server named `"default"` (which
 * is the default value for the `server.name` field in `www.yaml`).
 * http_cfg() is usually only necessary when more than one server/listener
 * is configured.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * struct server_cfg cfg;
 * if (http_cfg("my_server", &cfg) != ERR_OK) {
 *         // Error handling
 * }
 * @endcode
 *
 * @param[in]  name server name configured in `www.yaml`<br>
 *                  `name` MUST be nul-terminated
 * @param[out] cfg  default configuration for the server
 * @return          `ERR_OK` on success<br>
 *                  `ERR_ARG` if either of `name` or `cfg` is `NULL`<br>
 *                  `ERR_VAL` if `name` is not configured in `www.yaml`
 *
 * @see http_default_cfg(), struct @ref server_cfg,
 *      [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 **/
err_t http_cfg(const char *name, struct server_cfg *cfg);

/**
 * @brief Get the default HTTP server configuration
 * @ingroup server
 *
 * Return an instance of struct @link server_cfg @endlink for the server
 * identified as `default` in the build-time configuration
 * ([www.yaml](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration)).
 * A server named `default` MUST be configured in `www.yaml` (note that
 * `default` is the default value of `server.name` in `www.yaml`). This
 * is a convenience for the case that only one server is configured.
 *
 * The fields of the returned configuration are set so that:
 *
 * * the @link ntp_default_cfg() default NTP configuration @endlink is set
 *   for the HTTP server
 * * the server listens at any IPv4 or IPv6 address for the PicoW
 * * for picow_http, the server listens at port 80; for picow_https
 *   at port 443
 * * for picow_http, the @ref server_cfg.tls field is set to `false`;
 *   for picow_https it is set to `true`
 * * the idle connection timeout is set to @ref HTTP_DEFAULT_IDLE_TMO_S
 * * the network send timeout is set to @ref HTTP_DEFAULT_SEND_TMO_S
 * * the maximum length of the listen queue is set to
 *   @ref HTTP_DEFAULT_LISTEN_BACKLOG
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * struct server_cfg cfg = http_default_cfg();
 * @endcode
 *
 * @return an HTTP configuration for server `default` set to default values
 *
 * @see http_cfg(), struct @ref server_cfg,
 */
static inline struct server_cfg
http_default_cfg(void)
{
	err_t err;
	struct server_cfg cfg;

	err = http_cfg("default", &cfg);
	PICOW_HTTP_ASSERT(err == ERR_OK);
	return cfg;
}

/**
 * @brief Start an HTTP server.
 * @ingroup server
 *
 * Start an HTTP server as configured by `cfg`. The server binds to the
 * address in `cfg->ipaddr` and `cfg->port`, and listens for HTTP
 * requests.
 *
 * @link ntp NTP synchronization @endlink as configured by `cfg->ntp_cfg`
 * is initiated, unless it has already begun due to an invocation of
 * http_srv_init() for another server. If so, the configuration in
 * `cfg->ntp_cfg` is ignored.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * struct server *srv;
 * struct server_cfg cfg;
 *
 * // Using the default configuration.
 * cfg = http_default_cfg();
 * if (http_srv_init(&srv, &cfg) != ERR_OK) {
 *         // Error handling
 * }
 * @endcode
 *
 * @param[out] server set to a pointer to the server on success
 * @param[in]  cfg pointer to the server configuration
 * @return     `ERR_OK` on success<br>
 *	       `ERR_VAL` if either of `cfg` is `NULL`; if
 *			 `cfg.ntp_cfg->servers` is `NULL` and NTP
 *			 synchronization was not already initialized;
 *                       or if either of `cfg.idle_tmo_s` or
 *                       `cfg.idle_tmo_s` is out of range
 *                       (greater then `INT64_MAX` microseconds)<br>
 *             `ERR_MEM` if there is insufficient memory to allocate
 *                       `*server` or the underlying TCP structures<br>
 *             or any error returned by the TCP stack for the bind or
 *                       listen operations
 * @see [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_srv_init(struct server **server, struct server_cfg *cfg);

/**
 * @brief Stop an HTTP server
 * @ingroup server
 *
 * Stop the HTTP server represented by `*server`. `*server` MUST be a
 * server started previously by http_srv_init().
 *
 * On success, the server listener is closed. If no other server is
 * running (i.e. http_srv_fini() has been called for all other servers
 * previously started), then @link ntp NTP time synchronization @endlink
 * is stopped.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * struct server *srv;
 * struct server_cfg cfg;
 *
 * if (http_srv_init(&srv, &cfg) != ERR_OK) {
 *         // Error handling
 * }
 *
 * // ...
 *
 * if (http_srv_fini(srv) != ERR_OK) {
 *         // Error handling
 * }
 * @endcode
 *
 * @param[in]  server pointer to a server previously started by http_srv_init().
 * @return     `ERR_OK` on success<br>
 *             `ERR_VAL` if `server` is `NULL`<br>
 *             or any error returned by the TCP stack for the close
 *             operation<br>
 *             if `ERR_MEM` is returned, the caller should retry http_srv_fini()
 *             at a later time, since the TCP stack may be able to free memory
 * @see [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_srv_fini(struct server *server);

/**
 * @brief Return the server listener IP address
 * @ingroup server
 *
 * Return the IP address at which the `server` is currently listening.
 * `server` MUST represent a running server when the function is called;
 * that is, http_srv_init() has been called successfully for the server,
 * and http_srv_fini() has not been called.
 *
 * The return value has an [lwIP
 * type](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html#ga16ef96d6cde029029bbf47fee35fd67a)
 * that represents IP addresses, and may be handled using the [lwIP API for
 * address handling](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html).
 * The return value MAY NOT be modified.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 * #include "lwip/ip_addr.h"
 *
 * struct server *srv;
 * struct server_cfg cfg;
 * ip_addr_t *ip;
 *
 * // ...
 * if (http_srv_init(&srv, &cfg) != ERR_OK) {
 *         // Error handling ...
 * }
 * else {
 *         ip = http_srv_ip(srv);
 *         HTTP_LOG_INFO("Server listening at: %s", ipaddr_ntoa(ip));
 *         switch (ip->type) {
 *         case IPADDR_TYPE_V4:
 *                 HTTP_LOG_INFO("IPv4 address");
 *                 break;
 *         case IPADDR_TYPE_V6:
 *                 HTTP_LOG_INFO("IPv6 address");
 *                 break;
 *         case IPADDR_TYPE_ANY:
 *                 HTTP_LOG_INFO("'dual-stack' IPv4+IPv6 address");
 *                 break;
 *         }
 * }
 * @endcode
 *
 * @param[in] server a running HTTP server
 * @return           the server's listener IP address
 * @see              http_srv_port(), [lwIP
 *                   ip_addr_t](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html#ga16ef96d6cde029029bbf47fee35fd67a),
 *                   [lwIP address
 *                   handling](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html)
 */
ip_addr_t * http_srv_ip(struct server *server);

/**
 * @brief Return the server listener port number
 * @ingroup server
 *
 * Return the port number at which `server` is currently listening. As with
 * http_srv_ip(), `server` MUST represent a running server when the function
 * is called.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 * #include "lwip/ip_addr.h"
 *
 * struct server *srv;
 * struct server_cfg cfg;
 * ip_addr_t *ip;
 *
 * // ...
 * if (http_srv_init(&srv, &cfg) != ERR_OK) {
 *         // Error handling ...
 * }
 * else {
 *         ip = http_srv_ip(srv);
 *         HTTP_LOG_INFO("Server listening at: %s:%u", ipaddr_ntoa(ip),
 *                       http_srv_port(srv));
 * }
 * @endcode
 *
 * @param[in] server a running HTTP server
 * @return           the server's listener port number
 * @see              http_srv_ip()
 */
uint16_t http_srv_port(struct server *server);

/**
 * @brief Set a server-wide private object
 * @ingroup server
 *
 * Sets a private object for `server`, which can be retrieved with
 * http_srv_priv(). `priv` can be a pointer to an object of any type. This
 * makes it possible for handlers to access server-wide data.
 *
 * `fini` is an optional finalizer function for `priv`. If `fini` is not
 * `NULL`, it is invoked with `priv` as its parameter when http_srv_fini()
 * is invoked.
 *
 * See the discussion in @ref priv.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // A struct for server-wide application data
 * static struct app_data my_app_data = {
 *	.foo = 47,
 *	.bar = 11,
 * };
 *
 * struct server *srv;
 *
 * // ...
 * http_srv_set_priv(srv, &my_app_data, NULL);
 * // The pointer to my_app_data can now be retrieved in every handler
 * // with http_srv_priv().
 * @endcode
 *
 * @param[in] server a running HTTP server
 * @param[in] priv   pointer to an object of any type
 * @param[in] fini   optional finalizer function for `priv`<br>
 *                   set to `NULL` if no finalizer is needed
 * @see              http_srv_priv(), @ref priv
 */
void http_srv_set_priv(struct server *server, void *priv, priv_fini_f *fini);

/**
 * @brief Return the local IP address of the current HTTP connection
 * @ingroup server
 *
 * Return the local IP address of the current HTTP connection. The return
 * value has an [lwIP
 * type](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html#ga16ef96d6cde029029bbf47fee35fd67a)
 * that represents IP addresses, and may be handled using the [lwIP API for
 * address handling](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html).
 * The return value MAY NOT be modified.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 * #include "lwip/ip_addr.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *         ip_addr_t *ip;
 *
 *         // ...
 *         ip = http_cx_local_ip(http);
 *         HTTP_LOG_INFO("Local connection: %s:%u", ipaddr_ntoa(ip),
 *                       http_cx_local_port(http));
 *         // ...
 * }
 * @endcode
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @return         the HTTP connection's local IP address
 * @see            http_cx_local_port(), http_cx_remote_ip(),
 *                 http_cx_remote_port(), [lwIP
 *                 ip_addr_t](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html#ga16ef96d6cde029029bbf47fee35fd67a),
 *                 [lwIP address
 *                 handling](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html)
 */
static inline ip_addr_t *
http_cx_local_ip(struct http *http)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	AN(http->pcb);
	return http_local_ip(http->pcb);
}

/**
 * @brief Return the local port of the current HTTP connection
 * @ingroup server
 *
 * Return the local port number of the current HTTP connection.
 *
 * See the example for http_cx_local_ip().
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @return         the HTTP connection's local port number
 * @see            http_cx_local_ip(), http_cx_remote_ip(),
 *                 http_cx_remote_port()
 */
static inline uint16_t
http_cx_local_port(struct http *http)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	AN(http->pcb);
	return http_local_port(http->pcb);
}

/**
 * @brief Return the remote IP address of the current HTTP connection
 * @ingroup server
 *
 * Return the remote IP address of the current HTTP connection. The return
 * value has an [lwIP
 * type](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html#ga16ef96d6cde029029bbf47fee35fd67a)
 * that represents IP addresses, and may be handled using the [lwIP API for
 * address handling](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html).
 * The return value MAY NOT be modified.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 * #include "lwip/ip_addr.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *         ip_addr_t *ip;
 *
 *         // ...
 *         ip = http_cx_remote_ip(http);
 *         HTTP_LOG_INFO("Remote connection: %s:%u", ipaddr_ntoa(ip),
 *                       http_cx_remote_port(http));
 *         // ...
 * }
 * @endcode
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @return         the HTTP connection's remote IP address
 * @see            http_cx_remote_port(), http_cx_local_ip(),
 *                 http_cx_local_port(), [lwIP
 *                 ip_addr_t](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html#ga16ef96d6cde029029bbf47fee35fd67a),
 *                 [lwIP address
 *                 handling](https://www.nongnu.org/lwip/2_1_x/group__ipaddr.html)
 */
static inline ip_addr_t *
http_cx_remote_ip(struct http *http)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	AN(http->pcb);
	return http_remote_ip(http->pcb);
}

/**
 * @brief Return the remote port of the current HTTP connection
 * @ingroup server
 *
 * Return the remote port number of the current HTTP connection.
 *
 * See the example for http_cx_remote_ip().
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @return         the HTTP connection's remote port number
 * @see            http_cx_remote_ip(), http_cx_local_ip(), http_cx_local_port()
 */
static inline uint16_t
http_cx_remote_port(struct http *http)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	AN(http->pcb);
	return http_remote_port(http->pcb);
}

/**
 * @brief Return the server-wide private object, if any
 * @ingroup server
 *
 * Return the private object previously set by http_srv_set_priv(), if
 * any.  If http_srv_set_priv() was never called, http_srv_priv() returns
 * `NULL`.
 *
 * See the discussion in @ref priv.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *	struct app_data *srv_data;
 *
 *	srv_data = (struct app_data *)http_srv_priv(http);
 *	if (srv_data == NULL) {
 *		HTTP_LOG_WARN("Server-wide data not set");
 *		// ...
 *	}
 *
 *	// ...
 * }
 * @endcode
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @return         pointer to an object set in http_srv_set_priv()<br>
 *                 `NULL` if http_srv_set_priv() was never called.
 * @see            http_srv_set_priv(), @ref priv
 */
static inline void *
http_srv_priv(struct http *http)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(http->srv_data, SRV_DATA_MAGIC);
	return http->srv_data->srv_priv;
}

/**
 * @brief Get the current request object
 * @ingroup req
 *
 * Get the object representing the current request on an HTTP connection.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *         struct req *req;
 *
 *         req = http_req(http);
 *
 *         // ...
 * }
 * @endcode
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @return pointer to the current request object
 */
static inline struct req *
http_req(struct http *http)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->req, REQ_MAGIC);
	return &http->req;
}

/**
 * @brief Return the value of a request header
 * @ingroup req
 *
 * Return a pointer to the value of the first request header field
 * identified by `name`, or `NULL` if there is no such header. `name` does
 * not necessarily have to be nul-terminated. The header name match
 * is case-insensitive.
 *
 * After successful return, `*val_len` is set to the length of the header
 * value, or 0 if the header was not found.
 *
 * @attention The header value is @em not nul-terminated, and MAY NOT be
 *            modified.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        const char *type;
 *        size_t type_len;
 *        struct req *req = http_req(http);
 *
 *        type = http_req_hdr(req, "Content-Type", 12, &type_len);
 *        if (type != NULL) {
 *                HTTP_LOG_INFO("Content-Type is %.*s", type_len, type);
 *                // ...
 *        }
 *        else {
 *                HTTP_LOG_INFO("No Content-Type request header");
 *        }
 * }
 * @endcode
 *
 * @param[in]  req      req object for the current request<br>
 *                      MUST be a valid req pointer returned from http_req()
 * @param[in]  name     request header name
 * @param[in]  name_len length of `name`
 * @param[out] val_len  on successful return, `*val_len` is the length of
 *                      the header value<br>
 *                      0 if the header is not found, or if `name` is `NULL`
 * @return              pointer to the header value, or `NULL` if there is no
 *                      such header<br>
 *                      `NULL` if either of `name` or `val_len` is `NULL`
 */
const char * http_req_hdr(struct req *req, const char *name, size_t name_len,
			  size_t *val_len);

/**
 * @brief Return the value of a request header with a nul-terminated name
 * @ingroup req
 *
 * Return a pointer to the value of the first request header field
 * identified by `name`, or `NULL` if there is no such header. `name` MUST
 * be nul-terminated. Otherwise identical to http_req_hdr().
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        const char *ua_name = "User-Agent", const char *ua_val;
 *        size_t ua_len;
 *        struct req *req = http_req(http);
 *
 *        ua_val = http_req_hdr_str(req, "User-Agent", &ua_len);
 *        if (ua_val != NULL) {
 *                HTTP_LOG_INFO("User-Agent is %.*s", ua_len, ua_val);
 *                // ...
 *        }
 *        else {
 *                HTTP_LOG_INFO("No User-Agent request header");
 *        }
 * }
 * @endcode
 *
 * @param[in]  req      req object for the current request<br>
 *                      MUST be a valid req pointer returned from http_req()
 * @param[in]  name     request header name<br>
 *                      MAY NOT be `NULL`, and MUST be nul-terminated
 * @param[out] val_len  on successful return, `*val_len` is the length of
 *                      the header value<br>
 *                      0 if the header is not found
 * @return              pointer to the header value, or `NULL` if there is no
 *                      such header<br>
 *                      `NULL` if `val_len` is `NULL`
 */
static inline const char *
http_req_hdr_str(struct req *req, const char *name, size_t *val_len)
{
	return http_req_hdr(req, name, strlen(name), val_len);
}

/**
 * @brief Return the value of a request header with a literal name
 * @ingroup req
 *
 * Return a pointer to the value of the first request header field
 * identified by `name`, or `NULL` if there is no such header. `name` MUST
 * be a literal string (a compile-time constant string). Otherwise
 * identical to http_req_hdr().
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        const char *host;
 *        size_t host_len;
 *        struct req *req = http_req(http);
 *
 *        host = http_req_hdr_ltrl(req, "Host", &host_len);
 *        if (host != NULL) {
 *                HTTP_LOG_INFO("Host is %.*s", host_len, host);
 *                // ...
 *        }
 *        else {
 *                HTTP_LOG_INFO("No Host request header");
 *        }
 * }
 * @endcode
 *
 * @param[in]  req      req object for the current request<br>
 *                      MUST be a valid req pointer returned from http_req()
 * @param[in]  name     request header name<br>
 *                      MUST be a literal string
 * @param[out] val_len  on successful return, `*val_len` is the length of
 *                      the header value<br>
 *                      0 if the header is not found
 * @return              pointer to the header value, or `NULL` if there is no
 *                      such header<br>
 *                      `NULL` if `val_len` is `NULL`
 */
#define http_req_hdr_ltrl(req, name, val_len)			\
	http_req_hdr((req), (name), STRLEN_LTRL(name), (val_len))

/**
 * @brief Return true if a request header value is equal to a string
 * @ingroup req
 *
 * Return true if the value of any request header field identified by
 * `name` is equal to `val`. `name` and `val` do not necessarily have to
 * be nul-terminated. Request header name matches are case insensitive.
 * But header values are matched exactly (i.e. case sensitive).
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *
 *        // Matches request header "Foo" or "foo", etc.
 *        if (http_req_hdr_eq(req, "Foo", 3, "bar", 3)) {
 *                HTTP_LOG_INFO("Foo is bar");
 *                // ...
 *        }
 * }
 * @endcode
 *
 * @param[in] req      req object for the current request<br>
 *                     MUST be a valid req pointer returned from http_req()
 * @param[in] name     request header name, matched case insensitively
 * @param[in] name_len length of `name`
 * @param[in] val      value to match, matched exactly
 * @param[in] val_len  length of `val`
 * @return             true if any request header matching `name` has a value
 *                     identical to `val`<br>
 *                     false if no header matching `name` has a value equal
 *                     to `val`, or if there is no such header, or if either
 *                     of `name` or `val` is `NULL`
 */
bool http_req_hdr_eq(struct req *req, const char *name, size_t name_len,
		     const char *val, size_t val_len);

/**
 * @brief Return true if a request header contains a string
 * @ingroup req
 *
 * Return true if the value of any request header identified by `name` is
 * equal to or contains `substring`. `name` and `substring` do not
 * necessarily have to be nul-terminated. Request header name matches are
 * case insensitive, but the values must contain the exact substring.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *
 *        if (http_req_hdr_contains(req, "User-Agent", 10, "Mozilla", 7)) {
 *                HTTP_LOG_INFO("User-Agent contains Mozilla");
 *                // ...
 *        }
 * }
 * @endcode
 *
 * @param[in] req           req object for the current request<br>
 *                          MUST be a valid req pointer returned from http_req()
 * @param[in] name          request header name, matched case insensitively
 * @param[in] name_len      length of `name`
 * @param[in] substring     substring to search for in header values
 * @param[in] substring_len length of `substring`
 * @return                  true if any request header matching `name` has a
 *                          value that is identical to or contains
 *                          `substring`<br>
 *                          false if no header matching `name` has a value that
 *                          contains `substring`, or no header matches `name`,
 *                          or if either of `name` or `substring` is `NULL`
 */
bool http_req_hdr_contains(struct req *req, const char *name, size_t name_len,
			   const char *substring, size_t substring_len);

/**
 * @brief Return true if a request header value is equal to a nul-terminated
 *        string
 * @ingroup req
 *
 * Return true if the value of any request header field identified by
 * `name` is equal to `val`. `name` and `val` MUST be nul-terminated.
 * Otherwise, the same conditions apply as for http_req_hdr_eq().
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        const char *hdr = "Baz", *val = "quux";
 *
 *        if (http_req_hdr_eq_str(http, hdr, val)) {
 *                HTTP_LOG_INFO("Baz is quux");
 *                // ...
 *        }
 * }
 * @endcode
 *
 * @param[in] req  req object for the current request<br>
 *                 MUST be a valid req pointer returned from http_req()
 * @param[in] name request header name, matched case insensitively<br>
 *                 MAY NOT be `NULL`, and MUST be nul-terminated
 * @param[in] val  value to match, matched exactly<br>
 *                 MAY NOT be `NULL`, and MUST be nul-terminated
 * @return         true or false under the same conditions as
 *                 http_req_hdr_eq()
 */
static inline bool
http_req_hdr_eq_str(struct req *req, const char *name, const char *val)
{
	return http_req_hdr_eq(req, name, strlen(name), val, strlen(val));
}

/**
 * @brief Return true if a request header contains a nul-terminated string
 * @ingroup req
 *
 * Return true if the value of any request header identified by `name` is
 * equal to or contains `substring`. `name` and `substring` MUST be
 * nul-terminated. Otherwise, the same conditions apply as for
 * http_req_hdr_contains().
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *        const char *hdr = "Content-Language", *len = "en";
 *
 *        // ...
 *        if (http_req_hdr_contains_str(req, hdr, len)) {
 *                HTTP_LOG_INFO("Language is English");
 *                // ...
 *        }
 * }
 * @endcode
 *
 * @param[in] req       req object for the current request<br>
 *                      MUST be a valid req pointer returned from http_req()
 * @param[in] name      request header name, matched case insensitively<br>
 *                      MAY NOT be `NULL`, and MUST be nul-terminated
 * @param[in] substring substring to search for in header values<br>
 *                      MAY NOT be `NULL`, and MUST be nul-terminated
 * @return              true or false under the same conditions as
 *                      http_req_hdr_contains()
 * @see                 http_req_hdr_contains()
 */
static inline bool
http_req_hdr_contains_str(struct req *req, const char *name,
			  const char *substring)
{
	return http_req_hdr_contains(req, name, strlen(name), substring,
				     strlen(substring));
}

/**
 * @brief Return true if a request header value is equal to a literal
 *        string
 * @ingroup req
 *
 * Return true if the value of any request header field identified by
 * `name` is equal to `val`. `name` and `val` MUST be literal strings
 * (compile-time constant strings). Otherwise, the same conditions apply
 * as for http_req_hdr_eq().
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        if (http_req_hdr_eq_ltrl(http, "Baz", "quux")) {
 *                HTTP_LOG_INFO("Baz is quux");
 *                // ...
 *        }
 * }
 * @endcode
 *
 * @param[in] req  req object for the current request<br>
 *                 MUST be a valid req pointer returned from http_req()
 * @param[in] name request header name, matched case insensitively<br>
 *                 MUST be a literal string
 * @param[in] val  value to match, matched exactly<br>
 *                 MUST be a literal string
 * @return         true or false under the same conditions as
 *                 http_req_hdr_eq()
 */
#define http_req_hdr_eq_ltrl(req, name, val)				\
	http_req_hdr_eq((req), (name), STRLEN_LTRL(name), (val),	\
			STRLEN_LTRL(val))

/**
 * @brief Return true if a request header contains a literal string
 * @ingroup req
 *
 * Return true if the value of any request header identified by `name` is
 * equal to or contains `substring`. `name` and `substring` MUST be
 * literal strings (compile-time constant strings). Otherwise, the same
 * conditions apply as for http_req_hdr_contains().
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *
 *        // ...
 *        if (http_req_hdr_contains_ltrl(req, "Content-Language", "en")) {
 *                HTTP_LOG_INFO("Language is English");
 *                // ...
 *        }
 * }
 * @endcode
 *
 * @param[in] req       req object for the current request<br>
 *                      MUST be a valid req pointer returned from http_req()
 * @param[in] name      request header name, matched case insensitively<br>
 *                      MUST be a literal string
 * @param[in] substring substring to search for in header values<br>
 *                      MUST be a literal string
 * @return              true or false under the same conditions as
 *                      http_req_hdr_contains()
 * @see                 http_req_hdr_contains()
 */
#define http_req_hdr_contains_ltrl(req, name, substring)		\
	http_req_hdr_contains((req), (name), STRLEN_LTRL(name),	(substring), \
			      STRLEN_LTRL(substring))

/**
 * @brief Function type for name/value iterators
 * @ingroup req
 *
 * Function typedef for callbacks used in name/value iterators, such as:
 *
 * * http_req_hdr_iter() for request header names and values
 * * http_req_query_iter() for query string parameters and their values
 * * http_req_cookie_iter() for Cookie names and values
 *
 * The function is called for every name/value pair.
 *
 * The callback must return
 * [`ERR_OK`](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 * to indicate success, or any other value to indicate failure. The calling
 * iterator returns `ERR_OK` if `ERR_OK` was returned for every name/value
 * pair; on any other return value, the iterator stops and returns the
 * value returned by the callback.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // A function that satisfies the typedef
 * err_t
 * my_name_val_cb(const char *name, size_t name_len, const char *val,
 *                size_t val_len, void *priv)
 * {
 *        HTTP_LOG_INFO("%.*s: %.*s", name_len, name, val_len, val);
 *        // ...
 *        return ERR_OK;
 * }
 * @endcode
 *
 * @param[in] name     name (such as a request header name)<br>
 * @param[in] name_len length of `name`<br>
 * @param[in] val      value assigned to `name`<br>
 * @param[in] val_len  length of `val`<br>
 *                     may be 0
 * @param[in] priv     pointer to any object, or `NULL`<br>
 *                     passed in as the `priv` argument to the iterator
 * @return             `ERR_OK` on success, any other error value on failure
 * @see                http_req_hdr_iter(), http_req_query_iter(),
 *                     [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
typedef err_t (*name_val_iter_f)(const char *name, size_t name_len,
				 const char *val, size_t val_len, void *priv);

/**
 * @brief Iterate over request headers
 * @ingroup req
 *
 * Iterate over the headers in the current request. The `iter_cb` callback
 * is invoked for every request header name/value pair.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // A function that satisfies typedef name_val_iter_f
 * err_t
 * my_name_val_cb(const char *name, size_t name_len, const char *val,
 *                size_t val_len, void *priv)
 * {
 *        // ...
 *        return ERR_OK;
 * }
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *
 *        // ...
 *        if (http_req_hdr_iter(req, my_name_val_cb, NULL) != ERR_OK) {
 *                // Error handling ...
 *        }
 *        // ...
 * }
 * @endcode
 *
 * @param[in] req     req object for the current request<br>
 *                    MUST be a valid req pointer returned from http_req()
 * @param[in] iter_cb @link name_val_iter_f iterator callback function
 *                    @endlink<br>
 *                    invoked for every request header name/value
 * @param[in] priv    pointer to any object, or `NULL`<br>
 *                    passed in as the `priv` argument to the callback
 * @return            `ERR_OK` if each invocation of the callback returned
 *                    `ERR_OK`<br>
 *                    otherwise, the non-`ERR_OK` value returned from the
 *                    callback
 * @see               name_val_iter_f,
 *                    [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_req_hdr_iter(struct req *req, name_val_iter_f iter_cb, void *priv);

/**
 * @brief Return the value of a cookie
 * @ingroup req
 *
 * Return the value of the cookie identified by `name` in the current
 * request header, if the `Cookie` header is present and includes `name`.
 * `name` does not necessarily have to be nul-terminated. Return `NULL` if
 * there is no `Cookie` header, or if the `Cookie` header does not contain
 * `name`.
 *
 * On return, `*val_len` is the length of the value. If the cookie value
 * is double-quoted, the return value points to the double-quoted string
 * (it is not "unquoted").
 *
 * `name` may be present in the `Cookie` header without a value, such as
 * the cookie `foo` in `foo=; bar=baz`. In that case, the return value is
 * the static constant empty string `""` (hence the character pointed to
 * by the return value is `\0`), and `*val_len` is 0. So the possibilities
 * are:
 *
 * * return value is `NULL`
 *   * `name` is not in the `Cookie` header, or there is no `Cookie` header.
 * * return value is non-`NULL` and `*val_len` > 0:
 *   * the return value points to the non-empty value of the cookie `name`.
 * * return value is non-`NULL` and `*val_len` == 0:
 *   * `name` is in the `Cookie` header, but its value is empty.
 *
 * @attention The special case of the empty string, which starts with
 * the nul character `\0`, for empty cookie values, is an exception.
 * Otherwise, the returned string (when not `NULL`) is @em not
 * nul-terminated, and MAY NOT be modified.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *        const unsigned char *val;
 *        size_t val_len;
 *
 *        // ...
 *        val = http_req_cookie(req, "foo", 3, &val_len);
 *        if (val == NULL)
 *                HTTP_LOG_INFO("Cookie foo not found");
 *        else if (val_len > 0)
 *                HTTP_LOG_INFO("Cookie foo: %.*s", val_len, val);
 *        else
 *                HTTP_LOG_INFO("Cookie foo found, but empty");
 *        // ...
 * }
 * @endcode
 *
 * @param[in]  req      req object for the current request<br>
 *                      MUST be a valid req pointer returned from http_req()
 * @param[in]  name     cookie name
 * @param[in]  name_len length of `name`
 * @param[out] val_len  length of the returned value, if any
 * @return              value of the cookie `name`, if found<br>
 *                      `NULL` if `name` was not found, or if there is no
 *                      `Cookie` header<br>
 *                      `NULL` if either of `name` or `val_len` is `NULL`<br>
 *                      `""` (the static constant empty string) if `name`
 *                      is in the `Cookie` header, but its value is empty
 * @see                 http_req_cookie_str(), http_req_cookie_ltrl(),
 *                      http_req_cookie_iter()
 */
const uint8_t * http_req_cookie(struct req *req, const char *name,
				size_t name_len, size_t *val_len);

/**
 * @brief Return the value of a cookie with a nul-terminated name
 * @ingroup req
 *
 * Return the value of the cookie `name` in the current request, if the
 * `Cookie` header is present and includes `name`.  Return `NULL` if there
 * is no `Cookie` header, or if the `Cookie` header does not contain
 * `name`.
 *
 * `name` MUST be nul-terminated. Otherwise, the conditions are the same
 * as for http_req_cookie().
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *        const char *name = "foo";
 *        const unsigned char *val;
 *        size_t val_len;
 *
 *        // ...
 *        val = http_req_cookie_str(req, name, &val_len);
 *        if (val == NULL)
 *                HTTP_LOG_INFO("Cookie %s not found", name);
 *        else if (val_len > 0)
 *                HTTP_LOG_INFO("Cookie %s: %.*s", name, val_len, val);
 *        else
 *                HTTP_LOG_INFO("Cookie %s found, but empty", name);
 *        // ...
 * }
 * @endcode
 *
 * @param[in]  req      req object for the current request<br>
 *                      MUST be a valid req pointer returned from http_req()
 * @param[in]  name     cookie name<br>
 *                      MUST be nul-terminated
 * @param[out] val_len  length of the returned value, if any
 * @return              value of the cookie `name`, if found<br>
 *                      `NULL` if `name` was not found, or if there is no
 *                      `Cookie` header<br>
 *                      `NULL` if `val_len` is `NULL`<br>
 *                      `""` (the static constant empty string) if `name`
 *                      is in the `Cookie` header, but its value is empty
 * @see                 http_req_cookie(), http_req_cookie_ltrl(),
 *                      http_req_cookie_iter()
 */
static inline const uint8_t *
http_req_cookie_str(struct req *req, const char *name, size_t *val_len)
{
	return http_req_cookie(req, name, strlen(name), val_len);
}

/**
 * @brief Return the value of a cookie whose name is a string literal
 * @ingroup req
 *
 * Return the value of the cookie `name` in the current request, if the
 * `Cookie` header is present and includes `name`.  Return `NULL` if there
 * is no `Cookie` header, or if the `Cookie` header does not contain
 * `name`.
 *
 * `name` MUST be a string literal (a compile-time constant string).
 * Otherwise, the conditions are the same as for http_req_cookie().
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *        const unsigned char *val;
 *        size_t val_len;
 *
 *        // ...
 *        val = http_req_cookie_ltrl(req, "foo", &val_len);
 *        if (val == NULL)
 *                HTTP_LOG_INFO("Cookie foo not found");
 *        else if (val_len > 0)
 *                HTTP_LOG_INFO("Cookie foo: %.*s", val_len, val);
 *        else
 *                HTTP_LOG_INFO("Cookie foo found, but empty");
 *        // ...
 * }
 * @endcode
 *
 * @param[in]  req      req object for the current request<br>
 *                      MUST be a valid req pointer returned from http_req()
 * @param[in]  name     cookie name<br>
 *                      MUST be a string literal
 * @param[out] val_len  length of the returned value, if any
 * @return              value of the cookie `name`, if found<br>
 *                      `NULL` if `name` was not found, or if there is no
 *                      `Cookie` header<br>
 *                      `NULL` if `val_len` is `NULL`<br>
 *                      `""` (the static constant empty string) if `name`
 *                      is in the `Cookie` header, but its value is empty
 * @see                 http_req_cookie(), http_req_cookie_str(),
 *                      http_req_cookie_iter()
 */
#define http_req_cookie_ltrl(req, name, val_len) \
	http_req_cookie((req), (name), STRLEN_LTRL(name), (val_len))

/**
 * @brief Iterate over cooke name/value pairs
 * @ingroup req
 *
 * Iterate over the cookies in the `Cookie` request header, if any.  The
 * `iter_cb` callback is invoked for each cookie name/value pair.
 *
 * As with `http_req_cookie()`: if there is a cookie with only a name and
 * no value, the static empty constant string `""` is passed to the
 * iterator in the `value` parameter, and `val_len` is 0. See
 * http_req_cookie() for details.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // A function that satisfies typedef name_val_iter_f
 * err_t
 * cookie_cb(const char *name, size_t name_len, const char *val,
 *           size_t val_len, void *priv)
 * {
 *        // ...
 *        if (val_len != NULL)
 *                HTTP_LOG_INFO("Cookie %.*s: %.*s", name_len, name,
 *                              val_len, val);
 *        else
 *                HTTP_LOG_INFO("Cookie %.*s found, but empty",
 *                              name_len, name);
 *        // ...
 *        return ERR_OK;
 * }
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *
 *        // ...
 *        if (http_req_cookie_iter(req, cookie_cb, NULL) != ERR_OK) {
 *                // Error handling ...
 *        }
 *        // ...
 * }
 * @endcode
 *
 * @param[in] req     req object for the current request<br>
 *                    MUST be a valid req pointer returned from http_req()
 * @param[in] iter_cb @link name_val_iter_f iterator callback function
 *                    @endlink<br>
 *                    invoked for every request header name/value
 * @param[in] priv    pointer to any object, or `NULL`<br>
 *                    passed in as the `priv` argument to the callback
 * @return            `ERR_OK` if each invocation of the callback returned
 *                    `ERR_OK`<br>
 *                    otherwise, the non-`ERR_OK` value returned from the
 *                    callback
 * @see               name_val_iter_f, http_req_cookie(), http_req_cookie_str(),
 *                    http_req_cookie_ltrl(),
 *                    [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_req_cookie_iter(struct req *req, name_val_iter_f iter_cb,
			   void *priv);

/**
 * @brief Return the request method
 * @ingroup req
 *
 * Return the request method.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *        enum http_method_t method;
 *
 *        // ...
 *
 *        method = http_req_method(req);
 *        if (method == HTTP_METHOD_HEAD)) {
 *                // Return the response header only ...
 *        }
 *        else {
 *                PICOW_HTTP_ASSERT(method == HTTP_METHOD_GET);
 *                // Return the response header and body ...
 *        }
 * }
 * @endcode
 *
 * @param[in] req req object for the current request<br>
 *                MUST be a valid req pointer returned from http_req()
 * @return        enum value representing the method
 * @see           http_method_t
 */
static inline enum http_method_t
http_req_method(struct req *req)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	return req->method;
}

/**
 * @brief Return the path from the first request line
 * @ingroup req
 *
 * Return the path sent in the first line of the request, not including
 * any query string. For example, if the first line was `GET
 * /foo/bar?baz=quux HTTP/1.1`, this function returns a pointer to
 * `/foo/bar`. `*len` is set to the length of the path.
 *
 * @attention The returned string is @em not nul-terminated.
 *
 * Example:
 *
 * @code{.c}
 * #include <stdint.h>
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *        const uint8_t *path;
 *        size_t path_len;
 *
 *        // ...
 *
 *        path = http_req_path(req, &path_len);
 *        HTTP_LOG_INFO("Request path %.*s", path_len, path);
 *
 *        // ...
 *
 * }
 * @endcode
 *
 * @param[in] req  req object for the current request<br>
 *                 MUST be a valid req pointer returned from http_req()
 * @param[out] len on return, `*len` is the length of the path<br>
 *                 MAY NOT be `NULL`
 * @return         the request path, not including any query string
 * @see            http_req_query()
 */
static inline const uint8_t *
http_req_path(struct req *req, size_t *len)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	AN(len);

	*len = req->path.len;
	return req->p->payload + req->path.off;
}

/**
 * @brief Return the request query string
 * @ingroup req
 *
 * Return the query string sent in the request, or `NULL` if none was
 * sent. `*len` is set to the length of the query string, or 0 if there
 * was no query string.
 *
 * @attention The returned string is @em not nul-terminated.
 *
 * Example:
 *
 * @code{.c}
 * #include <stdint.h>
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *        const uint8_t *query;
 *        size_t query_len;
 *
 *        // ...
 *
 *        query = http_req_query(req, &query_len);
 *        if (query != NULL)
 *                HTTP_LOG_INFO("Query string: %.*s", query_len, query);
 *        else
 *                HTTP_LOG_INFO("No query string");
 *
 *        // ...
 *
 * }
 * @endcode
 *
 * @param[in] req  req object for the current request<br>
 *                 MUST be a valid req pointer returned from http_req()
 * @param[out] len on return, `*len` is the length of the query string<br>
 *                 set to 0 if there was no query string<br>
 *                 MAY NOT be `NULL`
 * @return         the request query string, or `NULL` if there was none
 */
static inline const uint8_t *
http_req_query(struct req *req, size_t *len)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	AN(len);

	*len = req->query.len;
	if (req->query.len == 0)
		return NULL;
	return req->p->payload + req->query.off;
}

/**
 * @brief Return the value of a query parameter
 * @ingroup req
 *
 * Return the value of the first instance of parameter `name` in the query
 * string `query`, or `NULL` if `name` is not in `query`. For example, if
 * `query` contains `foo=bar`, and `name` is `"foo"`, then
 * http_req_query_val() returns `"bar"`.
 *
 * `name` does not necessarily have to be nul-terminated. After successful
 * return, `*val_len` is set to the length of the value, or 0 if `name` is
 * not found.
 *
 * @attention The returned string is @em not nul-terminated.
 *
 * `query` may be any query string (names and values separated by `=`, and
 * name-value pairs separated by `&`). It may be a query string from the
 * request URL, as returned by http_req_query(), or the body of a POST
 * request with content type `application/x-www-form-urlencoded`.
 *
 * Example:
 *
 * @code{.c}
 * #include <stdint.h>
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *        const uint8_t *query, *val;
 *        size_t query_len, val_len;
 *
 *        // ...
 *
 *        query = http_req_query(req, &query_len);
 *        if (query != NULL) {
 *                val = http_req_query_val(query, query_len, "foo", 3,
 *                                         &val_len);
 *                if (val != NULL)
 *                        HTTP_LOG_INFO("Value of 'foo': %.*s", val_len, val);
 *                else
 *                        HTTP_LOG_INFO("'foo' not found in the query string");
 *                // ...
 *        }
 *
 *        // ...
 * }
 * @endcode
 *
 * @param[in]  query     query string in which to search for `name`
 * @param[in]  query_len length of `query`
 * @param[in]  name      name of a parameter in `query`
 * @param[in]  name_len  length of `name`
 * @param[out] val_len   on return, `*val_len` is the length of the returned
 *                       value, or 0 if `name` is not found
 * @return               the value of `name` in `query`<br>
 *                       `NULL` if `name` is not found, or if any of `query`
 *                       `name` or `val_len` is `NULL`
 * @see                  http_req_query()
 */
const uint8_t * http_req_query_val(const uint8_t *query, size_t query_len,
				   const uint8_t *name, size_t name_len,
				   size_t *val_len);

/**
 * @brief Iterate over query string parameter names and values
 * @ingroup req
 *
 * Iterate over the parameter names and values in a query string, where
 * names and values are separated by `=`, and name-value pairs by `&`. The
 * query string may obtained from the URL as the string following the `?`
 * character (see http_req_query()), or from the body of a POST request
 * with content type `application/x-www-form-urlencoded`. The `iter_cb`
 * callback is invoked for every name/value pair in the query string.
 *
 * The value assigned to a query string parameter may be empty, for example
 * the value of `foo` in strings such as:
 *
 * * `foo&bar=baz`
 * * `foo=&bar=baz`
 *
 * If the value is empty, then the `val_len` argument passed to the
 * callback (for the length of the value) is 0. In that case, the `val`
 * argument should be ignored (it will not necessarily be `NULL`).
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * query_iter_cb(const char *name, size_t name_len, const char *val,
 *               size_t val_len, void *priv)
 * {
 *        if (val_len == 0)
 *                HTTP_LOG_INFO("query parameter %.*s with no value",
 *                              name_len, name);
 *        else
 *                HTTP_LOG_INFO("query parameter %.*s: %.*s",
 *                              name_len, name, val_len, val);
 *        // ...
 *        return ERR_OK;
 * }
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *        const uint8_t *query;
 *        size_t qlen;
 *
 *        // ...
 *        query = http_req_query(req, &qlen);
 *        if (qlen != 0)
 *                if (http_req_query_iter(query, qlen, query_iter_cb, NULL)
 *                    != ERR_OK) {
 *                        // Error handling ...
 *                }
 *        // ...
 * }
 * @endcode
 *
 * @param[in] query   query string<br>
 *                    may not be `NULL`
 * @param[in] len     length of `query`<br>
 *                    may not be 0
 * @param[in] iter_cb @link name_val_iter_f iterator callback function
 *                    @endlink<br>
 *                    invoked for every query parameter name/value
 * @param[in] priv    pointer to any object, or `NULL`<br>
 *                    passed in as the `priv` argument to the callback
 * @return            `ERR_OK` if each invocation of the callback returned
 *                    `ERR_OK`<br>
 *                    `ERR_ARG` if `query` is `NULL` or `len` is 0<br>
 *                    otherwise any non-`ERR_OK` value returned from the
 *                    callback
 * @see               name_val_iter_f, http_req_query(),
 *                    [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_req_query_iter(const uint8_t *query, size_t len,
			  name_val_iter_f iter_cb, void *priv);

/**
 * @brief Return the length of the request body
 * @ingroup req
 *
 * Return the length of the request body as set in the `Content-Length`
 * request header, or 0 if there was no such header.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *p)
 * {
 *        struct req *req = http_req(http);
 *        size_t body_len;
 *
 *        // ...
 *
 *        body_len = http_req_body_len(req);
 *        if (body_len != 0) {
 *                // Read the request body ...
 *        }
 *
 *        // ...
 *
 * }
 * @endcode
 *
 * @param[in] req  req object for the current request<br>
 *                 MUST be a valid req pointer returned from http_req()
 * @return         length of the request body as set in the `Content-Length`
 *                 header, or 0 if there was no such header
 * @see            http_req_body(), http_req_body_ptr()
 */
static inline size_t
http_req_body_len(struct req *req)
{
	CHECK_OBJ_NOTNULL(req, REQ_MAGIC);
	return req->clen;
}

/**
 * @brief Copy the request body to a buffer
 * @ingroup req
 *
 * Copy the request body, if any, into `buf`, wholly or partially. On
 * invocation, set `*len` to the maximum number of bytes to copy; after
 * successful return, `*len` is the number of bytes that were copied.  If
 * there is no request body, `*len` is 0 after the call. The body is
 * copied from the offset in `off`; set `off` to 0 to read the body from
 * the beginning.
 *
 * @attention http_req_body() may copy fewer bytes than requested.  Check
 *            the value of `*len` after the call; if the request body has
 *            been partially read, http_req_body() may be called again
 *            with an appropriate value in `off` to copy more bytes.
 *
 * http_req_body_len() may be used to determine the size of the request
 * body. This makes it possible to dynamically allocate memory for the
 * copied body, or to read it incrementally into small buffers, as
 * illustrated in the example.
 *
 * Example:
 *
 * @code{.c}
 * #include <stdlib.h>
 * #include <stdint.h>
 *
 * #include "picow_http/http.h"
 *
 * // This handler uses malloc() to allocate a buffer for the whole body.
 * err_t
 * a_handler(struct http *http, void *p)
 * {
 *         uint8_t *buf;
 *         size_t body_len, l, total;
 *         err_t err;
 *         struct req *req = http_req(http);
 *
 *         body_len = http_req_body_len(req);
 *         l = body_len;
 *
 *         buf = malloc(body_len);
 *         if (buf == NULL) {
 *                 // Error handling ...
 *         }
 *         err = http_req_body(http, buf, &l, 0);
 *         if (err != ERR_OK) {
 *                 // Error handling ...
 *         }
 *
 *         // If the copy was incomplete, continue copying.
 *         total = l;
 *         while (total < body_len) {
 *                 l = body_len - total;
 *                 err = http_req_body(http, buf + total, &l, total);
 *                 if (err != ERR_OK) {
 *                         // Error handling ...
 *                 }
 *                 total += l;
 *         }
 *
 *         // The body has now been fully read and can be processed ...
 * }
 *
 * // This handler reads the body incrementally into a limited buffer.
 * err_t
 * another_handler(struct http *http, void *p)
 * {
 *         uint8_t buf[MAX_BUF];
 *         size_t body_len, l;
 *         err_t err;
 *         struct req *req = http_req(http);
 *
 *         body_len = http_req_body_len(req);
 *         for (size_t total = 0; total < body_len; total += l) {
 *                 l = MAX_BUF;
 *                 if ((err = http_req_body(http, buf, &l, 0)) != ERR_OK) {
 *                         // Error handling ...
 *                 }
 *                 // Process the body partially copied into buf ...
 *         }
 * }
 * @endcode
 *
 * @param[in] http    MUST be a valid HTTP object passed into a handler
 *                    function
 * @param[in] buf     buffer into which the request body is copied
 * @param[in,out] len before the call, set `*len` to the maximum number of
 *                    bytes to copy to `buf`<br>
 *                    after the call, `*len` is the number of bytes copied,
 *                    0 if there was no request body
 * @param[in] off     offset in bytes from which to copy the request body
 * @return            ERR_OK on success<br>
 *                    ERR_ARG if either of `buf` or `len` is `NULL`<br>
 *                    ERR_VAL if `off` is greater than the size of the
 *                    request body<br>
 *                    ERR_BUF if the copy fails<br>
 *                    ERR_INPROGRESS if the portion of the request body
 *                    to be copied has not yet been fully received; in
 *                    this case, http_req_body() may be retried, since the
 *                    TCP stack is still receiving the request
 * @see               http_req_body_len(), http_req_body_ptr(),
 *                    [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_req_body(struct http *http, uint8_t buf[], size_t *len, size_t off);

/**
 * @brief Get a pointer to request body contents with zero-copy
 * @ingroup req
 *
 * Set `buf` to point to a read-only region of the request body starting
 * at `off` bytes from the beginning of the body. After the invocation,
 * `*len` is the length of the region. The resulting pointer directly
 * addresses the message payload in the TCP stack, so that no copying is
 * necessary.
 *
 * Depending on its length, the request body may occupy more than one
 * non-contiguous buffer in the TCP stack. In that case, `*len` is set to
 * the length of contiguous memory accessible from `buf`, which may be
 * less than the length of the body. Then additional invocations of
 * http_req_body_ptr() are necessary to access the whole body, in which
 * `off` is set to the start of the previously unread region.
 * http_req_body_len() can be used to get the total expected body length.
 *
 * After calling http_req_body_ptr(), the contents of the buffer MAY NOT
 * be modified.
 *
 * http_req_body_ptr() only makes sense for requests sent with a
 * `Content-Length` header. If there is none, or if the length was set to
 * 0, `*len` is set to 0.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // A custom response handler that processes the request body with
 * // zero copy.
 * err_t
 * no_copy_hndlr(struct http *http, void *priv)
 * {
 *        struct req *req = http_req(http);
 *        size_t req_len, off;
 *
 *        // Get the length of the request body.
 *        // If http_req_body_len() returns 0, this request was sent
 *        // without a Content-Length header (or content length 0).
 *        // In that case, the loop below is not executed.
 *        req_len = http_req_body_len(req);
 *
 *        // Process body contents in a loop.
 *        off = 0;
 *        while (req_len > 0) {
 *                const uint8_t *buf;
 *                size_t len;
 *                err_t err;
 *
 *                err = http_req_body_ptr(http, &buf, &len, off);
 *                if (err != ERR_OK) {
 *                    // Error handling ...
 *                }
 *
 *                // If the call returned ERR_OK, buf is now a pointer
 *                // to a region of length len in the request body.
 *
 *                // Process the buffer ...
 *
 *                // Count down from the total request length to determine
 *                // when the whole body has been read. Set the offset for
 *                // the next iteration to the previously unread portion
 *                // of the request body.
 *                req_len -= len;
 *                off += len;
 *        }
 *
 *        // Send a response ...
 *
 * }
 * @endcode
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @param[out] buf after a successful call, `buf` points to a region of the
 *                 request body
 * @param[out] len after a successful call, `*len` is the length of the region;
 *                 0 if there was no `Content-Length` header, or if it was
 *                 set to 0
 * @param[in]  off offset in bytes from the start of the request body
 *                 to which a pointer is requested
 * @return         ERR_OK on success<br>
 *                 ERR_ARG if either of `buf` or `len` is `NULL`<br>
 *                 ERR_VAL if `off` is greater than the size of the request
 *                 body<br>
 *                 ERR_INPROGRESS if the request body at the given offset
 *                 has not yet been fully received; in this case,
 *                 http_req_body_ptr() may be retried, since the TCP stack
 *                 is still receiving the request
 * @see            http_req_body_len(), http_req_body(),
 *                 [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_req_body_ptr(struct http *http, const uint8_t **buf, size_t *len,
			size_t off);

/**
 * @brief Copy the next chunk in a request body sent with chunked encdoding
 * @ingroup req
 *
 * Copy the next chunk in a request body that was sent with [chunked
 * transfer encoding](https://datatracker.ietf.org/doc/html/rfc9112#section-7.1)
 * into `buf`. On invocation, set `*len` to the maximum number of bytes to
 * copy; after successful return, `*len` is the number of bytes that were
 * copied -- the length of the chunk. When there are no chunks left,
 * `*len` is 0 after the call.
 *
 * Each sucessive invocation of http_req_chunk() copies the next chunk,
 * until the zero-length chunk signals the end of the request body. After
 * that, http_req_chunk() always sets `*len` to 0.
 *
 * Consider the following when using http_req_chunk():
 *
 * * A conforming HTTP client only sends a request body with chunked
 *   encoding when the request header `Transfer-Encoding` is present
 *   and has the value `chunked`. http_req_chunk()  doesn't check for this,
 *   but will reject invalid body contents (with `ERR_VAL`).
 * * A conforming client will not send both `Transfer-Encoding` and
 *   `Content-Length` in the same request header. The [HTTP
 *   standard](https://www.rfc-editor.org/rfc/rfc9112#name-transfer-encoding)
 *   gives the options to assume chunked encoding in such a case, or
 *   to reject the request (with [status 400 "Bad
 *   Request"](https://datatracker.ietf.org/doc/html/rfc9110#name-400-bad-request)).
 * * Do not use both of http_req_chunk() and http_req_body() for the same
 *   request.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * struct http *http;
 *
 * // Process request body chunks in a loop.
 * for (;;) {
 *         uint8_t buf[BUFLEN];
 *         size_t len = BUFLEN;
 *         err_t err;
 *
 *         // Before the call, len is the size of buf.
 *         if ((err = http_req_chunk(http, buf, &len)) != ERR_OK) {
 *                 // Error handling ...
 *         }
 *
 *         // len == 0 signals the end of the request body
 *         if (len == 0)
 *                 break;
 *
 *         // If len != 0 after calling http_req_chunk(), its value is
 *         // the length of the chunk copied into buf.
 *
 *         // Process the chunk ...
 *
 * }
 * @endcode
 *
 * @param[in] http    MUST be a valid HTTP object passed into a handler
 *                    function
 * @param[in] buf     buffer into which the chunk is to be copied
 * @param[in,out] len before the call, set `*len` to the maximum number of
 *                    bytes to copy to `buf`<br>
 *                    after the call, `*len` is the number of bytes copied
 *                    (the length of the chunk).<br>
 *                    0 when there are no more chunks
 * @return            ERR_OK on success<br>
 *                    ERR_ARG if either of `buf` or `len` is `NULL`<br>
 *                    ERR_VAL if the request body contents are invalid --
 *                    the next bytes are not correctly formatted as a
 *                    chunk<br>
 *                    ERR_MEM if there is insufficient space for the next
 *                    chunk (`*len` is too small)<br>
 *                    ERR_BUF if the copy fails<br>
 *                    ERR_INPROGRESS if the portion of the request body
 *                    to be copied has not yet been fully received; in
 *                    this case, http_req_chunk() may be retried, since the
 *                    TCP stack is still receiving the request
 * @see               [chunked transfer
 *                    encoding](https://datatracker.ietf.org/doc/html/rfc9112#section-7.1),
 *                    [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_req_chunk(struct http *http, uint8_t *buf, size_t *len);

/**
 * @brief Get the current response object
 * @ingroup resp
 *
 * Get the object representing the current response on an HTTP connection.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *         struct resp *resp;
 *
 *         resp = http_resp(http);
 *
 *         // ...
 * }
 * @endcode
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @return pointer to the current response object
 */
static inline struct resp *
http_resp(struct http *http)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);
	return &http->resp;
}

/**
 * @brief Set the response status code
 * @ingroup resp
 *
 * Set the [response status
 * code](https://www.rfc-editor.org/rfc/rfc9110.html#name-status-codes).
 * If the status is not set by the time the @link http_resp_send_hdr()
 * response header is sent @endlink, the status is set to 200 ("OK").
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        struct resp *resp = http_resp(http);
 *        // ...
 *
 *        // Set response status 202 "Accepted"
 *        if (http_resp_set_status(resp, HTTP_STATUS_ACCEPTED) != ERR_OK) {
 *                // Error handling
 *        }
 * }
 * @endcode
 *
 * @param[in] resp   resp object for the current response<br>
 *                   MUST be a valid resp pointer obtained from
 *                   http_resp()
 * @param[in] status response status
 * @return           ERR_OK on success<br>
 *                   ERR_VAL if `status` is out of range (< 100 or
 *                   >= 1000)
 * @see http_resp_status(), enum http_status_t,
 *       [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
static inline err_t
http_resp_set_status(struct resp *resp, uint16_t status)
{
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	if (status < 100 || status >= 1000)
		return ERR_VAL;
	resp->status = status;
	return ERR_OK;
}

/**
 * @brief Get the response status code
 * @ingroup resp
 *
 * Get the [response status
 * code](https://www.rfc-editor.org/rfc/rfc9110.html#name-status-codes)
 * currently set for `resp`. This may be a status previously set with
 * http_resp_set_status(), or the default 200 ("OK").
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // This is a custom error handler, see register_error_hndlr().
 * // If the handler was configured for mutliple error status codes,
 * // http_resp_status() can be used to distinguish them.
 * err_t
 * err_handler(struct http *http, void *priv)
 * {
 *        struct resp *resp = http_resp(http);
 *        uint16_t status;
 *        // ...
 *
 *        status = http_resp_status(resp);
 *        switch (status) {
 *        case HTTP_STATUS_NOT_FOUND:
 *                // ... generate the 404 response ...
 *                break;
 *
 *        // ... cases for other status codes ...
 *        }
 *        return ERR_OK;
 * }
 * @endcode
 *
 * @param[in] resp   resp object for the current response<br>
 *                   MUST be a valid resp pointer obtained from
 *                   http_resp()
 * @return           response status
 * @see http_resp_set_status(), enum http_status_t,
 *       [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
static inline uint16_t
http_resp_status(struct resp *resp)
{
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	return resp->status;
}

/**
 * @brief Size of "small" buffers for response headers
 * @ingroup resp
 *
 * The size in bytes of memory pool buffers that are initally used for
 * response headers. To save memory footprint, this value is set to a
 * small size that is sufficient for most or all response headers.
 *
 * The default value can be overridden by `#define`-ing
 * `RESP_HDR_SMALL_BUF_SZ` prior to including `picow_http/http.h`, or by
 * passing in a definition as a compile option.
 *
 * @see @ref resp_hdr_pools
 */
#ifndef RESP_HDR_SMALL_BUF_SZ
#define RESP_HDR_SMALL_BUF_SZ (512)
#endif

/**
 * @brief Size of "large" buffers for response headers
 * @ingroup resp
 *
 * The maximum size in bytes for a response header. Buffers of this size
 * are obtained from a memory pool if the header exceeds the "small" size
 * (#RESP_HDR_SMALL_BUF_SZ). By default, there are fewer buffers in the
 * "large" pool, on the assumption that they will only rarely be
 * necessary.
 *
 * The default is the TCP maximum segment size (MSS) configured for lwIP,
 * which in most cases is 1460 bytes. The default value can be overridden
 * by `#define`-ing `RESP_HDR_LARGE_BUF_SZ` prior to including
 * `picow_http/http.h`, or by passing in a definition as a compile option.
 *
 * @see @ref resp_hdr_pools
 */
#ifndef RESP_HDR_LARGE_BUF_SZ
#define RESP_HDR_LARGE_BUF_SZ (TCP_MSS)
#endif

#ifdef __DOXYGEN__
/**
 * @brief Number of buffers in the "small" memory pool for response headers
 * @ingroup resp
 *
 * The number of buffers of size #RESP_HDR_SMALL_BUF_SZ that are allocated
 * in the "small" memory pool for response headers.
 *
 * The default value is set to accomodate the maximum number of concurrent
 * connections for all of the servers (listeners) configured for the
 * application. If the macro `MAX_CONCURRENT_CX_HINT` has been defined
 * (for "max concurrent connection hint", see the [documentation for the
 * sample `lwipopts.h`](https://gitlab.com/slimhazard/picow_http/-/tree/master/etc)),
 * then the value is `MAX_CONCURRENT_CX_HINT` times the number of
 * listeners. Otherwise, the likely maximum for concurrent connections is
 * inferred from lwIP settings.
 *
 * The default value can be overridden by `#define`-ing
 * `RESP_HDR_SMALL_POOL_SZ` prior to including `picow_http/http.h`, or by
 * passing in a definition as a compile option.
 *
 * @see @ref resp_hdr_pools
 */
#define RESP_HDR_SMALL_POOL_SZ (MAX_CONCURRENT_CX_HINT)
#endif

#ifndef RESP_HDR_SMALL_POOL_SZ
#define RESP_HDR_SMALL_POOL_SZ (HTTP_N * SRV_N)
#endif

/**
 * @brief Number of buffers in the "large" memory pool for response headers
 * @ingroup resp
 *
 * The number of buffers of size #RESP_HDR_LARGE_BUF_SZ that are allocated
 * in the "large" memory pool for response headers. The default value is
 * half the size of the "small" pool for response headers.
 *
 * The default value can be overridden by `#define`-ing
 * `RESP_HDR_LARGE_POOL_SZ` prior to including `picow_http/http.h`, or by
 * passing in a definition as a compile option.
 *
 * @see @ref resp_hdr_pools
 */
#ifndef RESP_HDR_LARGE_POOL_SZ
#define RESP_HDR_LARGE_POOL_SZ (RESP_HDR_SMALL_POOL_SZ / 2)
#endif

/**
 * @brief Set a response header
 * @ingroup resp
 *
 * Add a response header field named `name` with the value `val`. `name`
 * and `val` do not necessarily have to be nul-terminated.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        struct resp *resp;
 *        // ...
 *
 *        // Set response header "Foo: bar"
 *        if (http_resp_set_hdr(resp, "Foo", 3, "bar", 3) != ERR_OK) {
 *                // Error handling
 *        }
 * }
 * @endcode
 *
 * @param[in] resp     resp object for the current response<br>
 *                     MUST be a valid resp pointer obtained from the
 *                     handler
 * @param[in] name     response header name
 * @param[in] name_len length of `name`
 * @param[in] val      header value
 * @param[in] val_len  length of `val`
 * @return             `ERR_OK` on success<br>
 *                     `ERR_VAL` if either of `name` or `val` is `NULL`<br>
 *                     `ERR_MEM` if there is insufficient memory for the
 *                               header
 * @see [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_resp_set_hdr(struct resp *resp, const char *name, size_t name_len,
			const char *val, size_t val_len);

/**
 * @brief Set a response header to a nul-terminated string
 * @ingroup resp
 *
 * Add a response header field named `name` with the value `val`. `name`
 * and `val` MUST be nul-terminated.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        struct resp *resp = http_resp(http);
 *        const char *hdr = "Baz", *val = "quux";
 *        // ...
 *
 *        // Set response header "Baz: quux"
 *        if (http_resp_set_hdr_str(resp, hdr, val) != ERR_OK) {
 *                // Error handling
 *        }
 * }
 * @endcode
 *
 * @param[in] resp     resp object for the current response<br>
 *                     MUST be a valid resp pointer obtained from
 *                     http_resp()
 * @param[in] name     response header name<br>
 *                     MAY NOT be `NULL`, and MUST be nul-terminated
 * @param[in] val      header value<br>
 *                     MAY NOT be `NULL`, and MUST be nul-terminated
 * @return             `ERR_OK` on success<br>
 *                     `ERR_MEM` if there is insufficient memory for the
 *                               header
 * @see                http_resp_set_hdr(),
 *                     [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
static inline err_t
http_resp_set_hdr_str(struct resp *resp, const char *name, const char *val)
{
	return http_resp_set_hdr(resp, name, strlen(name), val, strlen(val));
}

/**
 * @brief Set a response header to a literal string
 * @ingroup resp
 *
 * Add a response header field named `name` with the value `val`. `name`
 * and `val` MUST be literal strings (compile-time constant strings).
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        struct resp *resp = http_resp(http);
 *        // ...
 *
 *        // Set the Cache-Control to cache for one week
 *        if (http_resp_set_hdr_str(resp, "Cache-Control", "max-age=604800")
 *            != ERR_OK) {
 *                // Error handling
 *        }
 * }
 * @endcode
 *
 * @param[in] resp     resp object for the current response<br>
 *                     MUST be a valid resp pointer obtained from
 *                     http_resp()
 * @param[in] name     response header name<br>
 *                     MUST be a literal string
 * @param[in] val      header value<br>
 *                     MUST be a literal string
 * @return             `ERR_OK` on success<br>
 *                     `ERR_MEM` if there is insufficient memory for the
 *                               header
 * @see                http_resp_set_hdr(),
 *                     [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
#define http_resp_set_hdr_ltrl(resp, name, val)				\
	http_resp_set_hdr((resp), (name), STRLEN_LTRL(name), (val),	\
			  STRLEN_LTRL(val))

/**
 * @brief Set the Content-Type response header
 * @ingroup resp
 *
 * Add the Content-Type response header with the value `type`. `type` does
 * not necessarily have to be nul-terminated.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        struct resp *resp = http_resp(http);
 *        // ...
 *
 *        if (http_resp_set_type(resp, "text/html", 9) != ERR_OK) {
 *                // Error handling
 *        }
 * }
 * @endcode
 *
 * @param[in] resp     resp object for the current response<br>
 *                     MUST be a valid resp pointer obtained from
 *                     http_resp()
 * @param[in] type     value to set for the Content-Type header
 * @param[in] type_len length of `type`
 * @return             `ERR_OK` on success<br>
 *                     `ERR_VAL` if `type` is `NULL`<br>
 *                     `ERR_MEM` if there is insufficient memory for the
 *                               header
 * @see                http_resp_set_hdr(),
 *                     [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
static inline err_t
http_resp_set_type(struct resp *resp, const char *type, size_t type_len)
{
	return http_resp_set_hdr(resp, "Content-Type",
				 STRLEN_LTRL("Content-Type"), type, type_len);
}

/**
 * @brief Set the Content-Type response header to a nul-terminated string
 * @ingroup resp
 *
 * Add the Content-Type response header with the value `type`. `type` MUST
 * be nul-terminated.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        struct resp *resp = http_resp(http);
 *        const char *jpg_type = "image/jpeg";
 *        // ...
 *
 *        if (http_resp_set_type(resp, jpeg_type) != ERR_OK) {
 *                // Error handling
 *        }
 * }
 * @endcode
 *
 * @param[in] resp     resp object for the current response<br>
 *                     MUST be a valid resp pointer obtained from
 *                     http_resp()
 * @param[in] type     value to set for the Content-Type header<br>
 *                     MAY NOT be `NULL`, and MUST be nul-terminated
 * @return             `ERR_OK` on success<br>
 *                     `ERR_MEM` if there is insufficient memory for the
 *                               header
 * @see                http_resp_set_hdr(),
 *                     [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
static inline err_t
http_resp_set_type_str(struct resp *resp, const char *type)
{
	return http_resp_set_type(resp, type, strlen(type));
}

/**
 * @brief Set the Content-Type response header to a literal string
 * @ingroup resp
 *
 * Add the Content-Type response header with the value `type`. `type` MUST
 * be a literal string (compile-time constant string).
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        struct resp *resp = http_resp(http);
 *        // ...
 *
 *        if (http_resp_set_type(resp, "application/json") != ERR_OK) {
 *                // Error handling
 *        }
 * }
 * @endcode
 *
 * @param[in] resp     resp object for the current response<br>
 *                     MUST be a valid resp pointer obtained from
 *                     http_resp()
 * @param[in] type     value to set for the Content-Type header<br>
 *                     MUST be a literal string
 * @return             `ERR_OK` on success<br>
 *                     `ERR_MEM` if there is insufficient memory for the
 *                               header
 * @see                http_resp_set_hdr(),
 *                     [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
#define http_resp_set_type_ltrl(resp, type) \
	http_resp_set_type((resp), (type), STRLEN_LTRL(type))

/**
 * @brief Set the Content-Length response header
 * @ingroup resp
 *
 * Add the Content-Length response header with the value `len`.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        struct resp *resp = http_resp(http);
 *        // ...
 *
 *        // Set Content-Length to this value
 *        if (http_resp_set_len(resp, 4711) != ERR_OK) {
 *                // Error handling
 *        }
 * }
 * @endcode
 *
 * @param[in] resp resp object for the current response<br>
 *                 MUST be a valid resp pointer obtained from http_resp()
 * @param[in] len  value to set for the Content-Length header<br>
 *                 may not be greater than `INT32_MAX`
 * @return         `ERR_OK` on success<br>
 *                 `ERR_VAL` if `len` is out of range (> `INT32_MAX`)<br>
 *                 `ERR_MEM` if there is insufficient memory for the
 *                           header
 * @see [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)

 */
err_t http_resp_set_len(struct resp *resp, size_t len);

/**
 * @brief Set the Transfer-Encdoing response header to "chunked"
 * @ingroup resp
 *
 * Add the Transfer-Encoding response header with the value `"chunked"`.
 * This is required for [chunked transfer
 * encoding](https://datatracker.ietf.org/doc/html/rfc9112#section-7.1).
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *        struct resp *resp = http_resp(http);
 *        // ...
 *
 *        // Set the header for chunked encoding
 *        if (http_resp_set_xfer_chunked(resp) != ERR_OK) {
 *                // Error handling
 *        }
 *
 *        // ...
 * }
 * @endcode
 *
 * @param[in] resp resp object for the current response<br>
 *                 MUST be a valid resp pointer obtained from http_resp()
 * @return         `ERR_OK` on success<br>
 *                 `ERR_MEM` if there is insufficient memory for the
 *                           header
 * @see \ref resp_bodies, http_resp_send_chunk(),
 *      [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
static inline err_t
http_resp_set_xfer_chunked(struct resp *resp)
{
	CHECK_OBJ_NOTNULL(resp, RESP_MAGIC);
	return http_resp_set_hdr_ltrl(resp, "Transfer-Encoding", "chunked");
}

/**
 * @brief Send the response header
 * @ingroup resp
 *
 * Initiate sending the response header to the client. The contents of the
 * header are queued by the TCP stack, which then begins the process of
 * sending packets over the network. If the response status has not
 * already been set, it is set to 200. After calling this function, it is
 * no longer possible to change the status or add a response header.
 *
 * See \ref resp_hdr above for notes about the use of this function.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *
 *        // Set the response status and headers as required ...
 *
 *        if (http_resp_send_hdr(http) != ERR_OK) {
 *                // Error handling
 *        }
 * }
 * @endcode
 *
 * @param[in] http MUST be a valid HTTP object passed into a handler
 *                 function
 * @return    `ERR_OK` on success<br>
 *            `ERR_ALREADY` if the header has already been sent<br>
 *            `ERR_VAL` if the response status is out of range (< 100 or
 *                      >= 1000)<br>
 *            `ERR_MEM` if there is insufficient memory to send the header
 * @see       \ref resp_hdr,
 *            [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_resp_send_hdr(struct http *http);

/**
 * @brief Send the contents of a buffer as the response body
 * @ingroup resp
 *
 * Initiate sending `len` bytes in the fixed-length buffer `buf` as the
 * response body to the client. If the response header has not already
 * been sent, it is sent as well. The contents of the response (header
 * and) body are queued by the TCP stack, which then begins the process of
 * sending packets over the network.
 *
 * The response header `Content-Length` should have been set to the value
 * of `len`; http_resp_set_len() does this as a convenience. The header
 * must \em not include the field `Transfer-Encoding: chunked`.
 *
 * `buf` may not be `NULL`. Set `durable` to true if the contents of `buf`
 * can be queued for send without copying, as described \ref resp_bodies
 * "above".
 *
 * If the response status has not already been set, it is set to 200.
 * After calling this function, it is no longer possible to modify
 * any part of the response header or body.
 *
 * If the properties of the response are such that it is @ref resp_hdr
 * "header-only", a @ref HTTP_LOG_DEBUG "debug level message" is issued to
 * the log, the body is not sent, and the function returns `ERR_OK`.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // This handler uses a local array for the response body.
 * err_t
 * a_handler(struct http *http, void *priv)
 * {
 *         const uint8_t body[] = "Hello, world!";
 *         size_t body_len = sizeof(body) - 1;
 *
 *         // Set response status and headers as required ...
 *
 *         // Set durable to false, so that the array is copied for send.
 *         // Also sends the response header, if not already sent.
 *         if (http_resp_send_buf(http, body, body_len, false) != ERR_OK) {
 *                 // Error handling ...
 *         }
 *
 *         // ...
 * }
 *
 * // The next handler uses a static const array for the response body.
 * static const uint8_t durable_body[] = "Hello, universe!";
 * #define DURABLE_LEN ((sizeof(durable_body) - 1)
 *
 * err_t
 * another_handler(struct http *http, void *priv)
 * {
 *         // Set response status and headers as required ...
 *
 *         // Set durable to true, since the array need not be copied for send.
 *         if (http_resp_send_buf(http, durable_body, DURABLE_LEN, true)
 *             != ERR_OK) {
 *                 // Error handling ...
 *         }
 *
 *         // ...
 * }
 * @endcode
 *
 * @param[in] http    MUST be a valid HTTP object passed into a handler
 *                    function
 * @param[in] buf     pointer to the buffer containing the response body
 * @param[in] len     number of bytes in `buf` to send
 * @param[in] durable set to true iff `buf` points to durable storage, as
 *                    described above
 * @return            `ERR_OK` on success (the response header and/or body
 *                             is queued for send)<br>
 *                    `ERR_VAL` if `buf` is `NULL`<br>
 *                    `ERR_ALREADY` if sending the body has already begun<br>
 *                    any error that may be returned from http_resp_send_hdr(),
 *                             if the header must be sent<br>
 *                    any error that may be returned from the TCP stack for
 *                             queueing the response
 * @see               \ref resp_bodies, http_resp_send_chunk(),
 *                    http_resp_send_hdr(),
 *                    [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_resp_send_buf(struct http *http, const uint8_t *buf, size_t len,
			 bool durable);

/**
 * @brief Send a chunk in a chunked-encoded response body
 * @ingroup resp
 *
 * Add `len` bytes at `buf` as a chunk in a response body sent with
 * [chunked transfer
 * encoding](https://datatracker.ietf.org/doc/html/rfc9112#section-7.1).
 * Each chunk is queued for immediate send by the TCP stack. Call with
 * `len` == 0 to finalize sending the response.
 *
 * `buf` may not be `NULL` unless `len` is 0. Set `durable` to true if
 * the contents of `buf` can be queued for send without copying, as
 * described \ref resp_bodies "above".
 *
 * If the response header has not already been sent, it is sent before the
 * first chunk. If the response status has not already been set, it is set
 * to 200. The header cannot be modified after the first chunk is sent.
 *
 * Arbitrarily many chunks with `len` > 0 may be sent, but after an
 * invocation with `len` = 0, it is no longer possible to modify the
 * response body.
 *
 * It is the programmer's responsibility to use this function in such a
 * way that the response is successfully transmitted. Make sure that these
 * requirements are met:
 *
 * * The response header must include the field `Transfer-Encoding: chunked`.
 *   http_resp_set_xfer_chunked() does this as a convenience.
 * * A `Content-Length` header must \em not be set.
 * * A sequence of chunks must always be concluded by calling the function
 *   with `len` = 0 (and no further chunks may follow).
 * * Do not use both of http_resp_send_chunk() and http_resp_send_buf()
 *   for the same response.
 *
 * If the properties of the response are such that it is @ref resp_hdr
 * "header-only", a @ref HTTP_LOG_DEBUG "debug level message" is issued to
 * the log, the chunk is ignored, and the function returns `ERR_OK`.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * #define CHUNK_SZ (1024)
 * static uint8_t chunk_buf[CHUNK_SZ];
 *
 * void
 * next_chunk(uint8_t *buf, int n)
 * {
 *	// ... A function that fills the contents of the nth chunk.
 * }
 *
 * // This handler sends a 10 KB response body, 1 KiB at a time.
 * err_t
 * chunk_handler(struct http *http, void *priv)
 * {
 *         // Set response status and headers as required ...
 *
 *         // Set the header for chunked encoding
 *         if (http_resp_set_xfer_chunked(resp) != ERR_OK) {
 *                 // Error handling
 *         }
 *
 *         for (int i = 0; i < 10; i++) {
 *                 next_chunk(chunk_buf, i);
 *                 if (http_resp_send_chunk(http, chunk_buf, CHUNK_SZ, false)
 *             	       != ERR_OK) {
 *                         // Error handling ...
 *                 }
 *         }
 *
 *         // Finalize the response by sending a "null chunk".
 *         if (http_resp_send_chunk(http, NULL, 0, false) != ERR_OK) {
 *                 // Error handling ...
 *         }
 *
 *         // ...
 * }
 * @endcode
 *
 * @param[in] http    MUST be a valid HTTP object passed into a handler
 *                    function
 * @param[in] buf     pointer to the buffer containing the chunk contents
 * @param[in] len     number of bytes in `buf` to send, or 0 to finalize
 *                    the response
 * @param[in] durable set to true iff `buf` points to durable storage, as
 *                    described \ref resp_bodies "above"
 * @return            `ERR_OK` on success (the chunk and if necessary the
 *                             response header are queued for send)<br>
 *                    `ERR_VAL` if `buf` is `NULL` but `len` > 0<br>
 *                    any error that may be returned from http_resp_send_hdr(),
 *                             if the header must be sent<br>
 *                    any error that may be returned from the TCP stack for
 *                             queueing the response
 * @see               \ref resp_bodies, http_resp_send_hdr(),
 *                    http_resp_send_buf(),
 *                    [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t http_resp_send_chunk(struct http *http, const uint8_t *buf, size_t len,
			   bool durable);

/**
 * @brief Send an error response
 * @ingroup resp
 *
 * Initiate sending a standard error response with response code `status`.
 * By default, the server sends a simple and small (but not very
 * aesthetic) built-in response. If a
 * @ref register_error_hndlr() "custom error handler" has been regsitered
 * that is
 * [configured](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration)
 * for `status`, the custom handler is called.
 *
 * The header includes the field "Connection: close", and the connection
 * to the client is closed after the response is sent.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // This handler includes an implementation of error handling (a
 * // possible solution for what is glossed over as "Error handling ..."
 * // elsewhere in this documentation).
 * err_t
 * my_handler(struct http *http, void *priv)
 * {
 *         err_t err;
 *
 *         // ...
 *
 *         // If setting the header fails, send an error response with status
 *         // 500 (Internal Server Error).
 *         err = http_resp_set_hdr_literal(resp, "Foo", "bar");
 *         if (err != ERR_OK) {
 *                HTTP_LOG_ERROR("Error setting response header: %d", err);
 *                return http_resp_err(http, HTTP_STATUS_INTERNAL_SERVER_ERROR);
 *         }
 *
 *         // ...
 * }
 * @endcode
 *
 * @param[in] http   MUST be a valid HTTP object passed into a handler
 *                   function
 * @param[in] status response status code<br>
 *                   MUST be >= 400
 * @return           `ERR_OK` on success<br>
 *                   or any error that may result from setting headers or
 *                   sending the response
 * @see register_error_hndlr(),
 * [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
static inline err_t
http_resp_err(struct http *http, uint16_t status)
{
	CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
	CHECK_OBJ_NOTNULL(&http->resp, RESP_MAGIC);

	http->resp.status = status;
	return http_hndlr_err(http, NULL);
}

/**
 * @brief Register a response handler for request methods and a path
 * @ingroup resp
 *
 * Register `hndlr` as the response handler for requests with the methods
 * designated in `methods` and the URL `path`, for the server configured
 * by `cfg`. A custom handler MUST have been configured for `path` and
 * each of the `methods` in the [build
 * configuration](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration).
 *
 * `methods` is a bit map, where the bit positions are values of enum
 * @link http_method_t @endlink. For example, set bit `(1U <<
 * HTTP_METHOD_GET)` in `methods` to register `hndlr` for the `GET`
 * method.
 *
 * Otherwise, the same conditions apply as for register_hndlr().
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // Bitmap representing the REST methods.
 * #define REST_METHODS \
 * 	((1U << HTTP_METHOD_GET) | (1U << HTTP_METHOD_POST) | \
 *	 (1U << HTTP_METHOD_PUT) | (1U << HTTP_METHOD_DELETE))
 *
 *
 * // Handler for a REST endpoint
 * err_t
 * rest_handler(struct http *http, void *priv)
 * {
 *         // ... generate a response for GET, POST, PUT or DELETE /rest
 * }
 *
 * // Handler for GET or HEAD /foo requests
 * // Also invoked with any query string, such as /foo?bar=baz
 * err_t
 * foo_handler(struct http *http, void *priv)
 * {
 *         // ... generate a response for GET or HEAD /foo
 * }
 *
 * int
 * main(void)
 * {
 *         struct server *srv;
 *         struct server_cfg cfg;
 *
 *         // Get the default configuration
 *         cfg = http_default_cfg();
 *
 *         // Register rest_handler for the REST endpoint
 *         if (register_hndlr_methods(
 *                 &cfg, "/rest", rest_handler, REST_METHODS, NULL) != ERR_OK) {
 *                 // Error handling ...
 *         }
 *
 *         // Register foo_handler for GET and HEAD /foo
 *         // Using HTTP_METHODS_GET_HEAD defined in http.h
 *         if (register_hndlr(
 *                 &cfg, "/foo", foo_handler, HTTP_METHODS_GET_HEAD, NULL)
 *             != ERR_OK) {
 *                 // Error handling ...
 *         }
 *
 *         // Start the server.
 *         // The handlers registered above are immediately active.
 *         if (http_srv_init(&srv, &cfg) != ERR_OK) {
 *                 // Error handling ...
 *         }
 * }
 * @endcode
 *
 * @param[in] cfg     server configuration<br>
 *                    MUST be a valid configuration returned from
 *                    http_cfg() or http_default_cfg()
 * @param[in] path    URL path for which the handler is registered<br>
 *                    query strings do not form a part of the path
 * @param[in] hndlr   handler function to be registered<br>
 *                    MUST satisfy the typedef for @link hndlr_f @endlink
 * @param[in] methods bitmap representing the request methods for which
 *                    the handler is registered<br>
 *                    the bit positions are values of enum
 *                    @link http_method_t @endlink
 * @param[in] priv    pointer to private data passed into the handler<br>
 *                    set to `NULL` if handler-specific private data is
 *                    not needed
 * @return            `ERR_OK` on success<br>
 *                    `ERR_VAL` if either of `path` or `hndlr` is `NULL`<br>
 *                    `ERR_ARG` if no custom handler was configured for
 *                              `method` and `path` at build time
 * @see               register_hndlr(), register_default_hndlr(),
 *                    register_error_hndlr(), typedef hndlr_f, http_cfg(),
 *                    http_default_cfg(), enum http_method_t,
 *                    [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html),
 *                    @ref priv
 */
err_t register_hndlr_methods(struct server_cfg *cfg, const char *path,
			     hndlr_f hndlr, uint8_t methods, void *priv);

/**
 * @brief Register a response handler for a request method and path
 * @ingroup resp
 *
 * Register `hndlr` as the response handler for requests with `method` and
 * `path`, for the server configued by `cfg`. A custom handler MUST have
 * been configured for `method` and `path` in the [build
 * configuration](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration).
 * `hndlr` MUST have the function signature defined for @link hndlr_f
 * @endlink.
 *
 * A query string is not considered part of the path. So requests for
 * `method` and `path` with any query string (or none) are directed to the
 * handler.
 *
 * register_hndlr() may be called prior to @link http_srv_init() server
 * start, @endlink so that the custom handler is immediately active for
 * requests for which it is configured. If the server is started and
 * requests are received for which register_hndlr() has not yet been
 * invoked, the server sends an error response with status 501 ("Not
 * Implemented").
 *
 * `priv` is an optional pointer that is passed in the `priv` parameter of
 * the handler, so that application code can access handler-specific data.
 * If a `priv` pointer is used, ensure that it points to storage that is
 * allocated for the lifetime of the server. See the discussion in @ref
 * priv.
 *
 * Unlike other API functions that provide for private data,
 * register_hndlr() and register_hndlr_methods() do not have a parameter
 * for a @ref priv_fini_f "finalizer function". If you need to de-allocate
 * resources associated with `priv`, do so after calling http_srv_fini().
 * See the discussion in @ref priv.
 *
 * If the same handler should be invoked for more than one method for the
 * same path, for example to use the same handler for `GET` and `HEAD`
 * requests, use register_hndlr_methods(), or call register_hndlr() with
 * each method for that path.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // Declaration of the private data type to be used by the foo handler.
 * // Using the "magic number" idiom, to enable checks from assertion.h
 * struct foo_data {
 *         unsigned magic;
 * #define FOO_MAGIC 0xf9d15092
 *         // ... additional struct members ...
 * };
 *
 * // Static declaration and initialization of private data for the
 * // foo handler
 * static struct foo_data foo = {
 *         .magic = FOO_MAGIC,
 *         // ... additional struct member initalizations ...
 * };
 *
 * // Handler for POST /foo requests
 * err_t
 * foo_handler(struct http *http, void *priv)
 * {
 *         struct foo_data *hndlr_data;
 *
 *         // Using checks from assertion.h
 *         CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
 *         // In debug builds, this verifies the type of the priv pointer
 *         CAST_OBJ_NOTNULL(hndlr_data, priv, FOO_MAGIC);
 *
 *         // ... generate a response for POST /foo
 * }
 *
 * // Handler for GET or HEAD /bar requests
 * // Also invoked with any query string, such as /bar?baz=quux
 * err_t
 * bar_handler(struct http *http, void *priv)
 * {
 *         // Using a check from assertion.h.
 *         CHECK_OBJ_NOTNULL(http, HTTP_MAGIC);
 *
 *         // No priv pointer is used by this handler
 *         (void)priv;
 *
 *         // ... generate a response for GET or HEAD /foo
 * }
 *
 * int
 * main(void)
 * {
 *         struct server *srv;
 *         struct server_cfg cfg;
 *
 *         // Get the default configuration
 *         cfg = http_default_cfg();
 *
 *         // Register foo_handler for POST /foo
 *         if (register_hndlr(
 *                 &cfg, "/foo", foo_handler, HTTP_METHOD_POST, &foo)
 *             != ERR_OK) {
 *                 // Error handling ...
 *         }
 *
 *         // Register bar_handler for GET and HEAD /bar
 *         // Here we call register_hndlr() for both methods separately;
 *         // the same could be accomplished by calling register_hndlr_methods()
 *         // with the bitmap defined in HTTP_METHODS_GET_HEAD.
 *         // Set priv to NULL, since the handler doesn't use private data
 *         if (register_hndlr(
 *                 &cfg, "/bar", bar_handler, HTTP_METHOD_GET, NULL)
 *             != ERR_OK) {
 *                 // Error handling ...
 *         }
 *         if (register_hndlr(
 *                 &cfg, "/bar", bar_handler, HTTP_METHOD_HEAD, NULL)
 *             != ERR_OK) {
 *                 // Error handling ...
 *         }
 *
 *         // Start the server.
 *         // The handlers registered above are immediately active.
 *         cfg = http_default_cfg();
 *         if (http_srv_init(&srv, &cfg) != ERR_OK) {
 *                 // Error handling ...
 *         }
 * }
 * @endcode
 *
 * @param[in] cfg    server configuration<br>
 *                   MUST be a valid configuration returned from http_cfg()
 *                   or http_default_cfg()
 * @param[in] path   URL path for which the handler is registered<br>
 *                   query strings do not form a part of the path
 * @param[in] hndlr  handler function to be registered<br>
 *                   MUST satisfy the typedef for @link hndlr_f @endlink
 * @param[in] method enum value representing the request method for which
 *                   the handler is registered
 * @param[in] priv   pointer to private data passed into the handler<br>
 *                   set to `NULL` if handler-specific private data is
 *                   not needed
 * @return           `ERR_OK` on success<br>
 *                   `ERR_VAL` if either of `path` or `hndlr` is `NULL`,
 *                             or if `method` is invalid (not a valid value
 *                             of enum @link http_method_t @endlink)<br>
 *                   `ERR_ARG` if no custom handler was configured for
 *                             `method` and `path` at build time
 * @see              register_hndlr_methods(), register_default_hndlr(),
 *                   register_error_hndlr(), typedef hndlr_f,
 *                   enum http_method_t,
 *                   [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html),
 *                   @ref priv
 */
static inline err_t
register_hndlr(struct server_cfg *cfg, const char *path, hndlr_f hndlr,
	       enum http_method_t method, void *priv)
{
	if (method < 0 || method >= __HTTP_METHOD_MAX)
		return ERR_VAL;
	return register_hndlr_methods(cfg, path, hndlr, (1U << method), priv);
}

/**
 * @brief Register a default response handler
 * @ingroup resp
 *
 * Register `hndlr` as the default response handler for the server
 * configured by `cfg`. If a default handler has been registered, it is
 * invoked for any request whose path does not match any path specified
 * in the `static` or `custom` elements of the [build
 * configuration](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration).
 * `hndlr` MUST have the function signature defined for
 * @link hndlr_f @endlink.
 *
 * A default response handler is optional. If no default handler is
 * registered, the server returns an error response with status 404 ("Not
 * found") for requests whose path does not match the build configuration.
 * If a default handler is registered, it may choose to generate responses
 * for any other path and method, and takes on the role of returning 404
 * responses when appropriate.
 *
 * `priv` is an optional pointer to private data for the default handler
 * that is passed in the `priv` parameter of `hndlr`. `fini` is an
 * optional @ref priv_fini_f "finalizer" for `priv`. Set one or both or
 * `priv` and `fini` to `NULL` if not needed; see @ref priv.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * // Default handler that generates responses to POST requests whose path
 * // begins with "/foo". Return a 404 response for any other request.
 * // No private data is used.
 * err_t
 * foo_hndlr(struct http *http, void *priv)
 * {
 *         struct req *req = http_req(http);
 *         const char *path;
 *         size_t path_len;
 *         (void)priv;
 *
 *         path = http_req_path(req, &path_len);
 *
 *         // Return a 404 response if the path prefix is not "/foo".
 *         if (path_len < STRLEN_LTRL("/foo") ||
 *             memcmp(path, "/foo", STRLEN_LTRL("/foo")) != 0)
 *                 return http_resp_err(http, HTTP_STATUS_NOT_FOUND);
 *
 *         // Return a 405 response if the method is not POST.
 *         if (http_req_method(req) != HTTP_METHOD_POST)
 *                 return http_resp_err(http, HTTP_STATUS_METHOD_NOT_ALLOWED);
 *
 *         // ... generate a response for POST /foo* ...
 *
 *         return ERR_OK;
 * }
 *
 * // Register the default handler.
 * struct server_cfg cfg = http_default_cfg();
 * if ((err = register_default_hndlr(&cfg, foo_hndlr, NULL, NULL)) != ERR_OK) {
 *         // ... error handling ...
 * }
 * @endcode
 *
 * @param[in] cfg    server configuration<br>
 *                   MUST be a valid configuration returned from http_cfg()
 *                   or http_default_cfg()
 * @param[in] hndlr  default handler function to be registered<br>
 *                   MUST satisfy typedef @ref hndlr_f
 * @param[in] priv   pointer to private data passed into the handler<br>
 *                   set to `NULL` if private data is not needed for the
 *                   default handler
 * @param[in] fini   pointer to a @ref priv_fini_f "finalizer function" for
 *                   `priv`<br>
 *                   set to `NULL` if not needed
 * @return           `ERR_OK` on success<br>
 *                   `ERR_VAL` if either of `cfg` or `hndlr` is `NULL`
 * @see              register_hndlr(), register_hndlr_methods(),
 *                   typedef hndlr_f,
 *                   [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html),
 *                   @ref priv
 */
err_t register_default_hndlr(struct server_cfg *cfg, hndlr_f hndlr, void *priv,
			     priv_fini_f fini);

/**
 * @brief Register a custom error response handler
 * @ingroup resp
 *
 * Register `hndlr` as the custom error response handler for the server
 * configured by `cfg`, to generate responses with [status
 * codes](https://datatracker.ietf.org/doc/html/rfc9110#name-status-codes)
 * in the 400 and 500 range. A custom error handler can only be registered
 * if response codes were defined for it in the optional `custom_error`
 * section of the [build
 * configuration](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration)
 * (`www.yaml`).
 *
 * The `custom_error` configuration specifies selected error response
 * status codes in the range 400 to 599. If an error handler is
 * registered, it is invoked for responses with those status codes that
 * are to be generated by the server under various conditions, including:
 *
 * * responses generated by http_resp_err()
 * * if status codes 404 ("Not found") and/or 405 ("Method not allowed") were
 *   configured in `custom_error`, the error responses for requests whose
 *   path and method do not match any combination configured for `static` or
 *   `custom` in the [build
 * configuration](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration)
 *   (unless a @ref register_default_hndlr() "default handler" is registered)
 * * if status 406 ("Not acceptable") was configured, the error response for
 *   a static resource when the request's `Accept-Encoding` header does not
 *   match any
 *   [encoding](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration#staticencodings)
 *   configured for the resource
 * * if status 500 ("Internal server error") was configured, the error response
 *   generated when any @ref hndlr_f "response handler" does not return
 *   [ERR_OK](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 * * if status 501 ("Not implemented") was configured, the error response
 *   generated if the path and method of a request match a combination
 *   configured for `custom`, but no handler has been @ref register_hndlr()
 *   "registered" for it
 *
 * A custom error handler is optional. If for any such cases the response
 * code was not configured for a custom error handler, or if no error
 * handler has been registered, the server generates a built-in (small but
 * plain) error response.
 *
 * Before the error handler is called, the response header `Connection` is
 * added with the value `close`. Accordingly, the connection with the
 * client is closed after the response is sent. The handler code does not
 * need to add the `Connection` header.
 *
 * `priv` is an optional pointer to private data for the error handler
 * that is passed in the `priv` parameter of `hndlr`. `fini` is an
 * optional @ref priv_fini_f "finalizer" for `priv`. Set one or both or
 * `priv` and `fini` to `NULL` if not needed; see @ref priv.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * static const uint8_t err_body[] =
 *         "... HTML for an aesthetic error response goes here ...";
 *
 * static const size_t body_len = sizeof(err_body) - 1;
 *
 * // Custom error handler that returns the aesthetic error response
 * // for response status codes configured in the custom_error section
 * // of www.yaml.
 * static err_t
 * err_hndlr(struct http *http, void *priv)
 * {
 *         struct resp *resp = http_resp(http);
 *         err_t err;
 *
 *         // The response status will have already been set.
 *         // Connection: close is added automatically to the response header.
 *         // The client connection will be closed after the response is sent.
 *
 *         // Set Content-Type to text/html
 *         if ((err = http_resp_set_type_ltrl(resp, "text/html")) != ERR_OK)
 *                 return err;
 *
 *         // Set Content-Length
 *         if ((err = http_resp_set_len(resp, body_len)) != ERR_OK)
 *                 return err;
 *
 *         // Send the response header and body
 *         return http_resp_send_buf(http, body, body_len, true);
 * }
 *
 * // Register the error handler.
 * struct server_cfg cfg = http_default_cfg();
 * if ((err = register_error_hndlr(&cfg, err_hndlr, NULL, NULL)) != ERR_OK) {
 *         // ... error handling ...
 * }
 * @endcode
 *
 * @param[in] cfg    server configuration<br>
 *                   MUST be a valid configuration returned from http_cfg()
 *                   or http_default_cfg()
 * @param[in] hndlr  error handler function to be registered<br>
 *                   MUST satisfy typedef @ref hndlr_f
 * @param[in] priv   pointer to private data passed into the handler<br>
 *                   set to `NULL` if private data is not needed for the
 *                   error handler
 * @param[in] fini   pointer to a @ref priv_fini_f "finalizer function" for
 *                   `priv`<br>
 *                   set to `NULL` if not needed
 * @return           `ERR_OK` on success<br>
 *                   `ERR_VAL` if either of `cfg` or `hndlr` is `NULL`<br>
 *                   `ERR_ARG` if `custom_error` did not appear in the [build
 *                   configuration](https://gitlab.com/slimhazard/picow_http/-/wikis/Build-time-configuration)
 * @see              register_hndlr(), register_hndlr_methods(),
 *                   register_default_hndlr(), typedef hndlr_f,
 *                   [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html),
 *                   @ref priv
 */
err_t register_error_hndlr(struct server_cfg *cfg, hndlr_f hndlr, void *priv,
			   priv_fini_f fini);

/**
 * @brief Format a decimal number
 * @ingroup util
 *
 * Format `n` as an ASCII decimal number in the buffer `s`. This is
 * usually faster than a call such as `sprintf(s, "%d", n)`.
 *
 * @attention The resulting string is @em not nul-terminated; but a
 *            terminating nul character may be added as shown in the
 *            example.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * #define MAX_DIGITS (4)
 * char buf[MAX_DIGITS];
 * size_t buflen = sizeof(buf);
 *
 * if (format_decimal(buf, &buflen, 100) != ERR_OK) {
 *         // Error handling
 * }
 *
 * // Append a terminating nul, if there is space for it after formatting.
 * if (buflen < MAX_DIGITS)
 *         buf[len] = '\0';
 * @endcode
 *
 * @param[out] s buffer in which the number is formatted
 * @param[in,out] len before the call, `*len` is the size of `s`<br>
 *                    after successful return, `*len` is the length of
 *                    the string formatted in `s`
 * @param[in] n the number to be formatted
 * @return `ERR_OK` on success<br>
 *         `ERR_ARG` if either of `s` or `len` is `NULL`<br>
 *         `ERR_BUF` if `s` is too small for the formatted number
 * @see [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t format_decimal(char *s, size_t *len, int32_t n);

/**
 * @brief Format a hexadecimal number
 * @ingroup util
 *
 * Format `n` as an ASCII hexadecimal number in the buffer `s`. This is
 * usually faster than a call such as `snprintf(s, *len, "%x", n)`.
 *
 * If `upper` is true, the digits `A-F` are formatted in upper case;
 * otherwise they are rendered as `a-f`.
 *
 * The string is not formatted with leading zeroes, nor with a prefix such
 * as `0x`.
 *
 * @attention The resulting string is @em not nul-terminated; but a
 *            terminating nul character may be added as shown in the
 *            example.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * #define MAX_LEN (9)
 * char buf[MAX_LEN];
 * size_t buflen = sizeof(buf);
 *
 * // Rendered as "ca771e".
 * if (format_hex(buf, &buflen, 13268766, false) != ERR_OK) {
 *         // Error handling
 * }
 *
 * // Append a terminating nul, if there is space for it after formatting.
 * if (buflen < MAX_DIGITS)
 *         buf[len] = '\0';
 * @endcode
 *
 * @param[out] s buffer in which the number is formatted
 * @param[in,out] len before the call, `*len` is the size of `s`<br>
 *                    after successful return, `*len` is the length of
 *                    the string formatted in `s`
 * @param[in] n     the number to be formatted
 * @param[in] upper if true, hex digits are formatted as upper case,
 *                  otherwise as lower case
 * @return `ERR_OK` on success<br>
 *         `ERR_ARG` if either of `s` or `len` is `NULL`<br>
 *         `ERR_BUF` if `s` is too small for the formatted number
 * @see [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t format_hex(char *s, size_t *len, uint32_t n, bool upper);

/**
 * @brief Decode a "percent-encoded" string
 * @ingroup req
 *
 * Decode a string in the buffer `in` that may be URL-encoded or
 * "percent-encoded" (per
 * [RFC3986](https://datatracker.ietf.org/doc/html/rfc3986#section-2.1)),
 * saving the result in the buffer `out`.
 *
 * The decoding translates any sequence \%XX, where `XX` are two ASCII
 * hexadecimal digits, into the byte value of `XX`. If `plus` is true,
 * then it also changes any ASCII plus character `+` into the ASCII space
 * character ' ' (byte value 32). The percent character \% must be encoded
 * as \%25 (its ASCII value in hex).
 *
 * This makes it possible, for example, for the server to receive a query
 * string with values coded for characters that are otherwise special in a
 * query, such as `&` and `=`; or to encode non-ASCII characters in a
 * request body.
 *
 * The decoding stops at any nul byte (value 0) in the buffer. If `inlen`
 * >= 0, then at most `inlen` bytes are decoded. If `inlen` < 0, then the
 * input buffer MUST contain a nul byte.
 *
 * The buffers at `in` and `out` MAY NOT overlap (this is the condition
 * imposed by `restrict`).
 *
 * @attention The resulting string is @em not nul-terminated; but a
 *            terminating nul character may be added as shown in the
 *            example.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * #define BUFSZ (64)
 *
 * // This handler decodes the request query string, if present.
 * err_t
 * decode_handler(struct http *http, void *priv)
 * {
 *         struct req *req = http_req(http);
 *         uint8_t buf[BUFSZ];
 *         const uint8_t *query;
 *         size_t query_len, bufsz = BUFSZ;
 *
 *         // ...
 *
 *         //
 *         query = http_req_query(req, &query_len);
 *         if (query != NULL) {
 *                 // query_len is now the length of the query string.
 *                 // Set the plus parameter to true to convert '+' to ' '.
 *                 // Before the call, bufsz is the size of the output buffer.
 *                 err = url_decode(buf, &bufsz, query, query_len, true);
 *                 if (err != ERR_OK) {
 *                         // ... error handling ...
 *                 }
 *
 *                 // If the call was successful, the decoded string is in
 *                 // the output buffer buf, and bufsz is the length of the
 *                 // decoded string.
 *
 *                 // Append a terminating nul, if there is space for it after
 *                 //formatting.
 *                 if (bufsz < BUFSZ)
 *                         buf[bufsz] = '\0';
 *
 *                 // ...
 *         }
 *
 *         // ...
 * }
 * @endcode
 *
 * @param[out]    out    buffer into which the decoded string is written
 * @param[in,out] outlen before the call, `*outlen` is the size of `out`<br>
 *                       after successful return, `*outlen` is the length
 *                       the decoded string in `out`
 * @param[in]     in     the string to be decoded
 * @param[in]     inlen  if >= 0, decode at most `inlen` bytes in `in`<br>
 *                       otherwise decode up to the first nul byte
 * @param[in]     plus   if true, translate any ASCII `+` to ASCII ' ' (space)
 * @return `ERR_OK` on success<br>
 *         `ERR_ARG` if any of `in` or `out` or `outlen` is `NULL`<br>
 *         `ERR_BUF` if `out` is too small for the decoded string<br>
 *         `ERR_VAL` if the input string cannot be decoded
 * @see [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t url_decode(char * restrict const out, size_t *outlen,
		 const char * restrict const in, ssize_t inlen, bool plus);

/**
 * @brief Decode a hexadecimal string
 * @ingroup util
 *
 * Set `*n` to the value of the ASCII hexadecimal string in `buf`.
 *
 * The decoding stops at any nul byte (value 0) in the buffer. If `len` >=
 * 0, then at most `len` bytes are decoded. If `len` < 0, then the input
 * buffer must contain a nul byte.
 *
 * The input string must contain @em only hex digits. A prefix such as
 * `0x` is invalid.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * uint64_t n;
 *
 * if (hex_decode("ca771e", -1, &n) != ERR_OK) {
 *         // Error handling ...
 * }
 * // n now has the value 13268766.
 * @endcode
 *
 * @param[in]  buf the string to be decoded
 * @param[in]  len if >= 0, decode at most `len` bytes in `buf`<br>
 *                 otherwise decode up to the first nul byte
 * @param[out] n   on success, `*n` is the decoded numeric value
 * @return `ERR_OK`  on success<br>
 *         `ERR_ARG` if either of `buf` or `n` is `NULL`,
 *                   or if the result would be too large for `uint64_t`<br>
 *         `ERR_VAL` if any character in `buf` is not a hex digit
 * @see [lwIP err_t](https://www.nongnu.org/lwip/2_1_x/group__infrastructure__errors.html)
 */
err_t hex_decode(const char * const buf, ssize_t len, uint64_t *n);

/**
 * @brief Return the integer logarithm base 2 of an integer
 * @ingroup util
 *
 * For `n`&ne;0, return `floor(log`<sub>2</sub>`(n))` as an
 * integer. In other words, return the largest integer exponent `e`
 * such that 2<sup>`e`</sup>&le;`n`.
 *
 * `n` MAY NOT be zero. Note that `ilog2(n|1)` returns 0 for `n`=0,
 * and the same value as `ilog2(n)` for all other values of `n`.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * uint32_t n;
 *
 * n = ilog2(4711);
 * // n now has the value 12.
 * @endcode
 *
 * @param[in] n an unsigned integer<br>
 *              MAY NOT be 0
 * @return `log`<sub>2</sub>`(n)` rounded down to an integer
 * @see ilog10()
 */
static inline uint32_t
ilog2(uint32_t n)
{
	AN(n);
	return 31 - __builtin_clz(n);
}

/**
 * @brief Return the integer logarithm base 10 of an integer
 * @ingroup util
 *
 * For `n`&ne;0, return `floor(log`<sub>10</sub>`(n))` as an
 * integer. In other words, return the largest integer exponent `e`
 * such that 10<sup>`e`</sup>&le;`n`.
 *
 * `n` MAY NOT be zero. As with ilog2(), note that `ilog10(n|1)`
 * returns 0 for `n`=0, and the same value as `ilog10(n)` for all
 * other values of `n`.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * uint32_t n;
 *
 * n = ilog10(4711);
 * // n now has the value 3.
 * @endcode
 *
 * @param[in] n an unsigned integer<br>
 *              MAY NOT be 0
 * @return `log`<sub>10</sub>`(n)` rounded down to an integer
 * @see ilog2()
 */
static inline uint32_t
ilog10(uint32_t n)
{
	/*
	 * Algorithm from Hacker's Delight ch. 11-4, figures 11-12 and 11-13.
	 * See also Daniel Lemire's blog at:
	 * https://lemire.me/blog/2021/05/28/computing-the-number-of-digits-of-an-integer-quickly/
	 */
	static const uint32_t nines[] = {
		9, 99, 999, 9999, 99999, 999999, 9999999, 99999999, 999999999,
	};
	/* Multiplication by 9/32 roughly approximates division by log2(10). */
	uint32_t x = __fast_mul(ilog2(n), 9) >> 5;
	/* Fix any off-by-one error. */
	return x + bool_to_bit(n > nines[x]);
}

#endif /* _PICOW_HTTP_H */
