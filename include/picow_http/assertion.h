/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

/**
 * @file
 * Assertions, type checking, and pointer operations
 *
 * @defgroup assert Assertions and type checking
 * @ingroup util
 *
 * picow-http uses a set of macros to implement assertions that check
 * against a variety of "must not ever happen" errors in debug builds, as
 * well as some common operations for pointers to struct types. The
 * assertions primarily check for "wild pointer" errors -- `NULL`
 * dereferences or other invalid pointer values; but they can be used to
 * check any boolean condition. Violations of an assertion cause a panic
 * when `NDEBUG` is not defined, as is the case for the CMake `Debug`
 * [build type](https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html).
 * When `NDEBUG` is defined, as for CMake `Release` builds, the assertions
 * are no-ops. The intent is that errors lead to hard and fast failures
 * during development and debugging, and then the overhead of assertions
 * is removed when debugging is done. Since these mechanisms are
 * sufficiently general and may be relevant for application code, they are
 * provided in the public API.
 *
 * Many of the assertions check a "magic number", the value of a struct
 * member named `magic`. The idiom is that `magic` is the first member of
 * the struct (thus at offset 0 from the struct's starting address), set
 * to an unsigned 32-bit value specific to the struct type:
 *
 * @code{.c}
 * struct foo {
 *         unsigned magic;
 * #define FOO_MAGIC (0x47110815)
 *         // ... additional member declarations ...
 * };
 * @endcode
 *
 * The value of a magic number is best chosen at random, and must be
 * different for every struct type to be checked. To use the type checking
 * code:
 *
 * * When a struct object is initialized, set the `magic` field to the
 *   value for its type (see INIT_OBJ()).
 * * In functions for which a pointer to the struct type is passed in,
 *   check the `magic` field for the value expected for the type.
 *
 * If the magic number doesn't match, then the pointer is definitely of
 * the wrong type. If it matches, then it's almost certainly the right
 * type.
 *
 * This code was inspired by (and much of it copied from) the
 * [Varnish cache](http://varnish-cache.org) project (see
 * [miniobj.h](https://github.com/varnishcache/varnish-cache/blob/master/include/miniobj.h)
 * and the
 * [VAS interface](https://github.com/varnishcache/varnish-cache/blob/master/include/vas.h)).
 */

#include <string.h>

#include "pico/version.h"

#if PICO_SDK_VERSION_MAJOR < 2
#include "pico/platform.h"
#else
#include "pico.h"
#endif

/**
 * @brief Assert that a condition is true
 * @ingroup assert
 *
 * If `NDEBUG` is not defined, panic unless `c` is true (i.e panic if `c`
 * evaluates to zero).  If `NDEBUG` is defined, `PICOW_HTTP_ASSERT` is a
 * no-op.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * // In a debugging build, panic unless an expected value is set
 * PICOW_HTTP_ASSERT(foo == 4711);
 * @endcode
 *
 * @param[in] c a boolean expression
 *
 */
#if defined (NDEBUG) && !defined(__DOXYGEN__)
#define PICOW_HTTP_ASSERT(c) ((void)(c))
#else
#define PICOW_HTTP_ASSERT(c) do {			\
	if (!(c))					\
		panic("%s(): " #c " false",  __func__);	\
	} while (0)
#endif

/**
 * @brief Assert that a value is zero or `NULL`
 * @ingroup assert
 *
 * If `NDEBUG` is not defined, panic unless `x` is zero or `NULL`. No-op
 * if `NDEBUG` is defined.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * // In a debugging build, panic unless the pointer is NULL
 * AZ(ptr);
 * @endcode
 *
 * @param[in] x any expression. If `x` is a pointer, this is a check expecting
 *            `NULL`.
 *
 */
#define AZ(x)	do { PICOW_HTTP_ASSERT((x) == 0); } while (0)

/**
 * @brief Assert that a value is not zero, or not `NULL`
 * @ingroup assert
 *
 * If `NDEBUG` is not defined, panic if `x` is zero or `NULL`. No-op
 * if `NDEBUG` is defined.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * // In a debugging build, panic if the pointer is NULL
 * AN(ptr);
 * @endcode
 *
 * @param[in] x any expression. If `x` is a pointer, this is a check expecting
 *            non-`NULL`.
 *
 */
#define AN(x)	do { PICOW_HTTP_ASSERT((x) != 0); } while (0)

/*
 * The following does not include ALLOC_OBJ and FREE_OBJ from Varnish
 * miniobj.h, because we may want to use LWIP custom memory pools.
 */

/**
 * @brief Set an object to all zero
 * @ingroup assert
 *
 * Set `sz` bytes starting at address `to` to 0.
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * struct foo myfoo;
 *
 * ZERO_OBJ(&myfoo, sizeof(*myfoo));
 * @endcode
 *
 * @param[in] to a pointer
 * @param[in] sz number of bytes (commonly from `sizeof()`)
 *
 */
#define ZERO_OBJ(to, sz)                                                \
        do {                                                            \
                void *(*volatile z_obj)(void *, int, size_t) = memset;  \
                (void)z_obj(to, 0, sz);                                 \
        } while (0)

/**
 * @brief Initialize an object with its magic number
 * @ingroup assert
 *
 * Set the `magic` member of the object pointed to by `to` to
 * `type_magic`, and all other members to 0.
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * struct foo {
 *         unsigned magic;
 * #define FOO_MAGIC (0x47110815)
 *         // ... additional member declarations ...
 * };
 *
 * struct foo myfoo;
 *
 * INIT_OBJ(&myfoo, FOO_MAGIC);
 * @endcode
 *
 * @param[in] to         pointer to a struct type for which a magic number
 *                       has been defined
 * @param[in] type_magic type-specific magic number constant
 *
 */
#define INIT_OBJ(to, type_magic)                                        \
        do {                                                            \
                ZERO_OBJ(to, sizeof *(to));                             \
                (to)->magic = (type_magic);                             \
        } while (0)

/**
 * @brief Finalize an object
 * @ingroup assert
 *
 * Set the `magic` member of the object pointed to by `to` to 0, and set
 * `to` to `NULL`. This ensures that checks will fail if the code
 * subsequently attempts to access the object.
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * struct bar {
 *         unsigned magic;
 * #define BAR_MAGIC (0xf000baa4)
 *         // ... additional member declarations ...
 * };
 *
 * void
 * myfunc()
 * {
 *         struct bar mybar;
 *
 *         INIT_OBJ(&bar, BAR_MAGIC);
 *         // ... use the object locally in the function ...
 *
 *         // Finalize the object, so that subsequent accesses will fail.
 *         FINI_OBJ(&bar);
 * }
 * @endcode
 *
 * @param[in] to pointer to a struct type for which the `magic` member has
 *            been defined
 */
#define FINI_OBJ(to)                                                    \
        do {                                                            \
                ZERO_OBJ(&(to)->magic, sizeof (to)->magic);             \
                to = NULL;                                              \
        } while (0)

/**
 * @brief Return true if an object is valid for its type
 * @ingroup assert
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * struct baz {
 *         unsigned magic;
 * #define BAZ_MAGIC (0x8c60c6e9)
 *         // ... additional member declarations ...
 * };
 *
 * err_t
 * baz_func(struct baz *baz)
 * {
 *         // Return an error status if baz is not properly initialized
 *         if (!VALID_OBJ(baz, BAZ_MAGIC))
 *                 return ERR_ARG;
 *
 *         // ...
 *
 * }
 * @endcode
 *
 * @param[in] ptr        pointer to a struct type for which a magic number
 *                       has been defined
 * @param[in] type_magic type-specific magic number constant
 * @return               true if `ptr` is not `NULL` and the value of its
 *                       `magic` member is `type_magic`
 */
#define VALID_OBJ(ptr, type_magic)                                      \
        ((ptr) != NULL && (ptr)->magic == (type_magic))

/**
 * @brief Assert that an object is valid for its type
 * @ingroup assert
 *
 * If `NDEBUG` is not defined, panic if `ptr` points to a struct whose
 * `magic` member is not equal to `type_magic`. No-op if `NDEBUG` is
 * defined.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * struct quux {
 *         unsigned magic;
 * #define QUUX_MAGIC (0x39ee7191)
 *         // ... additional member declarations ...
 * };
 *
 * err_t
 * quux_func(struct quux *quux)
 * {
 *         // Return an error status if the object is NULL
 *         if (quux == NULL)
 *                 return ERR_ARG;
 *         // Assert that the object is valid
 *         CHECK_OBJ(quux, QUUX_MAGIC));
 *
 *         // ...
 *
 * }
 * @endcode
 *
 * @param[in] ptr        pointer to a struct type for which a magic number
 *                       has been defined. MAY NOT be `NULL`
 * @param[in] type_magic type-specific magic number constant
 */
#define CHECK_OBJ(ptr, type_magic)                                      \
        do {                                                            \
                PICOW_HTTP_ASSERT((ptr)->magic == type_magic);          \
        } while (0)

/**
 * @brief Assert that a pointer is not `NULL`, and points to an object
 *        that is valid for its type
 * @ingroup assert
 *
 * If `NDEBUG` is not defined, panic if `ptr` is `NULL`, or points to a
 * struct whose `magic` member is not equal to `type_magic`. No-op if
 * `NDEBUG` is defined.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * struct foobar {
 *         unsigned magic;
 * #define FOOBAR_MAGIC (0xf0000ba4)
 *         // ... additional member declarations ...
 * };
 *
 * err_t
 * foobar_func(struct foobar *foobar)
 * {
 *         // Assert that the pointer is non-NULL and the object is valid
 *         CHECK_OBJ_NOTNULL(foobar, FOOBAR_MAGIC));
 *
 *         // ...
 *
 * }
 * @endcode
 *
 * @param[in] ptr        pointer to a struct type for which a magic number
 *                       has been defined
 * @param[in] type_magic type-specific magic number constant
 */
#define CHECK_OBJ_NOTNULL(ptr, type_magic)                              \
        do {                                                            \
                PICOW_HTTP_ASSERT((ptr) != NULL);                       \
                PICOW_HTTP_ASSERT((ptr)->magic == type_magic);          \
        } while (0)

/**
 * @brief Assert that a pointer is either `NULL`, or it points to an object
 *        that is valid for its type
 * @ingroup assert
 *
 * If `NDEBUG` is not defined and `ptr` is not `NULL`, then panic if `ptr`
 * points to a struct whose `magic` member is not equal to
 * `type_magic`. No-op if `NDEBUG` is defined.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * struct bazquux {
 *         unsigned magic;
 * #define BAZQUUX_MAGIC (0x6c0d60cd)
 *         // ... additional member declarations ...
 * };
 *
 * err_t
 * bazquux_func(struct bazquux *bazquux)
 * {
 *         // If the pointer is non-NULL, assert that the object is valid
 *         CHECK_OBJ_ORNULL(bazquux, BAZQUUX_MAGIC));
 *         if (bazquux == NULL) {
 *                 bazquux = malloc(sizeof(*bazquux));
 *                 // ...
 *         }
 *
 *         // ...
 *
 * }
 * @endcode
 *
 * @param[in] ptr        pointer to a struct type for which a magic number
 *                       has been defined
 * @param[in] type_magic type-specific magic number constant
 */
#define CHECK_OBJ_ORNULL(ptr, type_magic)                               \
        do {                                                            \
                if ((ptr) != NULL)                                      \
                        PICOW_HTTP_ASSERT((ptr)->magic == type_magic);  \
        } while (0)

/**
 * @brief Cast a pointer, and if not `NULL`, assert that it points to an
 *        object that is valid for its type
 * @ingroup assert
 *
 * Assign `from` to `to`, and if `NDEBUG` is not defined and the pointers
 * are not `NULL`, then panic if `to` points to a struct whose `magic`
 * member is not equal to `type_magic`.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * struct hndl {
 *         unsigned magic;
 * #define HNDL_MAGIC (0xee0f1e3a)
 *         // ... additional member declarations ...
 * };
 *
 * err_t
 * hndl_func(void *priv)
 * {
 *         struct hndl *hndl;
 *
 *         if (priv == NULL) {
 *                 hndl = malloc(sizeof(*hndl));
 *                 // ...
 *         }
 *         else
 *              // Assert that the non-NULL priv pointer points to a
 *              // valid hndl object.
 *              CAST_OBJ(hndl, priv, HNDL_MAGIC));
 *
 *         // ...
 *
 * }
 * @endcode
 *
 * @param[in] to         pointer to a struct type for which a magic number
 *                       has been defined
 * @param[in] from       pointer
 * @param[in] type_magic type-specific magic number constant
 */
#define CAST_OBJ(to, from, type_magic)                                  \
        do {                                                            \
                (to) = (from);                                          \
                if ((to) != NULL)                                       \
                        CHECK_OBJ((to), (type_magic));                  \
        } while (0)

/**
 * @brief Cast a pointer, and assert that it it is not `NULL` and points
 *        to an object that is valid for its type
 * @ingroup assert
 *
 * Assign `from` to `to`, and if `NDEBUG` is not defined, then panic if
 * the pointer is `NULL`, or if after the cast, `to` points to a struct
 * whose `magic` member is not equal to `type_magic`.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/assertion.h"
 * // Or picow_http/http.h, which includes assertion.h
 *
 * struct hndlr_priv {
 *         unsigned magic;
 * #define HNDLR_PRIV_MAGIC (0xc71adcbb)
 *         // ... additional member declarations ...
 * };
 *
 * err_t
 * my_hndlr(struct http *http, void *priv)
 * {
 *         struct hndlr_priv *hndlr_priv;
 *
 *         // Assert that priv is not NULL and points to a valid
 *         // hndlr_priv object.
 *         CAST_OBJ_NOTNULL(hndlr_priv, priv, HNDLR_PRIV_MAGIC));
 *
 *         // ...
 *
 * }
 * @endcode
 *
 * @param[in] to         pointer to a struct type for which a magic number
 *                       has been defined
 * @param[in] from       pointer
 * @param[in] type_magic type-specific magic number constant
 */
#define CAST_OBJ_NOTNULL(to, from, type_magic)                          \
        do {                                                            \
                (to) = (from);                                          \
                AN((to));                                               \
                CHECK_OBJ((to), (type_magic));                          \
        } while (0)
