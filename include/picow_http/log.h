/*
 * Copyright (c) 2022 Geoff Simmons <geoff@simmons.de>
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * See LICENSE
 */

/**
 * @file
 * Logging
 *
 * @defgroup log Logging
 *
 * The logging API controls log output at selected verbosity levels, and
 * request logging. Logs are printed as formatted strings to `stdout`, and
 * hence are written to any devices configured for the Pico SDK's
 * [pico_stdio](https://raspberrypi.github.io/pico-sdk-doxygen/group__pico__stdio.html)
 * drivers. Log output can be formatted with the common "printf-like"
 * directives (`%%s`, `%%d` and so forth) implemented by `stdio`.
 *
 * Standard logging and log levels are mostly implemented by the
 * preprocessor, so that logging that is omitted due to the level is left
 * out of compilation altogether. Logging may have a significant impact on
 * the size and speed of the code; while verbose logging may be helpful
 * and tolerable during development, consider setting quieter log levels
 * for production deployments. For these reasons, the `HTTP_LOG_*()`
 * family of macros is the preferred means for standard logging.
 *
 * @section reqlog Request logging
 *
 * Request logging is configured separately from standard logging;
 * independently of the configured log level, the value of the
 * `HTTP_REQUEST_LOG` macro determines whether request logs are enabled or
 * disabled.
 *
 * When enabled, request logs are written in
 * [Common Log Format](https://en.wikipedia.org/wiki/Common_Log_Format),
 * where the date/time field is always in the UTC time zone (and hence
 * the time zone offset is always `+0000`). For example:
 *
 * @code
 * 192.0.2.47 - - [04/Jul/2022:11:47:47 +0000] "GET / HTTP/1.1" 200 4711
 * @endcode
 *
 * Request logs are emitted after the response has been fully queued for
 * send by the TCP stack, so they don't adversely impact the latency of a
 * response. Disabling request logs may shorten the delay between
 * completing a response and processing the next request on the same
 * network connection.
 */

#ifndef _PICOW_HTTP_LOG_H
#define _PICOW_HTTP_LOG_H

#include <stdio.h>
#include <stdarg.h>

/**
 * @brief Log level at which all standard logging is disabled
 * @ingroup log
 *
 * At this log level, all of the `HTTP_LOG_*()` calls are no-ops. It may
 * be used to eliminate all of the impact of logging.
 */
#define HTTP_LOG_LVL_NONE	(-1)
/**
 * @brief Log level for error messages
 * @ingroup log
 */
#define HTTP_LOG_LVL_ERROR	(0)
/**
 * @brief Log level for warnings
 * @ingroup log
 */
#define HTTP_LOG_LVL_WARN	(1)
/**
 * @brief Log level for informational messages
 * @ingroup log
 */
#define HTTP_LOG_LVL_INFO	(2)
/**
 * @brief Log level for debugging
 * @ingroup log
 */
#define HTTP_LOG_LVL_DEBUG	(3)
/**
 * @brief Log level at highest verbosity
 * @ingroup log
 */
#define HTTP_LOG_LVL_VERBOSE	(4)

/**
 * @brief Log level for standard logging
 * @ingroup log
 *
 * The verbosity level for standard logging. For `HTTP_LOG_*()` calls at
 * this level or at lower levels, the log message is printed; calls at
 * higher levels are no-ops.
 *
 * By default (if `HTTP_LOG_LVL` is not defined prior to including the
 * header file):
 *   * If `NDEBUG` is defined (as is the case for CMake
 *     [build type](https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html)
 *     `Release`), then `HTTP_LOG_LVL` is `HTTP_LOG_LVL_INFO`.
 *   * If `NDEBUG` is not defined (as for CMake `Debug` builds), then
 *     `HTTP_LOG_LVL` is `HTTP_LOG_LVL_DEBUG`.
 *
 * To override the default, define the value of `HTTP_LOG_LVL` prior to
 * including the header, for example with a `#define` before the
 * `#include`, or by passing in the definition as a compile option.
 *
 * Example using `#define`:
 *
 * @code{.c}
 * // Highest verbosity in a debug build
 * #ifndef NDEBUG
 * #define HTTP_LOG_LVL (HTTP_LOG_LVL_VERBOSE)
 * #endif
 *
 * // includes picow_http/log.h
 * #include "picow_http/http.h"
 * @endcode
 *
 * Example setting a compile definition in `CMakeLists.txt`:
 *
 * @code
 * # Set highest log verbosity in Debug builds.
 * set_directory_properties(PROPERTIES COMPILE_DEFINITIONS_DEBUG HTTP_LOG_LVL=HTTP_LOG_LVL_VERBOSE)
 * @endcode
 *
 * `HTTP_LOG_LVL` does not affect request logging, which is enabled or
 * disabled by @ref HTTP_REQUEST_LOG.
 */
#ifndef HTTP_LOG_LVL
# ifdef NDEBUG
# define HTTP_LOG_LVL (HTTP_LOG_LVL_INFO)
# else
# define HTTP_LOG_LVL (HTTP_LOG_LVL_DEBUG)
# endif
#endif

/**
 * @brief Enable or disable request logging
 * @ingroup log
 *
 * @ref reqlog is enabled when `HTTP_REQUEST_LOG` is non-zero, and
 * disabled when set to zero.
 *
 * By default, if `HTTP_LOG_LVL` is set to `HTTP_LOG_LVL_NONE`, then
 * request logging is disabled, otherwise it is enabled. To override the
 * default, define a value for `HTTP_REQUEST_LOG` prior to including the
 * header. This can be achieved with a `#define` before the `#include`, or
 * with a compile definition, as illustrated for @ref HTTP_LOG_LVL.
 */
#ifndef HTTP_REQUEST_LOG
# if HTTP_LOG_LVL == HTTP_LOG_LVL_NONE
# define HTTP_REQUEST_LOG (0)
# else
# define HTTP_REQUEST_LOG (1)
# endif
#endif

/**
 * @brief Print a log message at a log level
 * @ingroup log
 *
 * Print a log message to `stdout` if `LVL` is the same as or less verbose
 * than `HTTP_LOG_LVL`.
 *
 * The logging code appends a newline to the message output; so the
 * messages in `HTTP_LOG()` or in any of the `HTTP_LOG_*()` calls does not
 * have to end with `\n`.
 *
 * Note that `stdout` must be configured and initialized for output using
 * one or more of the drivers in the Pico SDK's
 * [pico_stdio](https://raspberrypi.github.io/pico-sdk-doxygen/group__pico__stdio.html)
 * API -- USB, UART or semihosting.
 *
 * @param[in] LVL MUST be one of the `HTTP_LOG_LVL_*` macros
 * @param[in] ... printf-like parameters for formatting a string -- a format
 *                string followed by 0 or more variables with values for the
 *                format directives.
 */
#define HTTP_LOG(LVL, ...) do {			\
		if ((LVL) <= (HTTP_LOG_LVL))	\
			http_log(__VA_ARGS__);	\
	} while (0)

/**
 * @brief Log an error message
 * @ingroup log
 *
 * Print a log message to `stdout` if `HTTP_LOG_LVL` has any value besides
 * `HTTP_LOG_LVL_NONE`. Intended for error messages.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/http.h"
 *
 * err_t err;
 * char *errmsg;
 *
 * // ...
 *
 * if (err != ERR_OK)
 *         HTTP_LOG_ERROR("Error %d: %s", err, errmsg):
 * @endcode
 *
 * @param[in] ... printf-like parameters for formatting a string, as for
 *                @ref HTTP_LOG
 */
#define HTTP_LOG_ERROR(...)   HTTP_LOG(HTTP_LOG_LVL_ERROR,   __VA_ARGS__)

/**
 * @brief Log a warning message
 * @ingroup log
 *
 * Print a log message if `HTTP_LOG_LVL` is `HTTP_LOG_LVL_WARN`, or more
 * verbose.
 *
 * Example:
 *
 * @code{.c}
 * #include "picow_http/log.h"
 * // Or picow_http/http.h, which includes log.h
 *
 * int foo, bar;
 *
 * // ...
 *
 * if (foo > bar)
 *         HTTP_LOG_WARN("foo=%d is greater than bar=%d", foo, bar):
 * @endcode
 *
 * @param[in] ... printf-like parameters for formatting a string, as for
 *                @ref HTTP_LOG
 */
#define HTTP_LOG_WARN(...)    HTTP_LOG(HTTP_LOG_LVL_WARN,    __VA_ARGS__)

/**
 * @brief Log an informational message
 * @ingroup log
 *
 * Print a log message if `HTTP_LOG_LVL` is `HTTP_LOG_LVL_INFO`, or more
 * verbose.
 *
 * Example:
 *
 * @code{.c}
 *
 * // This example includes the initialization of stdout, which is
 * // required for all HTTP logging output.
 *
 * #include <stdio.h>
 * #include "pico/stdio.h"
 *
 * // picow_http/log.h is included by http.h.
 * #include "picow_http/http.h"
 *
 * int
 * main(void)
 * {
 *         char *foobar, *bazquux;
 *
 *         // Initialize stdio on all enabled drivers.
 *         // For HTTP logging, it suffices to initialize stdout on
 *         // at least one driver; for example with stdout_uart_init(),
 *         // for log output via UART.
 *         stdio_init_all();
 *
 *         // ...
 *
 *         HTTP_LOG_INFO("foobar is %s and bazquux is %s", foobar,
 *                       bazquux):
 *
 *         // ...
 *
 * }
 * @endcode
 *
 * @param[in] ... printf-like parameters for formatting a string, as for
 *                @ref HTTP_LOG
 */
#define HTTP_LOG_INFO(...)    HTTP_LOG(HTTP_LOG_LVL_INFO,    __VA_ARGS__)

/**
 * @brief Log a debugging message
 * @ingroup log
 *
 * Print a log message if `HTTP_LOG_LVL` is `HTTP_LOG_LVL_DEBUG`, or more
 * verbose.
 *
 * @param[in] ... printf-like parameters for formatting a string, as for
 *                @ref HTTP_LOG
 */
#define HTTP_LOG_DEBUG(...)   HTTP_LOG(HTTP_LOG_LVL_DEBUG,   __VA_ARGS__)

/**
 * @brief Log a message at the highest verbosity level
 * @ingroup log
 *
 * Print a log message if `HTTP_LOG_LVL` is `HTTP_LOG_LVL_VEROSE`.
 *
 * @param[in] ... printf-like parameters for formatting a string, as for
 *                @ref HTTP_LOG
 */
#define HTTP_LOG_VERBOSE(...) HTTP_LOG(HTTP_LOG_LVL_VERBOSE, __VA_ARGS__)

/**
 * @brief Print a log message
 * @ingroup log
 *
 * This is the core function for logging. The `HTTP_LOG_*()` family of
 * macros is preferred.
 *
 * @param[in] fmt printf-like format string. `\n` is always appended
 * @param[in] ... optional parameters for the format string
 */
static inline void
http_log(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vprintf(fmt, ap);
	va_end(ap);
	puts("");
}

#endif /* _PICOW_HTTP_LOG_H */
